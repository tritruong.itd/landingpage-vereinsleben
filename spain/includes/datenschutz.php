<div class="container page" id="datenschutz">
    <div class="row">
        <div class="col-12 text-right">
            <a href="assets/files/Datenschutz.pdf" download class="btn btn-warning">Download</a>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
          <p>Traducci&oacute;n de ayuda de la&nbsp;<br />
            Declaraci&oacute;n de privacidad de datos</p>

          <p><span style="color:#e74c3c">(S&oacute;lo la versi&oacute;n m&aacute;s abajo en alem&aacute;n es legalmente vinculante)</span></p>

          <p><br />
            <strong>Declaraci&oacute;n de privacidad de datos</strong></p>

          <p>Protecci&oacute;n de datos</p>

          <p>Las siguientes disposiciones le informan a usted (el &quot;Usuario&quot;) de la naturaleza, el alcance y la finalidad de la recogida, el uso y el tratamiento de datos personales por parte de</p>

          <p>Martina Mart&iacute;nez<br />
            Im M&uuml;hlengrund 11<br />
            56566 Neuwied</p>

          <p>Tel&eacute;fono: +49.2631.9569718<br />
            Fax: +49.2631.9522749<br />
            M&oacute;vil: +49.170.9604841<br />
            E-mail: <a href="to:mm@martina-martinez.de"><span style="color:#3498db">mm@martina-martinez.de</span></a></p>

          <p>(en adelante, el &quot;proveedor&quot;) en este sitio web.</p>

          <p>Los datos est&aacute;n protegidos de acuerdo con la normativa legal. A continuaci&oacute;n, encontrar&aacute; informaci&oacute;n sobre qu&eacute; datos se recopilan durante su visita y c&oacute;mo se utilizan.</p>

          <p>De esta manera, el proveedor cumple con las disposiciones pertinentes de protecci&oacute;n de datos, en particular las disposiciones de la Ley sobre Medios de Comunicaci&oacute;n (TMG) y la Ley Federal de Protecci&oacute;n de Datos (BDSG).</p>

          <p><strong>1. Recopilaci&oacute;n y procesamiento de datos</strong></p>

          <p><strong>2. Tratamiento an&oacute;nimo de datos</strong></p>

          <p>Los usuarios pueden visitar el sitio web del proveedor sin dar informaci&oacute;n sobre su persona. S&oacute;lo se almacenan los datos de acceso sin referencia personal.<br />
            Por razones t&eacute;cnicas, se registran los siguientes datos transmitidos por su navegador de Internet al proveedor o a su proveedor de espacio web (los llamados &ldquo;log files&rdquo; del servidor):</p>

          <p>- Tipo y versi&oacute;n del navegador<br />
            - Sistema operativo utilizado<br />
            - Sitio web desde el que nos visita (URL de referencia)<br />
            - Sitio web que visita<br />
            - Fecha y hora de su acceso<br />
            - Su direcci&oacute;n de Protocolo de Internet (IP)<br />
            - &quot;Proveedor de servicios de Internet&quot;</p>

          <p>Para ello no es necesario el consentimiento previo del usuario.</p>

          <p>Estos datos an&oacute;nimos se almacenan por separado de cualquier informaci&oacute;n personal que se pueda proporcionar y, por lo tanto, no permiten sacar ninguna conclusi&oacute;n sobre una persona en particular. Se eval&uacute;an con fines estad&iacute;sticos para optimizar la presencia en Internet y la oferta del proveedor. Sin embargo, esta &uacute;ltima se reserva el derecho de revisar posteriormente los datos de acceso si, sobre la base de pruebas concretas, existe una sospecha leg&iacute;tima de uso il&iacute;cito.</p>

          <p>Estos datos no se combinar&aacute;n con otras fuentes de datos. Despu&eacute;s del an&aacute;lisis estad&iacute;stico, todos los datos ser&aacute;n borrados.</p>

          <p>Estos datos se eval&uacute;an &uacute;nicamente para mejorar la oferta del proveedor y no permiten ninguna conclusi&oacute;n personal sobre el usuario. Los datos personales s&oacute;lo se recogen si el usuario est&aacute; de acuerdo voluntariamente con ello durante su visita a la p&aacute;gina web.</p>

          <p><strong>1. Datos personales</strong></p>

          <p>Los datos personales son informaciones con las que se puede determinar la identidad de una persona. Esto incluye informaci&oacute;n personal como nombre, direcci&oacute;n, fecha de nacimiento, direcci&oacute;n de correo electr&oacute;nico o n&uacute;mero de tel&eacute;fono. El proveedor s&oacute;lo recoge, utiliza o transfiere datos personales de acuerdo con la normativa de protecci&oacute;n de datos o si el usuario est&aacute; de acuerdo con la recogida de datos, por ejemplo, en el contacto personal o al enviar correos electr&oacute;nicos al proveedor. Los datos personales s&oacute;lo se recogen si el usuario proporciona voluntariamente esta informaci&oacute;n durante su visita al sitio web. Estos datos son recogidos por el proveedor, procesados y almacenados y utilizados con fines de cotizaci&oacute;n, as&iacute; como para su posterior procesamiento y soporte. La transferencia de estos datos a terceros no involucrados sin consentimiento no tiene lugar, a menos que as&iacute; lo disponga la ley. Adem&aacute;s, los datos personales se recogen, procesan, almacenan y utilizan si el usuario lo especifica expl&iacute;citamente. El proveedor s&oacute;lo recoge, utiliza o transfiere datos personales de acuerdo con la normativa de protecci&oacute;n de datos. Los datos personales del usuario almacenados en el proveedor son procesados por el proveedor o utilizados para salvaguardar sus propios intereses comerciales leg&iacute;timos y, en caso necesario, transmitidos a los proveedores de servicios adecuados con el fin de prestar el servicio deseado y comunicarse con el usuario. En el contexto de la intermediaci&oacute;n contractual, el proveedor tambi&eacute;n recoge, procesa y utiliza los datos requeridos por el usuario a efectos fiscales y de facturaci&oacute;n.</p>

          <p><strong>2. Formulario de contacto</strong></p>

          <p>El proveedor ofrece al usuario la oportunidad de ponerse en contacto con ellos por correo electr&oacute;nico y / o a trav&eacute;s de un formulario de contacto en su sitio. En este caso, los datos proporcionados por el usuario con el fin de procesar su contacto, as&iacute; como en el caso de que surjan preguntas de seguimiento, se almacenan. La transmisi&oacute;n a terceros no tiene lugar. Tampoco se produce una comparaci&oacute;n de los datos recopilados con los datos que pueden haber sido recopilados en otras partes del sitio.</p>

          <p><strong>3. Uso de cookies</strong></p>

          <p>El proveedor utiliza cookies en su sitio web. Las cookies son peque&ntilde;os archivos de texto que se almacenan en el ordenador del usuario y que permiten analizar el uso que &eacute;ste hace de la web. La informaci&oacute;n generada por las cookies, como la hora, el lugar y la frecuencia de la visita al sitio web, incluida la direcci&oacute;n IP del usuario, es almacenada por el proveedor en el PC del usuario. La direcci&oacute;n IP es inmediatamente anonimizada durante este proceso para que usted permanezca an&oacute;nimo para el proveedor como usuario. La informaci&oacute;n generada por la cookie sobre el uso de este sitio web no ser&aacute; revelada a terceros. El usuario puede impedir la instalaci&oacute;n de las cookies mediante la correspondiente configuraci&oacute;n de su navegador. Sin embargo, el usuario debe ser consciente de que en este caso es posible que no pueda utilizar todas las funciones del sitio web.</p>

          <p><strong>4. Seguridad de los datos</strong></p>

          <p>Por razones t&eacute;cnicas, la obtenci&oacute;n de datos por parte de personas no autorizadas a trav&eacute;s de Internet nunca puede descartarse por completo. El proveedor ha tomado las medidas t&eacute;cnicas y organizativas necesarias para proteger adecuadamente los datos almacenados del usuario. El proveedor declara expresamente que, a pesar de todas las precauciones t&eacute;cnicas, el Internet no permite la seguridad absoluta de los datos. El proveedor no es responsable de las acciones de terceros.</p>

          <p><br />
            <strong>5. Revocaci&oacute;n, cambios, correcciones y actualizaciones de datos</strong></p>

          <p>El usuario tiene derecho, previa solicitud, a recibir informaci&oacute;n gratuita sobre los datos personales que se han almacenado sobre &eacute;l. Adem&aacute;s, tiene derecho a corregir datos inexactos, bloquear y eliminar sus datos personales, siempre que ello no entre en conflicto con un requisito legal de retenci&oacute;n.</p>

          <p>La revocaci&oacute;n debe ser enviada a:</p>

          <p>Martina Mart&iacute;nez<br />
            Im M&uuml;hlengrund 11<br />
            56566 Neuwied&nbsp;</p>

          <p>Tel&eacute;fono: +49.2631.9569718<br />
            Fax: +49.2631.9522749<br />
            M&oacute;vil: +49.170.9604841<br />
            E-mail: mm@martina-martinez.de</p>

          <p><strong>6. Persona de contacto para el uso de sus datos</strong></p>

          <p>Si tiene alguna pregunta sobre la recopilaci&oacute;n, el procesamiento o el uso de sus datos personales, as&iacute; como sobre la informaci&oacute;n, las correcciones, el bloqueo o la eliminaci&oacute;n de datos, p&oacute;ngase en contacto con nosotros:</p>

          <p>Martina Mart&iacute;nez<br />
            Im M&uuml;hlengrund 11<br />
            56566 Neuwied&nbsp;</p>

          <p>Tel&eacute;fono: +49.2631.9569718<br />
            Fax: +49.2631.9522749<br />
            M&oacute;vil: +49.170.9604841<br />
            E-mail: mm@martina-martinez.de</p>

          <p><br />
            <strong>7. Borrado de sus datos</strong></p>

          <p>Usted tiene derecho a solicitar la eliminaci&oacute;n de los datos personales almacenados por nosotros. La posibilidad de una cancelaci&oacute;n real depende de si el cumplimiento de una obligaci&oacute;n legal por nuestra parte, como el cumplimiento de los requisitos legales de retenci&oacute;n y la afirmaci&oacute;n, ejercicio y defensa de reclamaciones legales, lo hace posible.</p>

          <p>B&aacute;sicamente, podemos eliminar sus datos si ya no son necesarios para el fin para el que fueron recogidos.</p>

          <p><br />
            <strong>Datenschutz</strong><br />
            Nachstehende Regelungen informieren Sie (den &bdquo;Nutzer&rdquo;) &uuml;ber die Art, den Umfang und Zweck der Erhebung, die Nutzung und die Verarbeitung von personenbezogenen Daten durch die<br />
            Martina Mart&iacute;nez<br />
            Im M&uuml;hlengrund 11<br />
            56566 Neuwied<br />
            Telefon: +49.2631.9569718<br />
            Telefax: +49.2631.9522749<br />
            Mobil: +49.170.9604841<br />
            E-Mail: mm@martina-martinez.de<br />
            (im folgenden &bdquo;Anbieter&ldquo; genannt) auf dieser Webseite.<br />
            Daten werden im Rahmen der gesetzlichen Vorschriften gesch&uuml;tzt. Nachfolgend finden Sie Informationen, welche Daten w&auml;hrend Ihres Besuchs erfasst und wie diese genutzt werden.<br />
            Hierbei beachtet der Anbieter die einschl&auml;gigen datenschutzrechtlichen Bestimmungen, insbesondere die Regelungen des Telemediengesetzes (TMG) und des Bundesdatenschutzgesetzes (BDSG).<br />
            3. <strong>Datenerhebung und -verarbeitung</strong><br />
            4.<strong> Anonyme Datenverarbeitung</strong><br />
            Der Nutzer kann den Internetauftritt des Anbieters besuchen, ohne Angaben zu seiner Person zu machen. Gespeichert werden lediglich Zugriffsdaten ohne Personenbezug.<br />
            Aus technischen Gr&uuml;nden werden folgende Daten, die sein Internet-Browser an den Anbieter bzw. an seinen Webspace-Provider &uuml;bermittelt, erfasst (sogenannte Serverlogfiles):<br />
            Browsertyp und -version<br />
            verwendetes Betriebssystem<br />
            Webseite, von der aus Sie uns besuchen (Referrer URL)<br />
            Webseite, die Sie besuchen<br />
            Datum und Uhrzeit Ihres Zugriffs<br />
            Ihre Internet Protokoll (IP)-Adresse<br />
            &bdquo;Internet Service Provider&ldquo;<br />
            Einer vorherigen Zustimmung des Nutzers bedarf es hierf&uuml;r nicht.<br />
            Diese anonymen Daten werden getrennt von eventuell angegebenen personenbezogenen Daten gespeichert und lassen so keine R&uuml;ckschl&uuml;sse auf eine bestimmte Person zu. Sie werden zu statistischen Zwecken ausgewertet, um den Internetauftritt und das Angebot des Anbieters optimieren zu k&ouml;nnen. Dieser beh&auml;lt sich jedoch vor, die Zugriffsdaten nachtr&auml;glich zu &uuml;berpr&uuml;fen, wenn aufgrund konkreter Anhaltspunkte der berechtigte Verdacht einer rechtswidrigen Nutzung steht.<br />
            Eine Zusammenf&uuml;hrung dieser Daten mit anderen Datenquellen wird nicht vorgenommen. Nach erfolgter statistischer Auswertung werden s&auml;mtliche Daten gel&ouml;scht.<br />
            Diese Daten werden ausschlie&szlig;lich zur Verbesserung des Angebots des Anbieters ausgewertet und erlauben keinerlei R&uuml;ckschl&uuml;sse auf die Person des Nutzers. Personenbezogene Daten werden nur erhoben, wenn der Nutzer uns diese im Rahmen seines Besuchs des Internetauftritts freiwillig mitteilt.<br />
            2.<strong> Personenbezogene Daten</strong><br />
            Personenbezogene Daten sind solche Informationen, mit deren Hilfe eine Person bestimmbar ist. Dazu geh&ouml;ren pers&ouml;nliche Daten wie der Name, die Adresse, das Geburtsdatum, die E-Mail- Adresse oder die Telefonnummer. Der Anbieter erhebt, nutzt oder gibt personenbezogene Daten nur unter Einhaltung der datenschutzrechtlichen Vorschriften weiter oder wenn der Nutzer in die Datenerhebung einwilligt, beispielsweise bei pers&ouml;nlicher Kontaktaufnahme oder bei der Versendung von E- Mails an den Anbieter. Personenbezogene Daten werden nur erhoben, wenn der Nutzer dieses im Rahmen seines Besuchs des Internetauftritts freiwillig mitteilt. Diese Daten werden von dem Anbieter erhoben, verarbeitet und f&uuml;r Zwecke der Angebotserstellung sowie der weiteren Bearbeitung und Betreuung gespeichert und genutzt. Eine Weitergabe dieser Daten an unbeteiligte Dritte ohne Einwilligung erfolgt nicht, sofern dies nicht gesetzlich vorgesehen ist. Dar&uuml;ber hinaus werden pers&ouml;nliche Daten erhoben, verarbeitet, gespeichert und genutzt, wenn der Nutzer diese von sich aus angibt. Der Anbieter erhebt, nutzt oder gibt personenbezogene Daten nur unter Einhaltung der datenschutzrechtlichen Vorschriften weiter. Die bei dem Anbieter gespeicherten pers&ouml;nlichen Daten des Nutzers werden von dem Anbieter verarbeitet bzw. zur Wahrung berechtigter eigener Gesch&auml;ftsinteressen genutzt und gegebenenfalls an die entsprechenden Serviceanbieter &uuml;bermittelt, um den jeweils gew&uuml;nschten Service zu erbringen und um mit dem Nutzer kommunizieren zu k&ouml;nnen. Im Rahmen der Vertragsvermittlung erhebt, verarbeitet und nutzt der Anbieter die daf&uuml;r erforderlichen Daten des Nutzers auch f&uuml;r steuerliche und abrechnungsrelevante Zwecke.<br />
            3. <strong>Kontaktm&ouml;glichkeit/Kontaktformular</strong><br />
            Der Anbieter bietet dem Nutzer auf seiner Seite die M&ouml;glichkeit, mit ihm per E-Mail und/oder &uuml;ber ein Kontaktformular in Verbindung zu treten. In diesem Fall werden die vom Nutzer gemachten Angaben zum Zwecke der Bearbeitung seiner Kontaktaufnahme sowie f&uuml;r den Fall, dass Anschlussfragen entstehen, gespeichert. Eine Weitergabe an Dritte erfolgt nicht. Ein Abgleich der so erhobenen Daten mit Daten, die m&ouml;glicherweise durch andere Komponenten der Seite erhoben werden, erfolgt ebenfalls nicht.<br />
            4. <strong>Einsatz von Cookies</strong><br />
            Der Anbieter setzt auf seiner Webseite Cookies ein. Cookies sind kleine Textdateien, die auf dem Computer des Nutzers gespeichert werden und die eine Analyse der Benutzung der Webseite durch den Nutzer erm&ouml;glichen. Die durch die Cookies erzeugten Informationen, beispielsweise Zeit, Ort und H&auml;ufigkeit des Webseiten-Besuchs einschlie&szlig;lich der IP-Adresse des Nutzers speichert der Anbieter auf dem PC des Nutzers. Die IP-Adresse wird bei diesem Vorgang umgehend anonymisiert, so dass Sie als Nutzer f&uuml;r den Anbieter anonym bleiben. Die durch den Cookie erzeugten Informationen &uuml;ber die Benutzung dieser Webseite werden nicht an Dritte weitergegeben. Der Nutzer kann die Installation der Cookies durch eine entsprechende Einstellung seiner Browser-Software verhindern; der Anbieter weist ihn jedoch darauf hin, dass er in diesem Fall gegebenenfalls nicht s&auml;mtliche Funktionen der Webseite vollumf&auml;nglich nutzen kann.<br />
            5. <strong>Sicherheit der Daten</strong><br />
            Bei der Daten&uuml;bertragung im Internet kann aus technischen Gr&uuml;nden nie vollst&auml;ndig ausgeschlossen werden, dass Unberechtigte Zugriff auf die &uuml;bertragenen Daten erhalten. Der Anbieter hat die erforderlichen technischen und organisatorischen Ma&szlig;nahmen ergriffen, um die durch ihn gespeicherten Daten der Nutzer angemessen zu sch&uuml;tzen. Er weist ausdr&uuml;cklich darauf hin, dass das Internet trotz aller technischen Vorkehrungen eine absolute Datensicherheit nicht zul&auml;sst. Der Anbieter haftet nicht f&uuml;r Handlungen Dritter.<br />
            6. <strong>Widerruf, &Auml;nderungen, Berichtigungen und Aktualisierungen von Daten</strong><br />
            Der Nutzer hat das Recht, auf Antrag unentgeltlich Auskunft zu erhalten &uuml;ber die personenbezogenen Daten, die &uuml;ber ihn gespeichert wurden. Zus&auml;tzlich hat er das Recht auf Berichtigung unrichtiger Daten, Sperrung und L&ouml;schung seiner personenbezogenen Daten, soweit dem keine gesetzliche Aufbewahrungspflicht entgegensteht<br />
            <strong>Der Widerruf ist zu richten an:</strong><br />
            Martina Mart&iacute;nez<br />
            Im M&uuml;hlengrund 11<br />
            56566 Neuwied<br />
            Telefon: <u>+49.2631.9569718</u><br />
            Telefax: <u>+49.2631.9522749</u><br />
            Mobil: <u>+49.170.9604841</u><br />
            E-Mail: <a href="to:mm@martina-martinez.de"><span style="color:#3498db">mm@martina-martinez.de</span></a><br />
            7. <strong>Ansprechpartner zur Nutzung Ihrer Daten</strong><br />
            Bei Fragen zur Erhebung, Verarbeitung oder Nutzung Ihrer personenbezogenen Daten sowie bei Ausk&uuml;nften, Berichtigungen, Sperrung oder L&ouml;schung von Daten wenden Sie sich bitte an:<br />
            Martina Mart&iacute;nez<br />
            Im M&uuml;hlengrund 11<br />
            56566 Neuwied<br />
            Telefon: <u>+49.2631.9569718</u><br />
            Telefax: <u>+49.2631.9522749</u><br />
            Mobil: <u>+49.170.9604841</u><br />
            E-Mail: <a href="to:mm@martina-martinez.de"><span style="color:#3498db">mm@martina-martinez.de</span></a><br />
            8. <strong>L&ouml;schung Ihrer Daten</strong><br />
            Sie haben das Recht, von uns L&ouml;schung der bei uns gespeicherten personenbezogenen Daten zu verlangen. Die M&ouml;glichkeit einer tats&auml;chlichen L&ouml;schung richtet sich dabei danach, ob die Erf&uuml;llung einer rechtlichen Verpflichtung durch uns, wie etwa die Einhaltung gesetzlicher Aufbewahrungspflichten sowie die Geltendmachung, Aus&uuml;bung und Verteidigung von Rechtsanspr&uuml;chen dies m&ouml;glich macht.<br />
            Grunds&auml;tzlich gilt f&uuml;r die L&ouml;schung Ihrer Daten, dass dies durch uns dann erfolgt, wenn die Daten f&uuml;r den jeweiligen Zweck nicht mehr erforderlich sind.</p>
        </div>
    </div>
</div>