<div class="container page" id="impressum">
    <div class="row">
        <div class="col-12">
            <h3>Impressum</h3><br>
            <div class="fusion-text">
              <p>
                Aviso Legal<br />
                <span style="color:#e74c3c">(S&oacute;lo la versi&oacute;n m&aacute;s abajo en alem&aacute;n es legalmente vinculante)</span></p>

              <p>Martina Mart&iacute;nez<br />
                Im M&uuml;hlengrund 11<br />
                56566 Neuwied</p>

              <p>Tel&eacute;fono: +49.2631.9569718<br />
                Telefax: +49.2631.9522749<br />
                Celular: +49.170.9604841<br />
                E-mail: mm@martina-martinez.de</p>

              <p><strong>Sector / Actividad: </strong>Corredur&iacute;a de seguros<br />
                <strong>Estado que otorg&oacute; el t&iacute;tulo profesional: </strong>Alemania<br />
                <strong>Responsable del contenido seg&uacute;n el art&iacute;culo 55 p&aacute;rrafo 2 RStV:</strong> Martina Mart&iacute;nez</p>

              <p><strong>Agencia Tributaria responsable:</strong><br />
                Neuwied<br />
                Augustastr. 70<br />
                56564 Neuwied<br />
                Tel&eacute;fono (centralita): 02631/9100<br />
                Fax (oficina central): 02631/91029906</p>

              <p><strong>Autorizaci&oacute;n para el permiso seg&uacute;n art&iacute;culo 34d p&aacute;rrafo 1 GewO (&sect; 34d Abs. 1 GewO):</strong><br />
                C&aacute;mara de Industria y Comercio de Coblenza (Industrie- und Handelskammer zu Koblenz - IHK Koblenz)<br />
                Schlossstra&szlig;e 2<br />
                56068 Koblenz</p>

              <p>Se ha concedido el permiso de conformidad con el art&iacute;culo 34d, p&aacute;rrafo 1 del GewO (&sect; 34d Abs. 1 GewO).<br />
                <strong>N&uacute;mero de registro (seg&uacute;n el art&iacute;culo 34d GewO): D-3KF9-A1I4B-40</strong></p>

              <p><strong>La inscripci&oacute;n en el registro de intermediarios puede comprobarse de la siguiente manera:</strong><br />
                Asociaci&oacute;n de C&aacute;maras de Industria y Comercio Alemanas (Deutscher Industrie- und Handelskammertag, DIHK) e.V.<br />
                Breite Stra&szlig;e 29<br />
                10178 Berl&iacute;n<br />
                Tel&eacute;fono: 0180 600 58 50<br />
                (precio fijo &euro; 0,20 /llamada; precios m&oacute;viles m&aacute;ximo &euro; 0,60 /llamada)<br />
                <a href="http://www.vermittlerregister.info">www.vermittlerregister.info</a></p>

              <p><strong>T&iacute;tulo profesional: </strong>Corredor de seguros con autorizaci&oacute;n seg&uacute;n el art&iacute;culo 34d p&aacute;rrafo 1 GewO (&sect; 34d Abs. 1 GewO)</p>

              <p><strong>Normativa profesional:</strong><br />
                - art&iacute;culo 34d GewO (&sect; 34d Gewerbeordnung)<br />
                - art&iacute;culos 59-68 VVG (&sect;&sect; 59-68 VVG)<br />
                - VersVermV<br />
                Se puede consultar y comprobar el reglamento profesional a trav&eacute;s del sitio web www.gesetze-im-internet.de, gestionada por el Ministerio Federal de Justicia y por juris GmbH.&nbsp;</p>

              <p><strong>Los siguientes &oacute;rganos de arbitraje pueden ser llamados a resolver disputas fuera de los tribunales:</strong></p>

              <p>Versicherungsombudsman e.V.,<br />
                Postfach 08 06 32, 10006 Berl&iacute;n<br />
                M&aacute;s informaci&oacute;n: <a href="http://www.versicherungsombudsmann.de">www.versicherungsombudsmann.de</a></p>

              <p>Ombudsmann Private Kranken- und Pflegeversicherung,<br />
                Postfach 06 02 02 22, 10052 Berl&iacute;n<br />
                M&aacute;s informaci&oacute;n: <a href="http://www.pkv-ombudsmann.de">www.pkv-ombudsmann.de</a></p>

              <p style="text-align:center">LOGO</p>

              <p><strong>Martina Mart&iacute;nez es una corredora de seguros independiente.</strong><br />
                No posee participaciones directas o indirectas superiores al 10% de los derechos de voto o del capital de una compa&ntilde;&iacute;a de seguros. Una compa&ntilde;&iacute;a de seguros no posee participaciones directas o indirectas superiores al 10% de los derechos de voto o del capital en Martina Mart&iacute;nez.</p>

              <p><strong>Nota de conformidad con el art&iacute;culo 14, p&aacute;rrafo 1, del Reglamento UE 524/2013</strong><br />
                Resoluci&oacute;n de litigios online (art&iacute;culo 14, p&aacute;rrafo 1, del Reglamento ODR): La Comisi&oacute;n Europea ofrece una plataforma para la resoluci&oacute;n de litigios online en ec.europa.eu/consumidores/odr/.</p>

              <p><strong>Descargo de responsabilidad:</strong><br />
                <strong>1. Limitaci&oacute;n de responsabilidad</strong><br />
                Los contenidos de esta p&aacute;gina web han sido elaborados con el mayor cuidado posible. Sin embargo, el proveedor no asume ninguna responsabilidad por la exactitud, integridad y vigencia del contenido proporcionado. El uso de los contenidos de la p&aacute;gina web es responsabilidad del usuario. Las contribuciones con autor nombrado reflejan la opini&oacute;n del autor respectivo y no siempre la opini&oacute;n del proveedor. El mero uso del sitio web no constituye una relaci&oacute;n contractual entre el usuario y el proveedor.</p>

              <p><strong>2. Enlaces externos</strong><br />
                Este sitio web contiene enlaces a sitios web de terceros (&quot;enlaces externos&quot;). Estos sitios web est&aacute;n sujetos a la responsabilidad de los respectivos operadores. En el momento de la creaci&oacute;n de los enlaces externos, el proveedor ha comprobado los contenidos externos en busca de posibles violaciones de la ley. En ese momento, ninguna violaci&oacute;n de la ley era aparente. El proveedor no tiene ninguna influencia sobre el dise&ntilde;o actual y futuro, ni sobre el contenido de las p&aacute;ginas enlazadas. La inclusi&oacute;n de enlaces externos no significa que el proveedor adopte el contenido detr&aacute;s de la referencia o enlace como propio. Un control constante de los enlaces externos no es razonable para nosotros, sin pruebas concretas de violaciones legales. Sin embargo, si tenemos conocimiento de alguna violaci&oacute;n de la ley, dichos enlaces externos ser&aacute;n eliminados inmediatamente.</p>

              <p><strong>3. Derechos de autor y derechos afines</strong><br />
                Los contenidos publicados en este sitio web est&aacute;n sujetos a los derechos de autor y derechos afines alemanes. Cualquier uso no permitido por la ley alemana de derechos de autor y derechos afines requiere el consentimiento previo por escrito del proveedor o del respectivo titular de los derechos. Esto se aplica en particular a la duplicaci&oacute;n, edici&oacute;n, traducci&oacute;n, almacenamiento, procesamiento o reproducci&oacute;n de contenidos en bases de datos u otros medios y sistemas electr&oacute;nicos. Los contenidos y derechos de terceros est&aacute;n marcados como tales. La reproducci&oacute;n o transmisi&oacute;n no autorizada de contenidos individuales o p&aacute;ginas completas no est&aacute; permitida y constituye un delito. S&oacute;lo se permite la producci&oacute;n de copias y descargas para uso personal, privado y no comercial.<br />
                La presentaci&oacute;n de este sitio web en frames externos s&oacute;lo se permite con permiso escrito.</p>

              <p><strong>4 Validez legal de esta exenci&oacute;n de responsabilidad</strong><br />
                Esta exenci&oacute;n de responsabilidad debe considerarse como parte de la oferta de Internet desde la que se le ha remitido. Si secciones o t&eacute;rminos individuales de esta declaraci&oacute;n no son legales o correctos, el contenido o la validez de las otras partes no se ver&aacute;n afectados por este hecho.</p>

              <p><strong>5. Aviso legal</strong><br />
                Los datos proporcionados no pretenden ser correctos o completos. No se asume ninguna responsabilidad por la exactitud e integridad de los datos y documentos proporcionados por terceros. Se excluye cualquier responsabilidad por da&ntilde;os y perjuicios derivados de la informaci&oacute;n aqu&iacute; proporcionada.</p>
            </div>
        </div>
    </div>
</div>