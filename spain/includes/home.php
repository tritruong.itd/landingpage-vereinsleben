<div class="container tabContent activeTab" id="homepage">
    <div class="row description">
        <div class="basket">
            Mis consultas
            <span class="badge badge-pill badge-danger"><?php echo getTotalCart(); ?></span>
            <ul class="listItem" id="list_item">
                <?php
                $itemCarts = getItemCart();
                if ($itemCarts) {
                    foreach ($itemCarts as $item) {
                        ?>
                        <li class="item btn-edit clearfix"><span
                            data-id="<?php echo $item['id'] ?>"><?php echo $item['name'] ?></span></li><?php
                    }
                    echo '<li class="item btn-edit"><button data-id=""
                                                          class="btn btn-sm btn-submit btn-success btn-block">Enviar solicitud
                            </button></li>';
                }
                ?>

            </ul>
        </div>
        <div class="avatar">
            <img src="assets/images/avatar.png" alt="">
        </div>
        <div class="col-md-2">

        </div>
        <div class="content col-md-11">
            <p class="col-7">
              ¿Es usted una persona hispanohablante en Alemania que está buscando un seguro personal?<br><br>
              Por favor elija los tipos de seguro que le interesan.
            </p>
        </div>
    </div>
    <div class="section row">
        <div class="item item-homepage col-md-4 text-center">
            <div class="title">
                <h2>Seguro de responsabilidad<br/>civil privada</h2>
                <div class="tool-tip">
                    <div class="icon-tooltip" data-toggle="tooltip" data-placement="bottom"
                         title="El seguro de responsabilidad civil privada lo protege contra las consecuencias financieras de dañar algo que pertenece a un tercero o herir a una persona involuntariamente.">
                        <p><i class="fa fa-question-circle"></i></p>
                    </div>
                </div>
            </div>
            <div class="icon" data-id="liability-insurance">
                <img src="assets/images/liability.png" alt="">
            </div>
            <div class="check-box">
                <input id="liability-insurance" type="checkbox" name="liability-insurance"
                       class="checkbox-label checkbox-item" value="liability-insurance"
                    <?php echo checkExistCart('liability-insurance') ? 'checked' : '' ?>/>
                <label for="liability-insurance">
                    Estoy interesado/a
                </label>
            </div>
        </div>
        <div class="item item-homepage col-md-4 text-center">
            <div class="title">
                <h2>Seguro <br/>
                  del hogar</h2>
                <div class="tool-tip">
                    <div class="icon-tooltip" data-html="true" data-toggle="tooltip" data-placement="bottom"
                         title="Protege sus pertenencias contra daños causados por incendio, fuga de agua, tormenta/granizo, robo y otros riesgos.">
                        <p><i class="fa fa-question-circle"></i></p>
                    </div>
                </div>
            </div>
            <div class="icon" data-id="household-insurance">
                <img src="assets/images/household.png" alt="">
            </div>
            <div class="check-box">
                <input id="household-insurance" type="checkbox" name="household-insurance"
                       class="checkbox-label checkbox-item" value="household-insurance"
                    <?php echo checkExistCart('household-insurance') ? 'checked' : '' ?>/>
                <label for="household-insurance">
                    Estoy interesado/a
                </label>
            </div>
        </div>
        <div class="item item-homepage col-md-4 text-center">
            <div class="title">
                <h2>Seguro de <br/>
                  protección jurídica</h2>
                <div class="tool-tip">
                    <div class="icon-tooltip" data-toggle="tooltip" data-placement="bottom" title="A veces se requiere apoyo legal para defenderse de las acusaciones o para hacer valer sus propios derechos e intereses. El seguro de protección jurídica cubre los costos de muchas de las posibles disputas legales.">
                        <p><i class="fa fa-question-circle"></i></p>
                    </div>
                </div>
            </div>
            <div class="icon" data-id="legal-protection-insurance">
                <img src="assets/images/legal-protection.png" alt="">
            </div>
            <div class="check-box">
                <input id="legal-protection-insurance" type="checkbox" name="legal-protection-insurance"
                       class="checkbox-label checkbox-item" value="legal-protection-insurance"
                    <?php echo checkExistCart('legal-protection-insurance') ? 'checked' : '' ?>/>
                <label for="legal-protection-insurance">
                    Estoy interesado/a
                </label>
            </div>
        </div>
    </div>
    <div class="section row">
        <div class="item item-homepage col-md-4 text-center">
            <div class="title">
                <h2>Seguros <br/>
                  de viaje</h2>
                <div class="tool-tip">
                    <div class="icon-tooltip" data-html="true" data-toggle="tooltip" data-placement="bottom"
                         title="Los seguros de viaje son &uacute;tiles para viajes privados y de negocios y para estancias en el extranjero. Incluso si usted planifica todo bien, pueden suceder cosas inesperadas. Se necesita un <strong>seguro m&eacute;dico de viaje</strong> si tiene un seguro de salud p&uacute;blica alem&aacute;n (que generalmente no es v&aacute;lido fuera de Alemania) y desea viajar al extranjero. Tambi&eacute;n es necesario para los viajeros extranjeros que quieren visitar Alemania. Los seguros de viaje cubren la <strong>tarifa de cancelaci&oacute;n</strong> de sus gastos de viaje en caso de enfermedad repentina, accidente, desempleo o muerte de una persona que viaja. El seguro de equipaje protege en caso de p&eacute;rdida de equipaje.">
                        <p><i class="fa fa-question-circle"></i></p>
                    </div>
                </div>
            </div>
            <div class="icon" data-id="travel-insurance">
                <img src="assets/images/travel-insurances.png" alt="">
            </div>
            <div class="check-box">
                <input id="travel-insurance" type="checkbox" name="travel-insurances"
                       class="checkbox-label checkbox-item" value="travel-insurance"
                    <?php echo checkExistCart('travel-insurance') ? 'checked' : '' ?>/>
                <label for="travel-insurance">
                    Estoy interesado/a
                </label>
            </div>
        </div>
        <div class="item item-homepage col-md-4 text-center">
            <div class="title">
                <h2>Seguro <br/>
                  del automóvil</h2>
                <div class="tool-tip">
                    <div class="icon-tooltip" data-html="true" data-toggle="tooltip" data-placement="bottom"
                         title="El <strong>seguro de responsabilidad civil del autom&oacute;vil</strong> es obligatorio para todos los propietarios de autom&oacute;viles que quieran conducir su veh&iacute;culo en el tr&aacute;fico p&uacute;blico. Este seguro cubre los gastos si el conductor da&ntilde;a un veh&iacute;culo ajeno. Adem&aacute;s, se puede asegurar por da&ntilde;os a su propio coche a trav&eacute;s de las coberturas <strong>parciales </strong>o <strong>completas.</strong>">
                        <p><i class="fa fa-question-circle"></i></p>
                    </div>
                </div>
            </div>
            <div class="icon" data-id="car-insurance">
                <img src="assets/images/car-insurance.png" alt="">
            </div>
            <div class="check-box">
                <input id="car-insurance" type="checkbox" name="car-insurance"
                       class="checkbox-label checkbox-item" value="car-insurance"
                    <?php echo checkExistCart('car-insurance') ? 'checked' : '' ?>/>
                <label for="car-insurance">
                    Estoy interesado/a
                </label>
            </div>
        </div>
        <div class="item item-homepage col-md-4 text-center">
            <div class="title">
                <h2>Seguro de <br/>
                  vida a término</h2>
                <div class="tool-tip">
                    <div class="icon-tooltip" data-html="true" data-toggle="tooltip" data-placement="bottom"
                         title="El  seguro de vida a término asegura a su familia. En la situación más difícil de la vida, su familia recibe apoyo financiero, que se puede utilizar por ejemplo para pagar préstamos abiertos o la educación de sus hijos.">
                        <p><i class="fa fa-question-circle"></i></p>
                    </div>
                </div>
            </div>
            <div class="icon" data-id="term-life-insurance">
                <img src="assets/images/term-life-insurance.png" alt="">
            </div>
            <div class="check-box">
                <input id="term-life-insurance" type="checkbox" name="term-life-insurance"
                       class="checkbox-label checkbox-item"
                       value="term-life-insurance"
                    <?php echo checkExistCart('term-life-insurance') ? 'checked' : '' ?>/>
                <label for="term-life-insurance">
                    Estoy interesado/a
                </label>
            </div>
        </div>
    </div>
    <div class="section row">
        <div class="item item-homepage col-md-4 text-center">
            <div class="title">
                <h2>Seguro de accidente/incapacidad ocupacional</h2>
                <div class="tool-tip">
                    <div class="icon-tooltip" data-html="true" data-toggle="tooltip" data-placement="bottom"
                         title="Un <strong>seguro de accidentes</strong> le ayuda con las consecuencias financieras despu&eacute;s de un accidente ofreci&eacute;ndole un beneficio por discapacidad, pensi&oacute;n por accidente, indemnizaci&oacute;n for fallecimiento, subsidio hospitalario diario y algunos extras m&aacute;s. Es v&aacute;lido en todo el mundo y le cubre las 24 horas del d&iacute;a.<br />
                                Un <strong>seguro de discapacidad ocupacional</strong> paga si no puede seguir trabajando en su trabajo debido a una enfermedad o accidente. Este seguro asegura la manutenci&oacute;n mensual para usted y su familia.">
                        <p><i class="fa fa-question-circle"></i></p>
                    </div>
                </div>
            </div>
            <div class="icon" data-id="accident-disability-insurance">
                <img src="assets/images/accident-disability.png" alt="">
            </div>
            <div class="check-box">
                <input id="accident-disability-insurance" type="checkbox" name="accident-disability-insurances"
                       class="checkbox-label checkbox-item"
                       value="accident-disability-insurance"
                    <?php echo checkExistCart('accident-disability-insurance') ? 'checked' : '' ?>/>
                <label for="accident-disability-insurance">
                    Estoy interesado/a
                </label>
            </div>
        </div>
        <div class="item item-homepage col-md-4 text-center">
            <div class="title">
                <h2>Seguro <br/>
                  de salud</h2>
                <div class="tool-tip">
                    <div class="icon-tooltip" data-html="true" data-toggle="tooltip" data-placement="bottom"
                         title="El seguro m&eacute;dico en Alemania es obligatorio. La cobertura general de las compa&ntilde;&iacute;as de <strong>seguros de salud p&uacute;blica </strong>est&aacute; prescrita por la ley, pero difieren en los precios y algunos extras.<br />
                                El <strong>seguro m&eacute;dico complementario</strong> es para personas que tienen seguro m&eacute;dico p&uacute;blico y quieren agregar los beneficios y ventajas que tiene un paciente privado.<br />
                                El <strong>seguro m&eacute;dico privado</strong> le permite elegir su propia cobertura m&eacute;dica, que por lo general tiene una calidad m&aacute;s alta que el seguro de salud p&uacute;blica. Generalmente tiene mejores servicios, tiempos de espera m&aacute;s cortos para citas m&eacute;dicas, mejor tratamiento y medicamentos. El seguro m&eacute;dico privado est&aacute; disponible tanto para aut&oacute;nomos como para empleados con un salario superior al l&iacute;mite m&aacute;ximo fijo para el seguro de salud p&uacute;blica (en 2019: 5.062,50 euros al mes / 60.750 euros al a&ntilde;o)">
                        <p><i class="fa fa-question-circle"></i></p>
                    </div>
                </div>
            </div>
            <div class="icon" data-id="health-insurance">
                <img src="assets/images/health-insurance.png" alt="">
            </div>
            <div class="check-box">
                <input id="health-insurance" type="checkbox" name="health-insurance"
                       class="checkbox-label checkbox-item" value="health-insurance"
                    <?php echo checkExistCart('health-insurance') ? 'checked' : '' ?>/>
                <label for="health-insurance">
                    Estoy interesado/a
                </label>
            </div>
        </div>
        <div class="item item-homepage col-md-4 text-center">
            <div class="title">
                <h2>Pensión <br/>
                  privada</h2>
                <div class="tool-tip">
                    <div class="icon-tooltip" data-html="true" data-toggle="tooltip" data-placement="bottom"
                         title="Dondequiera que pase su jubilaci&oacute;n, ya sea en Alemania o en el extranjero, un d&iacute;a el pago de su empleador terminar&aacute; y usted vivir&aacute; de los ahorros que hizo durante su vida laboral. En Alemania hay varios modelos de pensiones, algunos est&aacute;n subvencionados por el gobierno alem&aacute;n con ahorros fiscales y subsidios. Cu&aacute;l de ellos se adapta mejor a usted depende de su situaci&oacute;n personal y preferencias.<br />
                                Usted puede obtener ofertas para el <strong>seguro de pensi&oacute;n privada</strong>, la <strong>pensi&oacute;n b&aacute;sica</strong> (ahorro de impuestos), la <strong>pensi&oacute;n Riester</strong> (ahorro de impuestos, desgravaciones) y la <strong>pensi&oacute;n de empresa</strong> (ahorro de impuestos)">
                        <p><i class="fa fa-question-circle"></i></p>
                    </div>
                </div>
            </div>
            <div class="icon" data-id="pension-insurance">
                <img src="assets/images/pension-insurance.png" alt="">
            </div>
            <div class="check-box">
                <input id="pension-insurance" type="checkbox" name="pension-insurance"
                       class="checkbox-label checkbox-item" value="pension-insurance"
                    <?php echo checkExistCart('pension-insurance') ? 'checked' : '' ?>/>
                <label for="pension-insurance">
                    Estoy interesado/a
                </label>
            </div>
        </div>
    </div>
    <div class="row description-footer">
      <p>&iquest;Est&aacute; usted buscando ofertas de seguros en Alemania y necesita asesoramiento en espa&ntilde;ol? &nbsp;Con nuestra herramienta de consulta puede elegir f&aacute;cil y r&aacute;pidamente los seguros que le interesan. &nbsp;No importa si usted est&aacute; buscando un seguro de responsabilidad personal, un seguro de gastos legales o un seguro de hogar, s&oacute;lo tiene que introducir algunas cifras clave y calcularemos las ofertas individuales para usted. Adem&aacute;s, podemos aclarar en detalle cualquier pregunta que pueda tener en una consulta telef&oacute;nica. &nbsp;Finalmente, aqu&iacute; podr&aacute; entender lo que significa cada seguro.<br />
        La protecci&oacute;n contra terceros est&aacute; prevista en el <strong>seguro de responsabilidad civil privada</strong> (= Privathaftpflichtversicherung) y por el <strong>seguro de protecci&oacute;n jur&iacute;dica</strong> (= Rechtsschutzversicherung).&nbsp;<br />
        La propiedad se puede proteger con el <strong>seguro del hogar</strong> (=Hausratversicherung), el <strong>seguro de autom&oacute;vil</strong> (= Kfz-Versicherung o Autoversicherung), y el <strong>seguro del edificio</strong> (= Geb&auml;udeversicherung).<br />
        Los seguros de viaje son &uacute;tiles para viajes privados y de negocios y para estancias en el extranjero. Hay <strong>seguro m&eacute;dico de viaje</strong> (= Auslandsreise-Krankenversicherung), <strong>seguro de cancelaci&oacute;n de viaje</strong> (= Reiser&uuml;cktritt-Versicherung), <strong>seguro de interrupci&oacute;n de viaje</strong> (= Reiseabbruch-Versicherung) y <strong>seguro de equipaje</strong> (= Reisegep&auml;ck-Versicherung)<br />
        La protecci&oacute;n personal para usted y su familia la encontrar&aacute; con el <strong>seguro de accidentes</strong> (= Unfallversicherung), el <strong>seguro de incapacidad ocupacional</strong> (= Berufsunf&auml;higkeitsversicherung) y el <strong>seguro de vida a t&eacute;rmino</strong> (= Risikolebensversicherung)<br />
        Los seguros de pensi&oacute;n para su jubilaci&oacute;n est&aacute;n disponibles como <strong>seguro de pensi&oacute;n privado</strong> (= Private Rentenversicherung), la <strong>pensi&oacute;n Riester</strong> (ahorro de impuestos/con subsidio del gobierno), la <strong>pensi&oacute;n b&aacute;sica</strong> (ahorro de impuestos, tambi&eacute;n llamada R&uuml;rup-Rente ), la <strong>pensi&oacute;n de la compa&ntilde;&iacute;a</strong> (ahorro de impuestos, provisi&oacute;n de jubilaci&oacute;n profesional = Betriebliche Altersvorsorge)</p>
    </div>
</div>