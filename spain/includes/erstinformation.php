<div class="container page" id="erstinformation">
    <div class="row">
        <div class="col-12 text-right">
            <a href="assets/files/Erstinformation.pdf" download class="btn btn-warning">Download</a>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
          <p>Traducci&oacute;n de ayuda de la</p>

          <p>Informaci&oacute;n inicial</p>

          <p><span style="color:#e74c3c">(S&oacute;lo la versi&oacute;n m&aacute;s abajo en alem&aacute;n es legalmente vinculante)</span></p>

          <p><strong>1. Su corredor de seguros</strong></p>

          <p>Su corredor tiene una licencia comercial seg&uacute;n el art&iacute;culo 34d p&aacute;rrafo 1 del GewO como corredor de seguros y est&aacute; registrado bajo el siguiente n&uacute;mero en el registro de corredores seg&uacute;n el art&iacute;culo 11a del GewO. Como corredor, es su persona de contacto para los asuntos de seguros acordados y personalmente responsable de su asesoramiento de acuerdo con los art&iacute;culos 60, 61 y 63 de la VVG.</p>

          <p>(1) Corredor de seguros</p>

          <p>Martina Mart&iacute;nez<br />
            Im M&uuml;hlengrund 11<br />
            56566 Neuwied<br />
            D-3KF9-A1I4B-40</p>

          <p>(2) Persona de contacto<br />
            Martina Mart&iacute;nez<br />
            Im M&uuml;hlengrund 11<br />
            56566 Neuwied<br />
            Fax.02631-9522749<br />
            mm@martina-martinez.de<br />
            D-3KF9-A1I4B-40</p>

          <p>(3) No hay participaci&oacute;n en o por parte de aseguradoras u otras compa&ntilde;&iacute;as matrices.</p>

          <p><strong>2. Su socio contractual</strong></p>

          <p>Su socio contractual es en todo momento la siguiente empresa y tiene una licencia comercial de acuerdo con el art&iacute;culo 34d p&aacute;rrafo 1 del GewO como corredor de seguros. En caso de no estar satisfecho en un caso concreto con el asesoramiento del corredor mencionado anteriormente, puede ponerse en contacto en todo momento con su socio contractual.</p>

          <p>(1) Socio contractual<br />
            Martina Mart&iacute;nez<br />
            Im M&uuml;hlengrund 11<br />
            56566 Neuwied<br />
            D-3KF9-A1I4B-40</p>

          <p>(2) No hay participaci&oacute;n en o por parte de aseguradoras u otras compa&ntilde;&iacute;as matrices.</p>

          <p>(3) Existe un seguro de responsabilidad profesional conforme a la ley. Esto ha sido verificado por la C&aacute;mara de Comercio e Industria (IHK). El registro se realiz&oacute; a trav&eacute;s de la C&aacute;mara de Comercio e Industria de Coblenza con el n&uacute;mero de registro D-3KF9-A1I4B-40.</p>

          <p><strong>3. Oferta de servicios de consultor&iacute;a</strong></p>

          <p>Al cliente se le ofrecer&aacute;n servicios de consultor&iacute;a sobre la cobertura de seguro deseada antes de la contrataci&oacute;n o firma de una p&oacute;liza de seguro. Si el cliente solicit&oacute; y recibi&oacute; asesoramiento se puede ver en la documentaci&oacute;n de asesoramiento o en la renuncia al asesoramiento por parte del cliente.&nbsp;</p>

          <p><strong>4. Informaci&oacute;n com&uacute;n</strong></p>

          <p>Si desea comprobar la inscripci&oacute;n en el registro de corretaje, puede hacerlo en este sitio web:</p>

          <p><a href="http://www.vermittlerregister.info"><span style="color:#3498db">www.vermittlerregister.info</span></a></p>

          <p>o en</p>

          <p>Tel&eacute;fono: (0 180) 60 05 85 0 (Precio fijo &euro;0,20 por llamada; precio m&aacute;ximo del tel&eacute;fono m&oacute;vil &euro;0,60 por llamada)</p>

          <p>o en</p>

          <p>DIHK e.V.<br />
            Breite Stra&szlig;e 29<br />
            10178 Berl&iacute;n<br />
            Tel.: (030) 20308-0<br />
            Sitio web: <a href="http://www.dihk.de"><span style="color:#3498db">www.dihk.de</span></a></p>

          <p>como &oacute;rganos competentes comunes seg&uacute;n el art&iacute;culo 11a del GewO.</p>

          <p>Si en alg&uacute;n momento no est&aacute; satisfecho con nuestros servicios, puede llamar a los siguientes &oacute;rganos extrajudiciales de reclamaci&oacute;n y recurso:</p>

          <p>Versicherungsombudsmann e.V., Postfach 080 632, 10006 Berlin<br />
            Ombudsmann Private Kranken- und Pflegeversicherung, Postfach 06 02 22, 10052 Berlin</p>

          <p>Resoluci&oacute;n de litigios online a trav&eacute;s de la UE<br />
            <a href="https://webgate.ec.europa.eu/odr"><span style="color:#3498db">https://webgate.ec.europa.eu/odr</span></a></p>

          <p><span style="color:#000000"><strong>5. Bases de los servicios de consultor&iacute;a y corretaje</strong></span></p>

          <p>Se informa al cliente de la base sobre la cual se prestan los servicios de consultor&iacute;a y corretaje del contrato:<br />
            servicios de consultor&iacute;a gratuitos para los clientes</p>

          <p><strong>6. Firma del cliente</strong></p>

          <p>Con la siguiente firma usted confirma que ha recibido y comprendido la informaci&oacute;n anteriormente mencionada.</p>

          <p><br />
            1.<strong> Ihr Vermittler</strong><br />
            Ihr Vermittler verf&uuml;gt &uuml;ber eine Gewerbeerlaubnis nach &sect; 34d Abs.1 GewO als Versicherungsmakler und ist unter der nachstehend genannten<br />
            Registernummer in das Vermittlerregister nach &sect; 11a GewO eingetragen. Er ist als Vermittler Ihr Ansprechpartner in den vereinbarten<br />
            Versicherungsangelegenheiten und pers&ouml;nlich verantwortlich f&uuml;r seine Beratung nach &sect;&sect; 60,61 und 63 VVG.</p>

          <p>(1) Vermittler<br />
            Martina Mart&iacute;nez<br />
            Im M&uuml;hlengrund 11<br />
            56566 Neuwied<br />
            D-3KF9-A1I4B-40</p>

          <p>(2) Ansprechpartner<br />
            Martina Mart&iacute;nez<br />
            Im M&uuml;hlengrund 11<br />
            56566 Neuwied<br />
            Fax.02631-9522749<br />
            mm@martina-martinez.de<br />
            D-3KF9-A1I4B-40</p>

          <p>(3) Es bestehen keine Beteiligungen an oder von Versicherern oder deren Muttergesellschaften.</p>

          <p>2.<strong> Ihr Vertragspartner</strong></p>

          <p>Ihr Vertragspartner ist stets die nachgenannte Gesellschaft und verf&uuml;gt &uuml;ber eine Gewerbeerlaubnis nach &sect; 34d Abs.1 GewO als<br />
            Versicherungsmakler. Sollten Sie mit der Beratung durch Ihren o.g. Vermittler im Einzelfall nicht zufrieden sein, so k&ouml;nnen Sie sich jederzeit an<br />
            Ihren Vertragspartner wenden.</p>

          <p>(1) Vertragspartner<br />
            Martina Mart&iacute;nez<br />
            Im M&uuml;hlengrund 11<br />
            56566 Neuwied<br />
            D-3KF9-A1I4B-40</p>

          <p>(2) Es bestehen keine Beteiligungen an oder von Versicherern oder deren Muttergesellschaften.</p>

          <p>(3) Es besteht eine gesetzeskonforme Verm&ouml;gensschadenhaftpflicht, diese wurde der IHK nachgewiesen. Die Registrierung erfolgte &uuml;ber die IHK Koblenz unter Registriernummer: D-3KF9-A1I4B-40</p>

          <p>3. <strong>Beratungsangebot</strong></p>

          <p>Dem Kunden wird eine Beratung &uuml;ber den gew&uuml;nschten Versicherungsschutz vor einer Vertragsvermittlung oder dem Abschluss eines<br />
            Versicherungsvertrages angeboten. Ob der Kunde eine Beratung gew&uuml;nscht und erhalten hatte, ergibt sich aus der Beratungsdokumentation<br />
            oder einer Beratungsverzichtserkl&auml;rung des Kunden.</p>

          <p>4. <strong>Gemeinsame Angaben</strong></p>

          <p>Sofern Sie de Eintragungen im Vermittlerregister &uuml;berpr&uuml;fen m&ouml;chten, so k&ouml;nnen Sie dies &uuml;ber die Internetseite&nbsp;</p>

          <p><a href="http://www.vermittlerregister.info"><span style="color:#3498db">www.vermittlerregister.info</span></a></p>

          <p>oder unter</p>

          <p>Telefon: (0 180) 60 05 85 0 (Festpreis 0,20 &euro;/Anruf; Mobilfunkpreise maximal 0,60 &euro;/Anruf)</p>

          <p>oder bei der</p>

          <p>DIHK e.V.<br />
            Breite Stra&szlig;e 29<br />
            10178 Berl&iacute;n<br />
            Tel&eacute;fono: (030) 20308-0<br />
            Internet: <a href="http://www.dihk.de"><span style="color:#3498db">www.dihk.de</span></a></p>

          <p>als registerf&uuml;hrende gemeinsame Stelle nach &sect; 11a GewO jederzeit veranlassen.</p>

          <p>Sofern Sie mit unseren Dienstleistungen einmal nicht zufrieden sein sollten, k&ouml;nnen Sie folgende Stellen als au&szlig;ergerichtliche<br />
            Schlichtungsstellen anrufen:<br />
            Versicherungsombudsmann e.V., Postfach 080 632, 10006 Berlin<br />
            Ombudsmann Private Kranken- und Pflegeversicherung, Postfach 06 02 22, 10052 Berlin<br />
            Online-Streitbeilegung via EU<br />
            <a href="https://webgate.ec.europa.eu/odr"><span style="color:#3498db">https://webgate.ec.europa.eu/odr</span></a><br />
            5. <strong>Grundlage der Beratung und Vermittlung</strong><br />
            Dem Mandanten/Kunden wird vom Vermittler hiermit mitgeteilt, auf welcher Grundlage die Beratung und Vermittlung des Vertrages erfolgt:<br />
            (1) Kostenfreie Beratung f&uuml;r den Kunden<br />
            6. <strong>Unterschrift Mandant</strong></p>

          <p>Mit der nachfolgenden Unterschrift best&auml;tigen Sie die vorgenannten Informationen erhalten und verstanden zu haben.</p>
        </div>
    </div>
</div>