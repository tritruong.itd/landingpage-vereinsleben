<?php
if(!isset($_SESSION))
{
    session_start();
}
@define('_MAIL_IP', 'smtp.office365.com');
@define('_MAIL_USER', 'mm@martina-martinez.de');
@define('_MAIL_PWD', 'Delacaridad5');
function dd(...$datas) {
    foreach($datas as $data) {
        echo '<pre>';
        print_r($data);
        echo '</pre>';
    }
    die();
}
function dump(...$datas) {
    foreach($datas as $data) {
        echo '<pre>';
        print_r($data);
        echo '</pre>';
    }
}
if (!function_exists('checkExistCart')) {
    function checkExistCart($id) {
        $ret = false;
        if (!isset($_SESSION['cart'])) return false;
        foreach ($_SESSION['cart'] as $cart) {
            if ($cart['id'] == $id) {
                $ret = true;
            }
        }
        return $ret;
    }
}
if (!function_exists('checkExistData')) {
    function checkExistData($id) {
        $ret = false;
        if (!isset($_SESSION['cart_tmp'])) return false;
        foreach ($_SESSION['cart_tmp'] as $cart) {
            if ($cart['id'] == $id) {
                $ret = true;
            }
        }
        return $ret;
    }
}
if (!function_exists('checkExistCartHealth')) {
    function checkExistCartHealth($id) {
        $ret = false;
        if (!isset($_SESSION['cart_health'])) return false;
        foreach ($_SESSION['cart_health'] as $cart) {
            if ($cart['id'] == $id) {
                $ret = true;
            }
        }
        return $ret;
    }
}
if (!function_exists('getCartById')) {
    function getCartById($id) {
        if (!isset($_SESSION['cart'])) return false;
        foreach ($_SESSION['cart'] as $cart) {
            if ($cart['id'] == $id) {
                return json_decode(json_encode($cart), true);
            }
        }
        return null;
    }
}
if (!function_exists('getCartDataById')) {
    function getCartDataById($id) {
        if (!isset($_SESSION['cart_tmp'])) return false;
        foreach ($_SESSION['cart_tmp'] as $cart) {
            if ($cart['id'] == $id) {
                return json_decode(json_encode($cart), true);
            }
        }
        return null;
    }
}

if (!function_exists('getTotalCart')) {
    function getTotalCart() {
        if (!isset($_SESSION['cart'])) return 0;
        return count($_SESSION['cart']);
    }
}


if (!function_exists('getTotalCartHealth')) {
    function getTotalCartHealth() {
        if (!isset($_SESSION['cart_health'])) return 0;
        return count($_SESSION['cart_health']);
    }
}


if (!function_exists('getTotalCartData')) {
    function getTotalCartData() {
        if (!isset($_SESSION['cart_tmp'])) return 0;
        return count($_SESSION['cart_tmp']);
    }
}

if (!function_exists('getItemCart')) {
    function getItemCart() {
        if (!isset($_SESSION['cart'])) return 0;
        return $_SESSION['cart'];
    }
}

if (!function_exists('addItem')) {
    function addItem($id, $name, $value) {
        if (!isset($_SESSION['cart'])) {
            $_SESSION['cart'][] = array(
                'id' => $id,
                'name' => $name,
                'value' => $value
            );
        } else {
            if (!checkExistCart($id)) {
                $_SESSION['cart'][] = array(
                    'id' => $id,
                    'name' => $name,
                    'value' => $value
                );
            }
        }
        echo getTotalCart();
    }
}

if (!function_exists('addItemData')) {
    function addItemData($id, $name, $value) {
        if (!isset($_SESSION['cart_tmp'])) {
            $_SESSION['cart_tmp'][] = array(
                'id' => $id,
                'name' => $name,
                'value' => $value
            );
        } else {
            if (!checkExistData($id)) {
                $_SESSION['cart_tmp'][] = array(
                    'id' => $id,
                    'name' => $name,
                    'value' => $value
                );
            }
        }
        echo getTotalCartData();
    }
}

if (!function_exists('addItemHealth')) {
    function addItemHealth($id, $name, $value) {
        if (!isset($_SESSION['cart_health'])) {
            $_SESSION['cart_health'][] = array(
                'id' => $id,
                'name' => $name,
                'value' => $value
            );
        } else {
            if (!checkExistCartHealth($id)) {
                $_SESSION['cart_health'][] = array(
                    'id' => $id,
                    'name' => $name,
                    'value' => $value
                );
            }
        }
        echo getTotalCartHealth();
    }
}

if (!function_exists('updateItem')) {
    function updateItem($id, $name, $data) {
        if (!checkExistCart($id)) {
          addItem($id, $name, $data);
        } else {
          foreach ($_SESSION['cart'] as $key => $cart) {
            if ($cart['id'] == $id) {
              $_SESSION['cart'][$key]['value'] = $data;
            }
          }
          echo getTotalCart();
        }
    }
}

if (!function_exists('updateItemHealth')) {
    function updateItemHealth($id, $name, $data) {
        if (!checkExistCartHealth($id)) {
          addItemHealth($id, $name, $data);
        } else {
          foreach ($_SESSION['cart'] as $key => $cart) {
            if ($cart['id'] == $id) {
              $_SESSION['cart_health'][$key]['value'] = $data;
            }
          }
          echo getTotalCartHealth();
        }
    }
}

if (!function_exists('keepDataWhenCloseModal')) {
    function keepDataWhenCloseModal($id, $name, $data) {
        if (!checkExistData($id)) {
          addItemData($id, $name, $data);
        } else {
          foreach ($_SESSION['cart_tmp'] as $key => $cart) {
            if ($cart['id'] == $id) {
              $_SESSION['cart_tmp'][$key]['value'] = $data;
            }
          }
          echo getTotalCartData();
        }
    }
}

if (!function_exists('removeItem')) {
    function removeItem($id) {
        if (count($_SESSION['cart'])) {
            foreach ($_SESSION['cart'] as $key => $cart) {
                if ($cart['id'] == $id) {
                    unset($_SESSION['cart'][$key]);
                }
            }
        }
        echo getTotalCart();
    }
}

if (!function_exists('removeItems')) {
    function removeItems($arrCart) {
        foreach ($_SESSION['cart'] as $key => $cartItem) {
            if (in_array($cartItem['id'], $arrCart)) {
                unset($_SESSION['cart'][$key]);
            }
        }
        echo json_encode(array('total' => getTotalCart(), 'listId' => $arrCart));
    }
}

if (!function_exists('sendEmail')) {
    function sendEmail($to, $subject, $body, $from) {
        include_once "phpmailer/class.phpmailer.php";
        try {
            $mail = new PHPMailer();
            $mail->SetLanguage("en");
            $mail->IsSMTP();
            $mail->SMTPAuth = true;
            $mail->Host = _MAIL_IP;
            $mail->Username = _MAIL_USER;
            $mail->Password = _MAIL_PWD;
            $mail->Port = 587;
            $mail->SMTPSecure = 'tls';
            $mail->isHTML(true);
            $mail->SetFrom(_MAIL_USER, "No Reply");

            $mail->AddAddress($to, $from);
            $mail->Subject = $subject;
            $mail->CharSet = "utf-8";

            $mail->AltBody = "To view the message, please use an HTML compatible email viewer!";
            $mail->MsgHTML($body);
            $mail->Send();
        } catch (Exception $e) {
            dd($e);
        }
    }
}

if (!function_exists('getFormFilled')) {
    function getFormFilled() {
        $arrItem = array();
        foreach ($_SESSION['cart'] as $key => $cart) {
            switch ($cart['id']) {
                case 'liability-insurance':
                    if ( isset($cart['value']['payment'])  && isset($cart['value']['prior-liability']) && (isset($cart['value']['claim']) && $cart['value']['claim'] != '')) {
                        $arrItem[] = 'liability-insurance';
                    }
                    break;
                case 'household-insurance':
                    if ( (isset($cart['value']['size-of-living-area']) && $cart['value']['size-of-living-area'] != '') && isset($cart['value']['building']) &&
                        (isset($cart['value']['apartment']) && $cart['value']['apartment'] != '' ) && isset($cart['value']['breakage-of-glass']) &&
                        isset($cart['value']['bicycles-theft']) && isset($cart['value']['household-against']) && (isset($cart['value']['bicycles-theft-value']) && $cart['value']['bicycles-theft-value'] != '') &&
                        isset($cart['value']['household-content']) && isset($cart['value']['payment']) &&
                        (isset($cart['value']['claim']) && $cart['value']['claim'] != '')
                    ){
                        $arrItem[] = 'household-insurance';
                    }
                    break;
                case 'legal-protection-insurance':
                    if (isset($cart['value']['cover-law']) && isset($cart['value']['payment']) && isset($cart['value']['legal-expenses-insurance']) && (isset($cart['value']['claim']) && $cart['value']['claim'] != '')) {
                        $arrItem[] = 'legal-protection-insurance';
                    }
                    break;
                case 'car-insurance':
                    if (isset($cart['value']['the-car']) && isset($cart['value']['desired-coverage'])) {
                        $arrItem[] = 'car-insurance';
                    }
                    break;
                case 'travel-insurance':
                    if (isset($cart['value']['travel-health-insurance']) && (isset($cart['value']['your-age']) && $cart['value']['your-age'] != '') && (isset($cart['value']['trip-for']) && $cart['value']['trip-for'] != '') &&
                        isset($cart['value']['travel-period-from']) && isset($cart['value']['travel-period-to']) && isset($cart['value']['booking-date']) &&
                        isset($cart['value']['direction-travel']) && isset($cart['value']['travel-destination']) && isset($cart['value']['reasion-of-travel'])
                    ) {
                        $arrItem[] = 'travel-insurance';
                    }
                    break;
                case 'term-life-insurance':
                    if ((isset($cart['value']['benefit']) && $cart['value']['benefit'] != '') && (isset($cart['value']['insurance-period']) && $cart['value']['insurance-period'] != '') && isset($cart['value']['iam']) && isset($cart['value']['visa-status']) && isset($cart['value']['payment'])) {
                        $arrItem[] = 'term-life-insurance';
                    }
                    break;
                case 'health-insurance':

                    if (isset($cart['value']['private-health-insurance']) || isset($cart['value']['public-health-insurance']) || isset($cart['value']['supplementary-health-insurance'])) {
                        $arrItem[] = 'health-insurance';
                    }
                    break;
                case 'pension-insurance':
                    if (isset($cart['value']['retirement-begin']) && isset($cart['value']['monthly-payment']) && $cart['value']['pension-plan'] &&
                        (isset($cart['value']['current-gross']) && $cart['value']['current-gross'] != '') && isset($cart['value']['my-tax-class']) && isset($cart['value']['church-tax']) &&
                        isset($cart['value']['pay-contribution']) && isset($cart['value']['family-status']) && (isset($cart['value']['number-of-children']) && $cart['value']['number-of-children'] != '') &&
                        isset($cart['value']['riester-subsidy']) && isset($cart['value']['born-until-end-of-2007']) && isset($cart['value']['born-from-2008']) &&
                        (isset($cart['value']['gross-income']) && $cart['value']['gross-income'] != '') && isset($cart['value']['additional-information'])
                    ) {
                        $arrItem[] = 'pension-insurance';
                    }
                    break;
                case 'accident-disability-insurance':
                    if (isset($cart['value']['iam']) && isset($cart['value']['disability']) || (isset($cart['value']['disability-pension']) && $cart['value']['disability-pension'] != '') &&
                        isset($cart['value']['desired-disability-benefit']) && isset($cart['value']['desired-death-benefit']) &&
                        isset($cart['value']['desired-daily-hospital-allowance']) && isset($cart['value']['desired-monthly-accident-pension'])
                    ) {
                        $arrItem[] = 'accident-disability-insurance';
                    }
                    break;
                default:
                    break;
            }
        }
        return $arrItem;
    }
}

if (!function_exists('checkFormFilled')) {
    function checkFormFilled($id, $data) {
        $ret = 0;
        switch ($id) {
            case 'liability-insurance':
                $count = 0;
                foreach($data as $item) {
                  if ($item['name'] == 'payment' && $item['value'] != '') {
                      $count++;
                  }
                  if ($item['name'] == 'prior-liability' && $item['value'] != '') {
                      $count++;
                  }
                  if ($item['name'] == 'claim' && $item['value'] != '') {
                      $count++;
                  }
                }
                if ($count == 3) {
                    $ret = 1;
                }
                break;
            case 'household-insurance':
                $count = 0;
                $total = 3;
                foreach($data as $item) {
                    if ($item['name'] == 'size-of-living-area' && $item['value'] != '') {
                        $count++;
                    }
                    if ($item['name'] == 'payment' && $item['value'] != '') {
                        $count++;
                    }
                    if ($item['name'] == 'claim' && $item['value'] != '') {
                        $count++;
                    }
                    if ($item['building'] == 'Apartment building') {
                        if ($item['name'] == 'apartment' && $item['value'] != '') {
                            $count++;
                            $total = 4;
                        }
                    }
                }
                if ($count == $total) {
                  $ret = 1;
                }
                break;
            case 'legal-protection-insurance':
                $count = 0;
                $cover = 0;
                foreach($data as $item) {
                    if ($item['name'] == 'legal-expenses-insurance' && $item['value'] != '') {
                        $count++;
                    }
                    if ($item['name'] == 'payment' && $item['value'] != '') {
                        $count++;
                    }
                    if ($item['name'] == 'claim' && $item['value'] != '') {
                        $count++;
                    }
                    if ($item['name'] == 'cover-law[]') {
                        $cover++;
                    }
                }
                if ($count == 3 && $cover > 0) {
                    $ret = 1;
                }
                break;
            case 'car-insurance':
                $ret = 1;
                break;
            case 'travel-insurance':
                $count = 0;
                $insurance = 0;
                foreach($data as $item) {
                    if ($item['name'] == 'your-age' && $item['value'] != '') {
                        $count++;
                    }
                    if ($item['name'] == 'trip-for' && $item['value'] != '') {
                        $count++;
                    }
                    if ($item['name'] == 'value-of-the-trip' && $item['value'] != '') {
                        $count++;
                    }
                    if ($item['name'] == 'travel-period-from' && $item['value'] != '') {
                        $count++;
                    }
                    if ($item['name'] == 'travel-period-to' && $item['value'] != '') {
                        $count++;
                    }
                    if ($item['name'] == 'booking-date' && $item['value'] != '') {
                        $count++;
                    }
                    if ($item['name'] == 'direction-travel' && $item['value'] != '') {
                        $count++;
                    }
                    if ($item['name'] == 'travel-destination' && $item['value'] != '') {
                        $count++;
                    }
                    if ($item['name'] == 'reasion-of-travel' && $item['value'] != '') {
                        $count++;
                    }
                    if ($item['name'] == 'travel-health-insurance[]') {
                        $insurance++;
                    }
                }
                if ($count == 9 && $insurance > 0) {
                    $ret = 1;
                }
                break;
            case 'term-life-insurance':
                $count = 0;
                foreach($data as $item) {
                    if ($item['name'] == 'benefit' && $item['value'] != '') {
                        $count++;
                    }
                    if ($item['name'] == 'insurance-period' && $item['value'] != '') {
                        $count++;
                    }
                    if ($item['name'] == 'iam' && $item['value'] != '') {
                        $count++;
                    }
                    if ($item['name'] == 'visa-status' && $item['value'] != '') {
                        $count++;
                    }
                    if ($item['name'] == 'payment' && $item['value'] != '') {
                        $count++;
                    }
                }
                if ($count == 5) {
                    $ret = 1;
                }
                break;

            case 'accident-disability-insurance':
                $count = 0;
                foreach($data as $item) {
                    if (checkFieldChecked($data, 'frm-disability')) {
                        if ($item['name'] == 'disability' && $item['value'] != '') {
                            $count++;
                        }
                    }
                    if (checkFieldChecked($data, 'frm-accident')) {
                        if (!checkFieldChecked($data, 'desired-disability-benefit-check')) {
                            if ($item['name'] == 'desired-disability-benefit' && $item['value'] != '') {
                                $count++;
                            }
                        }
                    }
                }
                $total = 1;
                if (checkFieldChecked($data, 'frm-disability') && checkFieldChecked($data, 'frm-accident') && !checkFieldChecked($data, 'desired-disability-benefit-check')) {
                    $total = 2;
                } else if (checkFieldChecked($data, 'frm-disability') && checkFieldChecked($data, 'frm-accident') && checkFieldChecked($data, 'desired-disability-benefit-check')) {
                    $total = 1;
                }
                if ($count == $total) {
                    $ret = 1;
                }
                break;
            case 'health-insurance':
                $count = 0;
                $cover = 0;
                foreach($data as $item) {
                    if (checkFieldChecked($data, 'public-health-insurance')) {
                        if ($item['name'] == 'occupation-group-public' && $item['value'] != '') {
                            $count++;
                        }
                        if ($item['name'] == 'gross-income-per-month' && $item['value'] != '') {
                            $count++;
                        }
                        if ($item['name'] == 'i-have-children' && $item['value'] != '') {
                            $count++;
                        }
                        if ($item['name'] == 'current-health-insurance-public' && $item['value'] != '') {
                            $count++;
                        }
                    }
                    if (checkFieldChecked($data, 'supplementary-health-insurance')) {
                        if ($item['name'] == 'outpatient-coverage[]') {
                            $cover++;
                        }
                    }
                }
                if (checkFieldChecked($data, 'public-health-insurance') && !checkFieldChecked($data, 'supplementary-health-insurance') && !checkFieldChecked($data, 'private-health-insurance')) {
                    $total = 4;
                    if ($count == $total) {
                        $ret = 1;
                    }
                } else if (!checkFieldChecked($data, 'supplementary-health-insurance') && !checkFieldChecked($data, 'public-health-insurance') && checkFieldChecked($data, 'private-health-insurance')) {
                    $ret = 1;
                } else if (checkFieldChecked($data, 'supplementary-health-insurance') && !checkFieldChecked($data, 'public-health-insurance') && !checkFieldChecked($data, 'private-health-insurance')) {
                    if ($cover > 0) {
                        $ret = 1;
                    }
                } else if (checkFieldChecked($data, 'public-health-insurance') && checkFieldChecked($data, 'supplementary-health-insurance') && checkFieldChecked($data, 'private-health-insurance')) {
                    $total = 4;
                    if ($count == $total && $cover > 0) {
                        $ret = 1;
                    }
                }
                break;
            case 'pension-insurance':
                $count = 0;
                foreach($data as $item) {
                    if (checkFieldChecked($data, 'pension-plan')) {
                        if ($item['name'] == 'current-gross' && $item['value'] != '') {
                            $count++;
                        }
                        if ($item['name'] == 'my-tax-class' && $item['value'] != '') {
                            $count++;
                        }
                        if ($item['name'] == 'church-tax' && $item['value'] != '') {
                            $count++;
                        }
                        if ($item['name'] == 'pay-contribution' && $item['value'] != '') {
                            $count++;
                        }
                        if ($item['name'] == 'family-status' && $item['value'] != '') {
                            $count++;
                        }
                    }
                    if (checkFieldChecked($data, 'riester-subsidy')) {
                        if ($item['name'] == 'number-of-children' && $item['value'] != '') {
                            $count++;
                        }
                        if ($item['name'] == 'born-until-end-of-2007' && $item['value'] != '') {
                            $count++;
                        }
                        if ($item['name'] == 'born-from-2008' && $item['value'] != '') {
                            $count++;
                        }
                        if ($item['name'] == 'gross-income' && $item['value'] != '') {
                            $count++;
                        }
                    }
                }
                $total = 0;
                if (checkFieldChecked($data, 'pension-plan') && !checkFieldChecked($data, 'riester-subsidy')) {
                    $total = 5;
                } else if (checkFieldChecked($data, 'pension-plan') && checkFieldChecked($data, 'riester-subsidy')) {
                    $total = 9;
                }
                if ($count == $total) {
                    $ret = 1;
                }
                break;
            default:
                break;
        }
      echo $ret;
    }
}

function checkFieldChecked($data, $field) {
    $ret = false;
    foreach($data as $item) {
        if ($item['name'] == $field) {
            $ret = true;
        }
    }
    return $ret;
}

if (!function_exists('getBodyMail')) {
    function getBodyMail($key, $cart) {
        if ($key)
            $body = '<p style="border-top: 1px solid #ccc; padding-top: 10px;">';
        else
            $body = '<p>';
        switch ($cart['id']) {
            case 'liability-insurance':
                $body .= '<p><h1><strong>Liability Insurance:</strong></h1></p>';
                $body .= '<p><strong>Payment:</strong> '. $cart['value']['payment'] .'</p>';
                $body .= '<p><strong>Prior liability insurance?:</strong> '. $cart['value']['prior-liability'] .'</p>';
                $body .= '<p><strong>Number of claims in the past 5 years:</strong> '. $cart['value']['claim'] .' claims</p>';
                break;
            case 'household-insurance':
                $body .= '<p><h1><strong>Household Insurance:</strong></h1></p>';
                $body .= '<p><strong>Size of living area in square meters:</strong> '. $cart['value']['size-of-living-area'] .' m²</p>';
                $body .= '<p><strong>Type of building:</strong> '. $cart['value']['building'] .'</p>';
                $body .= '<p><strong>Number apartments in your house:</strong> '. $cart['value']['apartment'] .'</p>';
                $body .= '<p><strong>Insure breakage of glass:</strong> '. $cart['value']['breakage-of-glass'] .'</p>';
                $body .= '<p><strong>Insure your bicycles against theft:</strong> '. $cart['value']['bicycles-theft'] .'</p>';
                if ($cart['value']['bicycles-theft'] == 'yes') {
                    $body .= '<p><strong>Value of your bicycles:</strong> ' . $cart['value']['bicycles-theft-value'] . '</p>';
                }
                $body .= '<p><strong>Insure your household items against natural hazards:</strong> '. $cart['value']['household-against'] .'</p>';
                $body .= '<p><strong>Payment:</strong> '. $cart['value']['payment'] .'</p>';
                $body .= '<p><strong>Prior household content insurance:</strong> '. $cart['value']['household-content'] .'</p>';
                $body .= '<p><strong>Number of claims in the past 5 years:</strong> '. $cart['value']['claim'] .' claims</p>';
                break;
            case 'legal-protection-insurance':
                $body .= '<p><h1><strong>Legal Protection Insurance:</strong></h1></p>';
                $body .= '<p><strong>I want to cover:</strong> '. $cart['value']['cover-law'] .'</p>';
                $body .= '<p><strong>Payment:</strong> '. $cart['value']['payment'] .'</p>';
                $body .= '<p><strong>Prior legal expenses insurance?:</strong> '. $cart['value']['legal-expenses-insurance'] .'</p>';
                $body .= '<p><strong>Number of claims in the past 5 years:</strong> '. $cart['value']['claim'] .' claims</p>';
                break;
            case 'car-insurance':
                $desiredCoverages = array(
                    'only-liability' => 'Only liability',
                    'liability-and-partial-cover' => 'Liability and partial cover',
                    'liability-and-comprehensive-cover' => 'Liability and comprehensive cover'
                );
                $body .= '<p><h1><strong>Car Insurance:</strong></h1></p>';
                $body .= '<p><strong>The car:</strong> '. $cart['value']['the-car'] .'</p>';
                $body .= '<p><strong>Desired coverage:</strong> '. $desiredCoverages[$cart['value']['desired-coverage']] .'</p>';
                break;
            case 'travel-insurance':
                $reasionOfTravels = array(
                    'private' => 'Private',
                    'business' => 'Business',
                    'private-business' => 'Private & Business',
                    'study' => 'Study',
                    'au-pair' => 'Au-pair',
                    'work-travel' => 'Work & travel',
                );
                $travelDestinations = array(
                    'germany' => 'Germany',
                    'europe' => 'Europe',
                    'including-usa-canada' => 'Worldwide including USA & Canada',
                    'without-usa-canada' => 'Worldwide without USA & Canada'
                );
                $directionTravels = array(
                    'from-germany-abroad' => 'From Germany abroad',
                    'inside-germany' => 'Inside Germany',
                    'from-abroad-to-germany' => 'From abroad to Germany'
                );
                $tripFors = array(
                    'single-person' => 'Single person',
                    'couple' => 'Couple',
                    'family' => 'Family'
                );
                $body .= '<p><h1><strong>Travel Insurance:</strong></h1></p>';
                $body .= '<p><strong>I want to insure:</strong> '. $cart['value']['travel-health-insurance'] .'</p>';
                $body .= '<p><strong>Your age (eldest person of travelling group):</strong> '. $cart['value']['your-age'] .'</p>';
                $body .= '<p><strong>Trip is for:</strong> '. $tripFors[$cart['value']['trip-for']] .'</p>';
                $body .= '<p><strong>Value of the trip in €:</strong> '. $cart['value']['value-of-the-trip'] .'</p>';
                $body .= '<p><strong>Travel period from:</strong> '. $cart['value']['travel-period-from'] .'</p>';
                $body .= '<p><strong>Travel period to:</strong> '. $cart['value']['travel-period-to'] .'</p>';
                $body .= '<p><strong>Booking date of the travel:</strong> '. $cart['value']['booking-date'] .'</p>';
                $body .= '<p><strong>Direction of the travel:</strong> '. $directionTravels[$cart['value']['direction-travel']] .'</p>';
                $body .= '<p><strong>Travel destination:</strong> '. $travelDestinations[$cart['value']['travel-destination']] .'</p>';
                $body .= '<p><strong>Reason of travel:</strong> '. $reasionOfTravels[$cart['value']['reasion-of-travel']] .'</p>';
                break;
            case 'term-life-insurance':
                $body .= '<p><h1><strong>Term Life Insurance:</strong></h1></p>';
                $body .= '<p><strong>Desired death benefit in €</strong> '. $cart['value']['benefit'] .'</p>';
                $body .= '<p><strong>Insurance period in years:</strong> '. $cart['value']['insurance-period'] .'</p>';
                $body .= '<p><strong>I am:</strong> '. $cart['value']['iam'] .'</p>';
                $body .= '<p><strong>Visa status:</strong> '. $cart['value']['visa-status'] .'</p>';
                $body .= '<p><strong>Payment:</strong> '. $cart['value']['payment'] .'</p>';
                break;
            case 'health-insurance':
                $body .= '<p><h1><strong>Desired offer for:</strong></h1></p>';
                if (isset($cart['value']['private-health-insurance']) && $cart['value']['private-health-insurance'] == 'on') {
                    $body .= '<p><h3><strong>Private health insurance</strong></h3></p>';
                    $body .= '<hr/>';
                    $body .= '<p><strong>Current health insurance:</strong> ' . $cart['value']['current-health-insurance-private'] . '</p>';
                    $body .= '<p><strong>Occupation group:</strong> ' . $cart['value']['occupation-group-private'] . '</p>';
                    $body .= '<p><strong>I have</strong> ' . $cart['value']['i-have'] . '</p>';
                    $body .= '<p><strong>Your desired coverage:</strong></p>';
                    $body .= '<p><strong>Hospital accommodation</strong> ' . $cart['value']['hospital-accommodation'] . '</p>';
                    $body .= '<p><strong>Daily hospital allowance</strong> €' . $cart['value']['daily-hospital-allowance'] . '</p>';
                    $body .= '<p><strong>Dental coverage</strong> ' . $cart['value']['dental-converage'] . '</p>';
                    $body .= '<p><strong>Sickness pay per month</strong> ' . $cart['value']['sickness-pay-per-month'] . '</p>';
                    if (isset($cart['value']['sickness-pay-per-month-add']) && $cart['value']['sickness-pay-per-month'] == 'Individual sickness pay') {
                        $body .= '<p><strong>Additional information</strong> ' . $cart['value']['sickness-pay-per-month-add'] . '</p>';
                    }
                    $body .= '<p><strong>Deductible per year</strong> ' . $cart['value']['deductible-per-year'] . '</p>';
                }
                if (isset($cart['value']['public-health-insurance']) && $cart['value']['public-health-insurance'] == 'on') {
                    $body .= '<hr/>';
                    $body .= '<p><h3><strong>Public health insurance</strong></h3></p>';
                    $body .= '<hr/>';
                    $body .= '<p><strong>Occupation group:</strong> ' . $cart['value']['occupation-group-public'] . '</p>';
                    $body .= '<p><strong>Gross income per month: €</strong> ' . $cart['value']['gross-income-per-month'] . '</p>';
                    $body .= '<p><strong>I have children:</strong> ' . $cart['value']['i-have-children'] . '</p>';
                    $body .= '<p><strong>Current health insurance</strong> ' . $cart['value']['current-health-insurance-public'] . '</p>';
                }
                if (isset($cart['value']['supplementary-health-insurance']) && $cart['value']['supplementary-health-insurance'] == 'on') {
                    $body .= '<hr/>';
                    $body .= '<p><h3><strong>Supplementary health insurance</strong></h3></p>';
                    $body .= '<hr/>';
                    $body .= '<p><strong>Your desired coverage:</strong></p>';
                    if (isset($cart['value']['supplementary-hospital-accommodation']) && $cart['value']['supplementary-hospital-accommodation'] == 'on') {
                        $body .= '<p><strong>Hospital accommodation:</strong> ' . $cart['value']['supplementary-hospital-accommodation-value'] . '</p>';
                    }
                    if (isset($cart['value']['supplementary-dental-coverage']) && $cart['value']['supplementary-dental-coverage'] == 'on') {
                        $body .= '<p><strong>Dental coverage:</strong> ' . $cart['value']['dental-coverage'] . '</p>';
                    }
                    $body .= '<p><strong>Outpatient coverage</strong> ' . $cart['value']['outpatient-coverage'] . '</p>';
                    if (isset($cart['value']['financial-aid']) && $cart['value']['financial-aid'] == 'on') {
                        $body .= '<p><strong>Financial aid: </strong>';
                        if (isset($cart['value']['daily-sickness']) && $cart['value']['daily-sickness'] == 'on') {
                            $body .= '<span>daily sickness allowance (closes the gap between sickness pay of public health insurance and your usual net income)</span></p>';
                          $body .= '<p><strong>Daily Sickness Cost:</strong> €' . $cart['value']['daily-sickness-cost'] . '</p>';
                        }
                        if (isset($cart['value']['daily-hospital']) && $cart['value']['daily-hospital'] == 'on') {
                            $body .= '<span>daily hospital allowance (pays every day you stay in hospital to cover the additional costs of your hospital stay)</span></p>';
                            $body .= '<p><strong>Daily Hospital Cost:</strong> €' . $cart['value']['daily-hospital-cost'] . '</p>';
                        }
                    }
                    $body .= '<p><strong>Additional information:</strong> ' . $cart['value']['additional-information-health'] . '</p>';
                }
                break;
            case 'pension-insurance':
                $body .= '<p><h1><strong>Pension Insurance:</strong></h1></p>';
                $body .= '<p><strong>Retirement begin:</strong> '. $cart['value']['retirement-begin'] .'</p>';
                $body .= '<p><strong>Monthly payment:</strong> €'. $cart['value']['monthly-payment'] .'</p>';
                if (isset($cart['value']['pension-plan']) && $cart['value']['pension-plan'] == 'on') {
                    $body .= '<p>I am interested in saving taxes with my pension plan</p>';
                    $body .= '<p><strong>Current gross income per year in €:</strong> ' . $cart['value']['current-gross'] . '</p>';
                    $body .= '<p><strong>My tax class:</strong> '. $cart['value']['my-tax-class'] .'</p>';
                    $body .= '<p><strong>I pay church tax:</strong> '. $cart['value']['church-tax'] .'</p>';
                    $body .= '<p><strong>I pay contributions to the state pension insurance:</strong> '. $cart['value']['pay-contribution'] .'</p>';
                    $body .= '<p><strong>Family status:</strong> '. $cart['value']['family-status'] .'</p>';
                    if (isset($cart['value']['number-of-children']) && $cart['value']['number-of-children'] != '') {
                        $body .= '<p><strong>Number of children for whom you receive "Kindergeld":</strong> ' . $cart['value']['number-of-children'] . '</p>';
                    }
                    if (isset($cart['value']['riester-subsidy']) && $cart['value']['riester-subsidy'] == 'on') {
                        $body .= '<p><strong>Born until end of 2007:</strong> ' . $cart['value']['born-until-end-of-2007'] . '</p>';
                        $body .= '<p><strong>Born from 2008:</strong> ' . $cart['value']['born-from-2008'] . '</p>';
                        $body .= '<p><strong>Last year\'s gross income (?) of the person to be insured in €:</strong> '. $cart['value']['gross-income'] .'</p>';
                    }
                    $body .= '<p><strong>Additional information:</strong> '. $cart['value']['additional-information'] .'</p>';
                }
                break;
            case 'accident-disability-insurance':
                if (isset($cart['value']['frm-disability']) && $cart['value']['frm-disability'] == 'on') {
                    $body .= '<p><h1><strong>Accident Bisability Insurance:</strong></h1></p>';
                    $body .= '<p><strong><i>For Disability insurance:</i></strong></p>';
                    $body .= '<p><strong>I am:</strong> ' . $cart['value']['iam'] . '</p>';
                    $body .= '<p><strong>Desired monthly disability pension in €:</strong> ' . $cart['value']['disability'] . '</p>';
                    $body .= '<p><strong>The disability pension should be paid until the age of:</strong> ' . $cart['value']['disability-pension'] . '</p>';
                }
                if (isset($cart['value']['frm-accident']) && $cart['value']['frm-accident'] == 'on') {
                    $body .= '<p><strong><i>For Disability insurance:</i></strong></p>';
                    if (isset($cart['value']['desired-disability-benefit-check']) && $cart['value']['desired-disability-benefit-check'] == 'I don´t know. Please suggest a disability benefit') {
                        $body .= '<p>I don´t know. Please suggest a disability benefit</p>';
                    } else if ($cart['value']['desired-disability-benefit'] != '') {
                        $body .= '<p><strong>Desired disability benefit:</strong> ' . $cart['value']['desired-disability-benefit'] . '</p>';
                    }
                    $body .= '<p><strong>Desired death benefit:</strong> ' . $cart['value']['desired-death-benefit'] . '</p>';
                    $body .= '<p><strong>Desired daily hospital allowance:</strong> ' . $cart['value']['desired-daily-hospital-allowance'] . '</p>';
                    $body .= '<p><strong>Desired monthly accident pension:</strong> ' . $cart['value']['desired-monthly-accident-pension'] . '</p>';
                }
                break;
            default:
                break;
        }
        $body .= '</p>';
        return $body;
    }
}

if (!function_exists('getValueByKey')) {
    function getValueByKey($data, $key) {
        $ret = '';
        if (count($data)) {
            foreach ($data as $v) {
                if ($v['name'] == $key) {
                    $ret = $v['value'];
                }
            }
        }
        return $ret;
    }
}

if (!function_exists('getHealthDataById')) {
    function getHealthDataById($id) {
        $data = null;
        foreach($_SESSION['cart_health'] as $item) {
            if ($item['id'] == $id) {
                $data = $item;
            }
        }
        return $data;
    }
}

if (!function_exists('getAnswerByKey')) {
    function getAnswerByKey($data, $key) {
        $ret = '';
        if (count($data)) {
            foreach ($data as $index => $v) {
                if ($index == $key) {
                    $ret = $v;
                }
            }
        }
        return $ret;
    }
}