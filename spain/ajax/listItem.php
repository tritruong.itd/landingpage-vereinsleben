<?php
include_once '../libraries/functions.php';

$itemCarts = getItemCart();
if($itemCarts) {
    foreach ($itemCarts as $item) {
        ?><li class="item btn-edit"><span
                data-id="<?php echo $item['id'] ?>"><?php echo $item['name'] ?></span></li><?php
    }
    echo '<li class="item btn-edit"><button data-id="" class="btn btn-sm btn-submit btn-success btn-block">Enviar solicitud </button></li>';
}