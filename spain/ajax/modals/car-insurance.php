<?php
include_once '../../libraries/functions.php';
if(!isset($_SESSION))  {
    session_start();
}
$cart = getCartById('car-insurance');
$cart_tmp = getCartDataById('car-insurance');

if ($cart) {
    $theCar = isset($cart['value']) ? $cart['value']['the-car'] : '';
    $desiredCoverage = isset($cart['value']) ? $cart['value']['desired-coverage'] : '';
} else if ($cart_tmp) {
    $theCar = isset($cart_tmp['value']) ? $cart_tmp['value']['the-car'] : '';
    $desiredCoverage = isset($cart_tmp['value']) ? $cart_tmp['value']['desired-coverage'] : '';
} else {
    $theCar = '';
    $desiredCoverage = '';
}
$desiredCoverages = array(
    'only-liability' => 'sólo responsabilidad civil',
    'liability-and-partial-cover' => 'responsabilidad civil y cobertura parcial',
    'liability-and-comprehensive-cover' => 'responsabilidad civil y cobertura integral'
);
?>
<form method="post" id="frm" novalidate>
    <div class="modal-header">
        <button type="button" class="close btn-close-without-save-data" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="mb-40 text-center">
            <h2>Seguro del automóvil</h2>
        </div>
        <div class="form-group">
            <div class="error"><span></span></div>
        </div>
        <div class="form-group">
            <label for="payment" class="label-control">El automóvil...</label>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="the-car" id="is-already-insurance" value="is already insured in my name (insurance change)" <?php echo (isset($theCar) && $theCar == 'is already insured in my name (insurance change)') ? 'checked' : "checked"; ?>/>
                <label class="form-check-label" for="is-already-insurance">ya está asegurado a mi nombre (cambio de seguro)</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="the-car" id="will-already-insurance" value="will be registered in my name now (purchase of new car or change of car holder)" <?php echo (isset($theCar) && $theCar == 'will be registered in my name now (purchase of new car or change of car holder)') ? 'checked' : ''; ?>/>
                <label class="form-check-label" for="will-already-insurance">será registrado a mi nombre ahora (compra de automóvil nuevo o cambio de dueño)</label>
            </div>
        </div>
        <div class="form-group">
            <label for="desired-coverage" class="label-control">Cobertura deseada</label>
            <select name="desired-coverage" id="desired-coverage" class="form-control">
                <?php foreach($desiredCoverages as $key => $value) { ?>
                    <option value="<?php echo $key?>" <?php echo ($desiredCoverage == $key ? 'selected' : '')?>><?php echo $value?></option>
                <?php } ?>
            </select>
        </div>
        <p>Los seguros de automóvil requieren más información.  Por lo tanto, nos contactaremos con usted para aclarar los detalles adicionales.</p>
    </div>
    <div class="modal-footer clearfix">
        <input type="hidden" name="id_box">
        <button type="submit" class="btn btn-secondary pull-left step1 btn-close">Ofertas adicionales</button>
        <button type="submit" class="btn btn-primary pull-right btn-next step1">Próximo</button>
    </div>
</form>