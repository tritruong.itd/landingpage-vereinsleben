<?php
include_once '../../libraries/functions.php';
if(!isset($_SESSION))  {
    session_start();
}
$cart = getCartById('household-insurance');
$cart_tmp = getCartDataById('household-insurance');
if ($cart) {
    $sizeOfLivingArea = isset($cart['value']) ? $cart['value']['size-of-living-area'] : '';
    $building = isset($cart['value']) ? $cart['value']['building'] : '';
    $apartment = isset($cart['value']) ? $cart['value']['apartment'] : '';
    $breakageOfGlass = isset($cart['value']) ? isset($cart['value']['breakage-of-glass']) ? $cart['value']['breakage-of-glass'] : 'yes' : '';
    $bicyclesTheft = isset($cart['value']) ? (isset($cart['value']['bicycles-theft']) ? $cart['value']['bicycles-theft'] : 'yes') : '';
    $bicyclesTheftValue = isset($cart['value']) ? $cart['value']['bicycles-theft-value'] : '';
    $householdAgainst = isset($cart['value']) ? isset($cart['value']['household-against']) ? $cart['value']['household-against'] : 'yes' : '';
    $payment = isset($cart['value']) ? $cart['value']['payment'] : '';
    $householdContent = isset($cart['value']) ? isset($cart['value']['household-content']) ? $cart['value']['household-content'] : 'yes' : '';
    $claim = isset($cart['value']) ? $cart['value']['claim'] : '';
} else if ($cart_tmp) {
    $sizeOfLivingArea = isset($cart_tmp['value']) ? $cart_tmp['value']['size-of-living-area'] : '';
    $building = isset($cart_tmp['value']) ? $cart_tmp['value']['building'] : '';
    $apartment = isset($cart_tmp['value']) ? $cart_tmp['value']['apartment'] : '';
    $breakageOfGlass = isset($cart_tmp['value']) ? isset($cart_tmp['value']['breakage-of-glass']) ? $cart_tmp['value']['breakage-of-glass'] : 'yes' : '';
    $bicyclesTheft = isset($cart_tmp['value']) ? (isset($cart_tmp['value']['bicycles-theft']) ? $cart_tmp['value']['bicycles-theft'] : 'yes') : '';
    $bicyclesTheftValue = isset($cart_tmp['value']) ? $cart_tmp['value']['bicycles-theft-value'] : '';
    $householdAgainst = isset($cart_tmp['value']) ? isset($cart_tmp['value']['household-against']) ? $cart_tmp['value']['household-against'] : 'yes' : '';
    $payment = isset($cart_tmp['value']) ? $cart_tmp['value']['payment'] : '';
    $householdContent = isset($cart_tmp['value']) ? isset($cart_tmp['value']['household-content']) ? $cart_tmp['value']['household-content'] : 'yes' : '';
    $claim = isset($cart_tmp['value']) ? $cart_tmp['value']['claim'] : '';
} else {
    $sizeOfLivingArea = '';
    $building = 'edificio de apartamentos';
    $apartment = '';
    $breakageOfGlass = '';
    $bicyclesTheft = 'yes';
    $bicyclesTheftValue = '';
    $householdAgainst = '';
    $payment = '';
    $householdContent = '';
    $claim = '';
}
$buildings = array(
    "edificio de apartamentos",
    "casa unifamiliar",
    "casa para dos familias",
    "casa adosada"
);
$yesno = array(
  "yes" => "sí",
  "no" => "no"
);
$payments = array(
    'yearly' => 'anual',
    'half-yearly' => 'semestral',
    'quaterly' => 'trimestral',
    'monthly' => 'mensual',
);
?>
<form method="post" id="frm" novalidate>
    <div class="modal-header">
        <button type="button" class="close btn-close-without-save-data" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="mb-40 text-center">
            <h2>Seguro del hogar</h2>
        </div>
        <div class="form-group">
            <div class="error"><span></span></div>
        </div>
        <div class="form-group form-inline">
            <label for="size-of-living-area" class="label-control">Tamaño de la superficie habitable en metros cuadrados:</label>&nbsp;&nbsp;
            <input class="form-control form-claim mx-width100" type="text" name="size-of-living-area" id="size-of-living-area" value="<?php echo $sizeOfLivingArea;?>" required/>&nbsp;&nbsp;
            <label for="size-of-living-area">m²</label>
        </div>
        <div class="form-group">
            <label for="building" class="label-control">Por favor elija el tipo de edificio</label>
            <select name="building" id="building" class="form-control">
                <?php foreach($buildings as $build) { ?>
                    <option value="<?php echo $build?>" <?php echo ($building == $build ? 'selected' : '')?>><?php echo $build?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group <?php echo ($building == 'edificio de apartamentos')? '': 'd-none'?> form-inline" id="apartment-field">
            <label for="claim" class="label-control">¿Cuántos apartamentos hay en su edificio?</label>&nbsp;&nbsp;
            <input class="form-control form-apartments mx-width100" type="text" name="apartment" id="apartments" value="<?php echo $apartment;?>" required/>&nbsp;&nbsp;
            <label for="apartments">apartamentos</label>
        </div>
        <div class="form-group">
            <label for="breakage-of-glass" class="label-control">¿Desea asegurar la rotura de vidrio? <i class="fa fa-question-circle icon-question" aria-hidden="true" data-toggle="tooltip" data-html="true" title="<strong>Seguro de rotura de vidrio</strong><br>
                                    Proporciona cobertura para los costos de reemplazo de vidrio dañado o roto accidentalmente, incluyendo ventanas, espejos o vidrio de muebles.
        windows, mirrors or furniture glass.<br><br>"></i></label>
            <select name="breakage-of-glass" id="breakage-of-glass" class="form-control" required>
                <?php foreach($yesno as $key => $value) { ?>
                    <option value="<?php echo $key?>" <?php echo ($breakageOfGlass == $key ? 'selected' : '')?>><?php echo $value?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label for="bicycles-theft" class="label-control">¿Desea asegurar contra el robo de bicicletas?</label>
            <select name="bicycles-theft" id="bicycles-theft" class="form-control">
                <?php foreach($yesno as $key => $value) { ?>
                    <option value="<?php echo $key?>" <?php echo ($bicyclesTheft == $key ? 'selected' : '')?>><?php echo $value?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group form-inline <?php echo isset($bicyclesTheft) && ($bicyclesTheft == 'yes') ? '' : 'd-none'?>" id="show-bicycles-theft">
            <label for="bicycles-theft-value" class="label-control"> valor de sus bicicletas en euros €:</label>&nbsp;&nbsp;
            <input type="text" id="bicycles-theft-value" name="bicycles-theft-value" class="form-control" value="<?php echo $bicyclesTheftValue?>"/>
        </div>
        <div class="form-group">
            <label for="household-against" class="label-control">¿Desea asegurar el contenido de su hogar contra peligros naturales (como sismos, lluvias intensas, etc.)?
              <i class="fa fa-question-circle icon-question" aria-hidden="true" data-toggle="tooltip" data-html="true" title="<strong>peligros naturales</strong><br>
                                    Riesgos asegurados:<br>
        - inundación de la propiedad asegurada debido a los efectos de lluvia intensa<br>
        - los efectos de remanso se consideran asegurados en la medida en que el edificio en el que se encuentran los locales asegurados tiene una válvula de retención o presión en funcionamiento.
        - sismo<br>
        - hundimiento del suelo<br>
        - deslizamiento de tierra<br>
        - fuertes nevadas<br>
        - avalanchas de nieve"></i></label>
            <select name="household-against" id="household-against" class="form-control">
                <?php foreach($yesno as $key => $value) { ?>
                    <option value="<?php echo $key?>" <?php echo ($householdAgainst == $key ? 'selected' : '')?>><?php echo $value?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label for="payment" class="label-control">Pago</label>
            <select name="payment" id="payment" class="form-control" required>
                <option value="">Por favor elija</option>
                <?php foreach($payments as $key => $pay) { ?>
                    <option value="<?php echo $key?>" <?php echo ($payment == $key) ? 'selected' : ''?>><?php echo $pay?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label for="household-content" class="label-control">¿Seguro de hogar previo?</label>
            <select name="household-content" id="household-content" class="form-control">
                <?php foreach($yesno as $key => $value) { ?>
                    <option value="<?php echo $key?>" <?php echo ($householdContent == $key ? 'selected' : '')?>><?php echo $value?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group form-inline">
            <label for="claim" class="label-control">Número de reclamos en los últimos 5 años:</label>&nbsp;&nbsp;
            <input class="form-control form-claim" type="text" name="claim" id="claim" value="<?php echo $claim?>" required/>&nbsp;&nbsp;
            <label for="payment">reclamos</label>
        </div>
    </div>
    <div class="modal-footer clearfix">
        <input type="hidden" name="id_box">
        <button type="submit" class="btn btn-secondary pull-left btn-close">Ofertas adicionales</button>
        <button type="submit" class="btn btn-primary pull-right btn-next">Próximo</button>
    </div>
</form>
<script>
    $().ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
        $('#bicycles-theft').change(function() {
            if ($(this).val() === 'yes') {
                $('#show-bicycles-theft').removeClass('d-none').attr('required', true);
            } else {
                $('#show-bicycles-theft').addClass('d-none').removeAttr('required');
            }
        });
        $('#building').change(function() {
            if ($(this).val() === 'edificio de apartamentos') {
                $('#apartments').attr('required', true);
                $('#apartment-field').removeClass('d-none');
            } else {
                $('#apartments').removeAttr('required');
                $('#apartment-field').addClass('d-none');
            }
        });
        $('#breakage-of-glass').change(function() {
            if ($(this).val() === 'yes') {
                $('#bicycles-theft-value').attr('required', true);
            } else {
                $('#bicycles-theft-value').removeAttr('required');
            }
        });
    })
</script>
<style>
    .mx-width100 {
        max-width: 100px;
    }
</style>