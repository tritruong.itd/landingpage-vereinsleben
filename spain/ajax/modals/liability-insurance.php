<?php
include_once '../../libraries/functions.php';
if(!isset($_SESSION))  {
    session_start();
}
$cart = getCartById('liability-insurance');
$cart_tmp = getCartDataById('liability-insurance');
if ($cart) {
    $payment = isset($cart['value']) ? $cart['value']['payment'] : (isset($cart_tmp['value']) ? $cart_tmp['value']['payment'] : '');
    $priorLiability = isset($cart['value']) ? $cart['value']['prior-liability'] : (isset($cart_tmp['value']) ? $cart_tmp['value']['prior-liability'] : '');
    $claim = isset($cart['value']) ? $cart['value']['claim'] : (isset($cart_tmp['value']) ? $cart_tmp['value']['claim'] : '');
} else if ($cart_tmp) {
    $payment = isset($cart_tmp['value']) ? $cart_tmp['value']['payment'] : '';
    $priorLiability = isset($cart_tmp['value']) ? $cart_tmp['value']['prior-liability'] : '';
    $claim = isset($cart_tmp['value']) ? $cart_tmp['value']['claim'] : '';
} else {
    $payment = '';
    $priorLiability = '';
    $claim = '';
}
$payments = array(
        'yearly' => 'anual',
        'half-yearly' => 'semestral',
        'quaterly' => 'trimestral'
    );
$priorLiabilitys = array(
        'yes' => 'sí',
        'no' => 'no'
    );
?>
<form method="post" id="frm" novalidate>
    <div class="modal-header">
        <button type="button" class="close btn-close-without-save-data" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="mb-40 text-center">
            <h2>Seguro de responsabilidad civil privada</h2>
        </div>
        <div class="form-group">
            <div class="error"><span></span></div>
        </div>
        <div class="clearfix">
            <div class="form-group">
                <label for="payment" class="label-control">Pago</label>
                <select name="payment" id="payment" class="form-control" required>
                    <option value="">Por favor elija</option>
                    <?php foreach($payments as $key => $pay) { ?>
                        <option value="<?php echo $key?>" <?php echo ($payment == $key) ? 'selected' : ''?>><?php echo $pay?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="prior-liability" class="label-control">¿Seguro de responsabilidad civil previo?</label>
                <select name="prior-liability" id="prior-liability" class="form-control" required>
                    <option value="">Por favor elija</option>
                    <?php foreach($priorLiabilitys as $key => $prior) { ?>
                        <option value="<?php echo $key?>" <?php echo ($priorLiability == $key) ? 'selected' : ''?>><?php echo $prior?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group form-inline">
                <label for="claim">Número de reclamos en los últimos 5 años:</label>&nbsp;&nbsp;
                <input class="form-control form-claim" type="text" name="claim" id="claim" value="<?php echo @$claim?>" required/>&nbsp;&nbsp;
                <label for="payment">reclamos</label>
            </div>
        </div>
    </div>
    <div class="modal-footer clearfix">
        <input type="hidden" name="id_box">
        <button type="submit" class="btn btn-secondary pull-left btn-close">Ofertas adicionales</button>
        <button type="submit" class="btn btn-primary pull-right btn-next step1">Próximo</button>
    </div>
</form>