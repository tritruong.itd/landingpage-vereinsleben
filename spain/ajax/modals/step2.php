<form method="post" id="frm-step2">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
  <div class="modal-body">
    <div class="mb-40 text-center">
      <h2>Tu información personal</h2>
    </div>
    <div class="form-group">
      <div class="error"><span></span></div>
    </div>
    <div class="form-group">
      <input type="text" class="form-control" id="name" name="name" placeholder="Apellido*" required>
    </div>
    <div class="form-group">
      <input type="text" class="form-control" id="firstName" name="firstName" placeholder="Nombre*" required>
    </div>
    <div class="form-group">
      <input type="text" class="form-control" id="streetAddress" name="streetAddress" placeholder="Calle, número*" required>
    </div>
    <div class="form-group">
      <div class="row">
        <div class="col-sm-5 col-12">
          <input type="text" class="form-control" id="postCode" name="postCode" placeholder="Códico postal*" required>
        </div>
        <div class="col-sm-7 col-12">
          <input type="text" class="form-control" id="place" name="place" placeholder="Ciudad*" required>
        </div>
      </div>
    </div>
    <div class="form-group">
      <input type="text" class="form-control" id="country" name="country" placeholder="País">
    </div>
    <div class="form-group">
      <input type="email" class="form-control" id="email" name="email" placeholder="Email*" required>
    </div>
    <div class="form-group">
      <label for="date-of-birth">Fecha de nacimiento</label>
        <div class="form-inline">
            <select id ="day" name = "dd" class="form-control">
            </select>&nbsp;&nbsp;
            <select  id ="month" name = "mm" class="form-control" onchange="change_month(this)">
            </select>&nbsp;&nbsp;
            <select id ="year" name = "yyyy" class="form-control" onchange="change_year(this)">
            </select>
        </div>
    </div>
    <div class="form-group">
      <label for="martital-status">Estado civil:</label>
      <select name="martial-status" id="martital-status" class="form-control">
        <option value="Single - With children">soltero/a - con hijos</option>
        <option value="Single - Without children">soltero/a - sin hijos</option>
        <option value="Family - With children">familia - con hijos</option>
        <option value="Family - Without children">familia - sin hijos</option>
      </select>
    </div>
    <div class="form-group">
      <label for="occupation-group">Grupo ocupacional: </label>
      <select name="occupation-group" id="occupation-group" class="form-control">
        <option value="Employee">empleado/a</option>
        <option value="Self-employed">autónomo/a</option>
        <option value="Trainee">aprendiz</option>
        <option value="Student">estudiante</option>
        <option value="Pensioner">jubilado/a</option>
        <option value="Unemployed">desempleado/a</option>
        <option value="Not Working">no trabaja</option>
        <option value="Civil Servant">empleado/a público/a</option>
      </select>
    </div>
    <div class="form-group">
      <label for="pracitsed-profession">Profesión actual: </label>
      <input type="text" class="form-control" name="pracitsed-profession" id="pracitsed-profession"/>
    </div>
    <div class="form-group">
      <label for="additional-information-question">Información adicional/pregunta: </label>
      <textarea name="additional-information-question" id="additional-information-question" cols="30" rows="3" class="form-control"></textarea>
    </div>
    <div class="form-group check-box">
      <input id="initial-information" type="checkbox" name="initial-information" class="checkbox-label">
        <label for="initial-information">Confirmo que he descarcado y leído la <a href="https://www.cleverly-insured-in-germany.com/inital-information" target="_blank" title="initial information">información inicial ("Erstinformation")</a> y la<a href="https://www.cleverly-insured-in-germany.com/datenschutz" target="_blank" title="data privacy statement"> Declaración de Privacidad de Datos ("Datenschutz")</a> al pie de esta página y estoy de acuerdo con los términos.</label>
    </div>
  </div>
  <div class="modal-footer clearfix">
    <input type="hidden" name="id_box">
      <img src="/assets/images/loading.gif" alt="Loading" class="d-none img-loading"/>
    <button type="submit" class="btn btn-primary pull-right">Enviar</button>
  </div>
</form>
<script>
  var Days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  $(document).ready(function () {
    var option = '<option value="day">día</option>';
    var selectedDay = "day";
    for (var i = 1; i <= Days[0]; i++) { //add option days
      option += '<option value="' + i + '">' + (i < 10 ? '0' + i : i) + '</option>';
    }
    $('#day').append(option);
    $('#day').val(selectedDay);

    var option = '<option value="month">mes</option>';
    var selectedMon = "month";
    for (var i = 1; i <= 12; i++) {
      option += '<option value="' + i + '">' + (i < 10 ? '0' + i : i) + '</option>';
    }
    $('#month').append(option);
    $('#month').val(selectedMon);

    var d = new Date();
    var option = '<option value="year">año</option>';
    selectedYear = "year";
    for (var i = 1930; i <= d.getFullYear(); i++) {// years start i
      option += '<option value="' + i + '">' + i + '</option>';
    }
    $('#year').append(option);
    $('#year').val(selectedYear);
  });

  function isLeapYear(year) {
    year = parseInt(year);
    if (year % 4 != 0) {
      return false;
    } else if (year % 400 == 0) {
      return true;
    } else return year % 100 != 0;
  }

  function change_year(select) {
    if (isLeapYear($(select).val())) {
      Days[1] = 29;
      if ($("#month").val() == 2) {
        var day = $('#day');
        var val = $(day).val();
        $(day).empty();
        var option = '<option value="day">día</option>';
        for (var i = 1; i <= Days[1]; i++) { //add option days
          option += '<option value="' + i + '">' + i + '</option>';
        }
        $(day).append(option);
        if (val > Days[month]) {
          val = 1;
        }
        $(day).val(val);
      }
    }
    else {
      Days[1] = 28;
    }
  }

  function change_month(select) {
    var day = $('#day');
    var val = $(day).val();
    $(day).empty();
    var option = '<option value="day">día</option>';
    var month = parseInt($(select).val()) - 1;
    for (var i = 1; i <= Days[month]; i++) { //add option days
      option += '<option value="' + i + '">' + (i < 10 ? '0' + i : i) + '</option>';
    }
    $(day).append(option);
    if (val > Days[month]) {
      val = 1;
    }
    $(day).val(val);
  }
</script>