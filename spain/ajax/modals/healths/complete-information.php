<form method="post" id="frm-step2" novalidate>
    <div class="modal-header">
        <button type="button" class="close btn-close-without-save-data" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="form-group">
            <div class="row clearfix">
                <label for="last-name" class="col-sm-4">Last name<span class="text-danger">*</span>:</label>
                <div class="col-sm-8">
                    <input class="form-control form-health" type="text" name="last-name" id="last-name" required/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row clearfix">
                <label for="first-name" class="col-sm-4">First name<span class="text-danger">*</span>:</label>
                <div class="col-sm-8">
                    <input class="form-control form-health" type="text" name="first-name" id="first-name" required/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row clearfix">
                <label for="email" class="col-sm-4">Email<span class="text-danger">*</span>:</label>
                <div class="col-sm-8">
                    <input class="form-control" type="email" name="email" id="email" required/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row clearfix">
                <label for="last-name" class="col-sm-4">Date of birth<span class="text-danger">*</span>:</label>
                <div class="col-sm-8">
                    <div class="form-inline">
                        <select id ="day" name = "dd" class="form-control">
                        </select>&nbsp;&nbsp;
                        <select  id ="month" name = "mm" class="form-control" onchange="change_month(this)">
                        </select>&nbsp;&nbsp;
                        <select id ="year" name = "yyyy" class="form-control" onchange="change_year(this)">
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="clearfix">
                <label for="additional-information">Additional information:</label>
                <textarea name="additional-information" id="additional-information" cols="30" rows="5" class="form-control"></textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="clearfix">
                <label for="privacy"></label>
                <textarea name="privacy" id="privacy" cols="30" rows="10" class="form-control" readonly><?php echo file_get_contents('privacy.txt')?></textarea>
            </div>
        </div>
        <div class="form-group check-box" id="privacy-checkbox">
            <input id="privacy-information" type="checkbox" name="privacy-information" class="checkbox-label">
            <label for="privacy-information">I have read privacy information and agree to the terms.</label>
        </div>
        <div class="form-group check-box">
            <input id="initial-information" type="checkbox" name="initial-information" class="checkbox-label">
            <label for="initial-information">I have downloaded and read the <a href="https://www.cleverly-insured-in-germany.com/inital-information" target="_blank" title="initial information">initial information (“Erstinformation”)</a> and <a href="https://www.cleverly-insured-in-germany.com/datenschutz" target="_blank" title="data privacy statement">data privacy statement (“Datenschutz“)</a> at the bottom of this page and agree to the terms.</label>
        </div>
    </div>
    <div class="modal-footer clearfix">
        <input type="hidden" name="id_box" value=""/>
        <button type="submit" class="btn btn-primary">Send</button>
        <button type="button" class="btn btn-default btn-close-without-save-data">Close</button>
    </div>
</form>
<script>
  var Days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  $(document).ready(function () {
    var option = '<option value="day">day</option>';
    var selectedDay = "day";
    for (var i = 1; i <= Days[0]; i++) { //add option days
      option += '<option value="' + i + '">' + (i < 10 ? '0' + i : i) + '</option>';
    }
    $('#day').append(option);
    $('#day').val(selectedDay);

    var option = '<option value="month">month</option>';
    var selectedMon = "month";
    for (var i = 1; i <= 12; i++) {
      option += '<option value="' + i + '">' + (i < 10 ? '0' + i : i) + '</option>';
    }
    $('#month').append(option);
    $('#month').val(selectedMon);

    var d = new Date();
    var option = '<option value="year">year</option>';
    selectedYear = "year";
    for (var i = 1930; i <= d.getFullYear(); i++) {// years start i
      option += '<option value="' + i + '">' + i + '</option>';
    }
    $('#year').append(option);
    $('#year').val(selectedYear);
  });

  function isLeapYear(year) {
    year = parseInt(year);
    if (year % 4 !== 0) {
      return false;
    } else if (year % 400 === 0) {
      return true;
    } else if (year % 100 === 0) {
      return false;
    } else {
      return true;
    }
  }

  function change_year(select) {
    if (isLeapYear($(select).val())) {
      Days[1] = 29;
      if ($("#month").val() == 2) {
        var day = $('#day');
        var val = $(day).val();
        $(day).empty();
        var option = '<option value="day">day</option>';
        for (var i = 1; i <= Days[1]; i++) { //add option days
          option += '<option value="' + i + '">' + i + '</option>';
        }
        $(day).append(option);
        if (val > Days[month]) {
          val = 1;
        }
        $(day).val(val);
      }
    }
    else {
      Days[1] = 28;
    }
  }

  function change_month(select) {
    var day = $('#day');
    var val = $(day).val();
    $(day).empty();
    var option = '<option value="day">day</option>';
    var month = parseInt($(select).val()) - 1;
    for (var i = 1; i <= Days[month]; i++) { //add option days
      option += '<option value="' + i + '">' + (i < 10 ? '0' + i : i) + '</option>';
    }
    $(day).append(option);
    if (val > Days[month]) {
      val = 1;
    }
    $(day).val(val);
  }
</script>