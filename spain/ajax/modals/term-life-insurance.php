<?php
include_once '../../libraries/functions.php';
if(!isset($_SESSION))  {
    session_start();
}
$cart = getCartById('term-life-insurance');
$cart_tmp = getCartDataById('term-life-insurance');

if ($cart) {
    $benefit = isset($cart['value']) ? $cart['value']['benefit'] : '';
    $insurancePeriod = isset($cart['value']) ? $cart['value']['insurance-period'] : '';
    $iam = isset($cart['value']) ? $cart['value']['iam'] : '';
    $visaStatus = isset($cart['value']) ? $cart['value']['visa-status'] : '';
    $payment = isset($cart['value']) ? $cart['value']['payment'] : '';
} else if ($cart_tmp) {
    $benefit = isset($cart_tmp['value']) ? $cart_tmp['value']['benefit'] : '';
    $insurancePeriod = isset($cart_tmp['value']) ? $cart_tmp['value']['insurance-period'] : '';
    $iam = isset($cart_tmp['value']) ? $cart_tmp['value']['iam'] : '';
    $visaStatus = isset($cart_tmp['value']) ? $cart_tmp['value']['visa-status'] : '';
    $payment = isset($cart_tmp['value']) ? $cart_tmp['value']['payment'] : '';
} else {
    $benefit = '';
    $insurancePeriod = '';
    $iam = '';
    $visaStatus = '';
    $payment = '';
}
$payments = array(
    'yearly' => 'anual',
    'half-yearly' => 'semestral',
    'quaterly' => 'trimestral',
    'monthly' => 'mensual',
);
$iams = array(
    'non-smoker' => 'no fumador',
    'smoker' => 'fumador'
);
?>
<form method="post" id="frm" novalidate>
    <div class="modal-header">
        <button type="button" class="close btn-close-without-save-data" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="mb-40 text-center">
            <h2>Seguro de vida a término</h2>
        </div>
        <div class="form-group form-inline">
            <label for="benefit" class="label-control">Indemnización for fallecimiento deseada en euros €:</label>&nbsp;&nbsp;
            <input class="form-control form-claim" type="text" name="benefit" id="benefit" value="<?php echo $benefit?>" required/>
        </div>
        <div class="form-group form-inline">
            <label for="insurance-period" class="label-control">Período de seguro en años:</label>&nbsp;&nbsp;
            <input class="form-control form-claim" type="text" name="insurance-period" id="insurance-period" value="<?php echo $insurancePeriod?>" required/>
        </div>
        <div class="form-group">
            <label for="iam" class="label-control">Yo soy</label>
            <select name="iam" id="iam" class="form-control" required>
                <option value="">Por favor elija</option>
                <?php foreach($iams as $key => $pay) { ?>
                    <option value="<?php echo $key?>" <?php echo ($iam == $key) ? 'selected' : ''?>><?php echo $pay?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label for="visa-status" class="label-control">Status de la visa</label>
            <select name="visa-status" id="visa-status" class="form-control" required>
                <option value="">Por favor elija</option>
                <?php for($i = 2019; $i <= 2023; $i++) { ?>
                    <option value="visa valid until <?php echo $i?>" <?php echo ((isset($visaStatus) && $visaStatus == 'visa valid until ' . $i) ? 'selected' : '')?>>Visa valid until <?php echo $i?></option>
                <?php } ?>
                <option value="permanent residence" <?php echo ((isset($visaStatus) && $visaStatus == 'permanent residence') ? 'selected' : '')?>>Permanent residence</option>
                <option value="I have German nationality" <?php echo ((isset($visaStatus) && $visaStatus == 'I have German nationality') ? 'selected' : '')?>>I have German nationality</option>
            </select>
        </div>
        <div class="form-group">
            <label for="payment" class="label-control">Pago</label>
            <select name="payment" id="payment" class="form-control" required>
                <option value="">Por favor elija</option>
                <?php foreach($payments as $key => $pay) { ?>
                    <option value="<?php echo $key?>" <?php echo ($payment == $key) ? 'selected' : ''?>><?php echo $pay?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="modal-footer clearfix">
        <input type="hidden" name="id_box">
        <button type="submit" class="btn btn-secondary pull-left step1 btn-close">Ofertas adicionales</button>
        <button type="submit" class="btn btn-primary pull-right btn-next step1">Próximo</button>
    </div>
</form>