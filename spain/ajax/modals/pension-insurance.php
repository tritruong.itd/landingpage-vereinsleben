<?php
include_once '../../libraries/functions.php';
if(!isset($_SESSION))  {
    session_start();
}
$cart = getCartById('pension-insurance');
$cart_tmp = getCartDataById('pension-insurance');

if ($cart) {
    $retirement_begin = isset($cart['value']) ? $cart['value']['retirement-begin'] : '';
    $monthly_payment = isset($cart['value']) ? $cart['value']['monthly-payment'] : '';
    $current_gross = isset($cart['value']) ? $cart['value']['current-gross'] : '';
    $pension_plan = isset($cart['value']) ? $cart['value']['pension-plan'] : '';
    $my_tax_class = isset($cart['value']) ? $cart['value']['my-tax-class'] : '';
    $church_tax = isset($cart['value']) ? $cart['value']['church-tax'] : '';
    $pay_contribution = isset($cart['value']) ? $cart['value']['pay-contribution'] : '';
    $family_status = isset($cart['value']) ? $cart['value']['family-status'] : '';
    $number_of_children = isset($cart['value']) ? $cart['value']['number-of-children'] : '';
    $riester_subsidy = isset($cart['value']) ? isset($cart['value']['riester-subsidy']) ? $cart['value']['riester-subsidy'] : '' : '';
    $born_until_end_of_2007 = isset($cart['value']) ? $cart['value']['born-until-end-of-2007'] : '';
    $born_from_2008 = isset($cart['value']) ? $cart['value']['born-from-2008'] : '';
    $gross_income = isset($cart['value']) ? $cart['value']['gross-income'] : '';
    $additional_information = isset($cart['value']) ? $cart['value']['additional-information'] : '';
} else if ($cart_tmp) {
    $retirement_begin = isset($cart_tmp['value']) ? $cart_tmp['value']['retirement-begin'] : '';
    $monthly_payment = isset($cart_tmp['value']) ? $cart_tmp['value']['monthly-payment'] : '';
    $current_gross = isset($cart_tmp['value']) ? $cart_tmp['value']['current-gross'] : '';
    $pension_plan = isset($cart_tmp['value']) ? $cart_tmp['value']['pension-plan'] : '';
    $my_tax_class = isset($cart_tmp['value']) ? $cart_tmp['value']['my-tax-class'] : '';
    $church_tax = isset($cart_tmp['value']) ? $cart_tmp['value']['church-tax'] : '';
    $pay_contribution = isset($cart_tmp['value']) ? $cart_tmp['value']['pay-contribution'] : '';
    $family_status = isset($cart_tmp['value']) ? $cart_tmp['value']['family-status'] : '';
    $number_of_children = isset($cart_tmp['value']) ? $cart_tmp['value']['number-of-children'] : '';
    $riester_subsidy = isset($cart_tmp['value']) ? isset($cart_tmp['value']['riester-subsidy']) ? $cart_tmp['value']['riester-subsidy'] : '' : '';
    $born_until_end_of_2007 = isset($cart_tmp['value']) ? $cart_tmp['value']['born-until-end-of-2007'] : '';
    $born_from_2008 = isset($cart_tmp['value']) ? $cart_tmp['value']['born-from-2008'] : '';
    $gross_income = isset($cart_tmp['value']) ? $cart_tmp['value']['gross-income'] : '';
    $additional_information = isset($cart_tmp['value']) ? $cart_tmp['value']['additional-information'] : '';
} else {
    $retirement_begin = '';
    $monthly_payment = '';
    $current_gross =  '';
    $pension_plan =  '';
    $my_tax_class = '';
    $church_tax = '';
    $pay_contribution = '';
    $family_status = '';
    $number_of_children = '';
    $riester_subsidy = '';
    $born_until_end_of_2007 = '';
    $born_from_2008 = '';
    $gross_income = '';
    $additional_information = '';
}
?>
<form method="post" id="frm" novalidate>
    <div class="modal-header">
        <button type="button" class="close btn-close-without-save-data" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="mb-40 text-center">
            <h2>Pensión privada</h2>
        </div>
        <div class="form-group">
            <div class="error"><span></span></div>
        </div>
        <div class="form-group">
            <label for="retirement-begin" class="label-control">Comienzo de la jubilación:</label>
            <select name="retirement-begin" id="retirement-begin" class="form-control">
                <?php for($i = 55; $i <= 70; $i++) { ?>
                    <option value="<?php echo $i?>" <?php echo (isset($retirement_begin) && $retirement_begin == $i) ? 'selected' : ""; ?>><?php echo $i?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label for="monthly-payment" class="label-control">Pago mensual:</label>
            <select name="monthly-payment" id="monthly-payment" class="form-control">
                <option value="minimum contribution" <?php echo (isset($monthly_payment) && $monthly_payment == 'minimum contribution') ? 'selected' : ""; ?>>contribución mínima</option>
                <?php for($i = 2; $i <= 15; $i++) { ?>
                    <option value="<?php echo ($i * 10)?>" <?php echo (isset($monthly_payment) && $monthly_payment == ($i*10)) ? 'selected' : ""; ?>>€<?php echo ($i * 10)?></option>
                <?php } ?>
                <option value="200" <?php echo (isset($monthly_payment) && $monthly_payment == '200') ? 'selected' : ""; ?>>200 euros</option>
                <option value="250" <?php echo (isset($monthly_payment) && $monthly_payment == '250') ? 'selected' : ""; ?>>250 euros</option>
                <option value="300" <?php echo (isset($monthly_payment) && $monthly_payment == '300') ? 'selected' : ""; ?>>300 euros</option>
                <option value="350" <?php echo (isset($monthly_payment) && $monthly_payment == '350') ? 'selected' : ""; ?>>350 euros</option>
                <option value="400" <?php echo (isset($monthly_payment) && $monthly_payment == '400') ? 'selected' : ""; ?>>400 euros</option>
                <option value="450" <?php echo (isset($monthly_payment) && $monthly_payment == '450') ? 'selected' : ""; ?>>450 euros</option>
                <option value="500" <?php echo (isset($monthly_payment) && $monthly_payment == '500') ? 'selected' : ""; ?>>500 euros</option>
                <option value="other" <?php echo (isset($monthly_payment) && $monthly_payment == 'other') ? 'selected' : ""; ?>>otra contribución: por favor, especifique en "información adicional"</option>
            </select>
        </div>
        <div class="form-group form-check">
            <input type="checkbox" class="form-check-input" id="pension-plan" name="pension-plan" <?php echo ($pension_plan == 'on') ? 'checked' : ""; ?>/>
            <label class="form-check-label label-control" for="pension-plan">Estoy interesado en ahorrar impuestos con mi seguro de pensión</label>
        </div>
        <div class="pension-plan <?php echo ($pension_plan == 'on') ? '' : "d-none"; ?>">
            <div class="form-group form-inline" >
                <label for="current-gross" class="label-control">Ingreso bruto actual por año en euros:</label>&nbsp;&nbsp;
                <input type="text" class="form-control" id="current-gross" name="current-gross" value="<?php echo $current_gross ?>" required/>
            </div>
            <div class="form-group">
                <label for="my-tax-class" class="label-control">Mi categoría fiscal:</label>
                <select name="my-tax-class" id="my-tax-class" class="form-control">
                    <option value="">Por favor elija</option>
                    <?php for($i = 1; $i <= 6; $i++) { ?>
                        <option value="<?php echo $i?>" <?php echo ($my_tax_class == $i) ? 'selected' : ""; ?>><?php echo $i?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="church-tax" class="label-control">Pago impuestos a la iglesia:</label>
                <select name="church-tax" id="church-tax" class="form-control">
                    <option value="">Por favor elija</option>
                    <option value="yes" <?php echo ($church_tax == 'yes') ? 'selected' : ""; ?>>sí</option>
                    <option value="no" <?php echo ($church_tax == 'no') ? 'selected' : ""; ?>>no</option>
                </select>
            </div>
            <div class="form-group">
                <label for="pay-contribution" class="label-control">Pago contribuciones al seguro de pensiones del estado:</label>
                <select name="pay-contribution" id="pay-contribution" class="form-control">
                    <option value="">Por favor elija</option>
                    <option value="yes" <?php echo ($pay_contribution == 'yes') ? 'selected' : ""; ?>>sí</option>
                    <option value="no" <?php echo ($pay_contribution == 'no') ? 'selected' : ""; ?>>no</option>
                </select>
            </div>
            <div class="form-group">
                <label for="family-status" class="label-control">Estado civil</label>
                <select name="family-status" id="family-status" class="form-control">
                    <option value="">Por favor elija</option>
                    <option value="single" <?php echo ($family_status == 'single') ? 'selected' : ""; ?>>soltero/a</option>
                    <option value="married" <?php echo ($family_status == 'married') ? 'selected' : ""; ?>>casado/a</option>
                    <option value="widowed" <?php echo ($family_status == 'widowed') ? 'selected' : ""; ?>>viudo/a</option>
                    <option value="divorced" <?php echo ($family_status == 'divorced') ? 'selected' : ""; ?>>divorciado/a</option>
                </select>
            </div>
            <div class="form-group form-inline">
                <label for="number-of-children" class="label-control">Número de niños por los que recibe usted "Kindergeld":</label>&nbsp;&nbsp;
                <input type="text" class="form-control" id="number-of-children" name="number-of-children" value="<?php echo $number_of_children; ?>"/>
            </div>
            <div class="form-group form-check">
                <div class="form-group">
                    <input type="checkbox" class="form-check-input" id="riester-subsidy" name="riester-subsidy" <?php echo ($riester_subsidy == 'on') ? 'checked' : ""; ?>/>
                    <label class="form-check-label" for="riester-subsidy">Estoy interesado en la "pensión Riester"<br/>Número de niños por los que recibe "Kindergeld"</label>
                </div>
                <div class="form-group form-inline <?php echo ($riester_subsidy == 'on') ? '' : "d-none"; ?>">
                    <label class="form-check-label" for="born-until-end-of-2007">nacido hasta finales de 2007 2007:</label>&nbsp;&nbsp;
                    <input type="text" class="form-control" id="born-until-end-of-2007" name="born-until-end-of-2007" value="<?php echo (isset($born_until_end_of_2007)) ?  $born_until_end_of_2007 : ""; ?>"/>
                </div>
                <div class="form-group form-inline <?php echo ($riester_subsidy == 'on') ? '' : "d-none"; ?>">
                    <label class="form-check-label" for="born-from-2008">nacidos desde 2008:</label>&nbsp;&nbsp;
                    <input type="text" class="form-control" id="born-from-2008" name="born-from-2008" value="<?php echo (isset($born_from_2008)) ?  $born_from_2008 : ""; ?>"/>
                </div>
            </div>
            <div class="form-group form-inline d-none gross-income">
                <label class="form-check-label label-control" for="gross-income">Ingresos brutos del año pasado&nbsp;<i class="fa fa-question-circle icon-question" aria-hidden="true" data-toggle="tooltip" data-html="true" title="Ingresos del seguro social = sozialversicherungspflichtiges Einkommen.  Este monto se puede encontrar en su 'elektronische Lohnsteuerbescheinigung' (notificación electrónica de impuestos sobre el empleo) o en el recibo de pago de diciembre del año pasado"></i>&nbsp;de la persona asegurada en euros:</label>&nbsp;&nbsp;
                <input type="text" class="form-control" id="gross-income" name="gross-income" style="width: 20%;" value="<?php echo (isset($gross_income)) ?  $gross_income : ""; ?>"/>
            </div>
            <div class="form-group">
                <label class="form-check-label label-control" for="additional-information">Información adicional:</label>
                <textarea name="additional-information" id="additional-information" cols="30" rows="5" class="form-control"><?php echo (isset($additional_information)) ? $additional_information : ""; ?>
                </textarea>
            </div>
        </div>
    </div>
    <div class="modal-footer clearfix">
        <input type="hidden" name="id_box">
        <button type="submit" class="btn btn-secondary pull-left step1 btn-close">Ofertas adicionales</button>
        <button type="submit" class="btn btn-primary pull-right btn-next step1">Próximo</button>
    </div>
</form>
<script>
    $().ready(function() {
      $('[data-toggle="tooltip"]').tooltip();
      $('#riester-subsidy').change(function() {
        var parent = $(this).parents('.form-check');
        if ($(this).is(':checked')) {
          $("#born-until-end-of-2007, #born-from-2008, #number-of-children, #gross-income").attr('required', true);
          parent.find('.form-group.form-inline').removeClass('d-none');
          $('.gross-income').removeClass('d-none');
        } else {
          parent.find('.form-group.form-inline').addClass('d-none');
          $('#number-of-children-error, #gross-income-error').css('display', 'none');
          $('#number-of-children, #gross-income, #born-until-end-of-2007, #born-from-2008').removeAttr('required').removeClass('error');
          $('.gross-income').addClass('d-none');
        }
      });
      $('#pension-plan').change(function() {
        if ($(this).is(":checked")) {
          $('.pension-plan').removeClass('d-none');
          $('#my-tax-class, #church-tax, #pay-contribution, #family-status, #current-gross').attr('required', true);
        } else {
          $('.pension-plan').addClass('d-none');
          $('#my-tax-class, #church-tax, #pay-contribution, #family-status, #current-gross').removeAttr('required');
        }
      });
    })
</script>