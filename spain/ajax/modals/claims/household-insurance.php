<?php
$typeOfDamages = array(
    'fire-damage' => 'Fire damage',
    'mains-water-damage' => 'Mains water damage',
    'storm-damage' => 'Storm damage',
    'hail-damage' => 'Hail damage',
    'burglary-robbery-vandalism' => 'Burglary / Robbery / Vandalism',
    'glass-breakage' => 'Glass breakage',
    'bicycle-theft' => 'Bicycle theft',
    'natural-hazards' => 'Natural hazards',
    'other' => 'Others',
);
?>
<form method="post" id="frm" novalidate>
    <div class="modal-header">
        <button type="button" class="close btn-close-without-save-data" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="text-center">
            <h2>Household insurance</h2>
        </div>
        <div class="form-group row clearfix">
            <div class="col-12"><div class="error"><span></span></div></div>
        </div>
        <p><strong>INSURED PERSON:</strong></p>
        <div class="form-group row">
            <label for="last-name" class="col-sm-3">Last name:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" name="last-name" id="last-name" required/>
            </div>
        </div>
        <div class="form-group row">
            <label for="first-name" class="col-sm-3">First name:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" name="first-name" id="first-name" required/>
            </div>
        </div>
        <div class="form-group row">
            <label for="date-of-birth" class="col-sm-3">Date of birth:</label>
            <div class="col-sm-9">
                <div class="form-inline">
                    <select id ="day" name = "dd" class="form-control">
                    </select>&nbsp;&nbsp;
                    <select  id ="month" name = "mm" class="form-control" onchange="change_month(this, false)">
                    </select>&nbsp;&nbsp;
                    <select id ="year" name = "yyyy" class="form-control" onchange="change_year(this, false)">
                    </select>
                </div>
                <p class="text-danger d-none error-birthday"><small>This field is required.</small></p>
            </div>
        </div>
        <div class="form-group row">
            <label for="street" class="col-sm-3">Street:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" name="street" id="street" required/>
            </div>
        </div>
        <div class="form-group row">
            <label for="postcode" class="col-sm-3">Postcode, City:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" name="postcode" id="postcode" required/>
            </div>
        </div>
        <div class="form-group row">
            <label for="email" class="col-sm-3">Email:</label>
            <div class="col-sm-9">
                <input type="email" name="email" id="email" class="form-control" required/>
            </div>
        </div>
        <p><strong>TYPE OF DAMAGE:</strong></p>
        <div class="form-group">
            <?php foreach ($typeOfDamages as $key => $type) { ?>
            <div class="form-check frm-multiple-checkbox">
                <input class="form-check-input" name="type-of-damage[]" type="checkbox" value="<?php echo $key?>" id="<?php echo $key?>">
                <label class="form-check-label" for="<?php echo $key?>"><?php echo $type;?></label>
            </div>
            <?php } ?>
            <p class="text-danger d-none error-checkbox"><small>Por favor, seleccione al menos 1 caso.</small></p>
        </div>
        <div class="form-group row d-none other-input">
            <div class="col-12">
                <input type="text" class="form-control" name="other-detail" required/>
            </div>
        </div>
        <p><strong>DETAILS OF DAMAGE:</strong></p>
        <div class="form-group row">
            <label for="date-of-damaged" class="col-sm-3">Date of damage:</label>
            <div class="col-sm-9">
                <div class="form-inline">
                    <select id ="day-damage" name="dd-damage" class="form-control">
                    </select>&nbsp;&nbsp;
                    <select  id ="month-damage" name="mm-damage" class="form-control" onchange="change_month(this, true)">
                    </select>&nbsp;&nbsp;
                    <select id ="year-damage" name="yyyy-damage" class="form-control" onchange="change_year(this, true)">
                    </select>
                </div>
                <p class="text-danger d-none error-damage"><small>This field is required.</small></p>
            </div>
        </div>
        <div class="form-group row">
            <label for="damage-location" class="col-sm-3">Damage location:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" name="damage-location" id="damage-location" required/>
            </div>
        </div>
        <div class="form-group row">
            <label for="expected-lost-in-euros" class="col-sm-3">Expected loss in euros:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" name="expected-lost-in-euros" id="expected-lost-in-euros" required/>
            </div>
        </div>
        <div class="form-group row">
            <label for="list-of-damaged" class="col-sm-3">List of damaged objects:</label>
            <div class="col-sm-9">
                <textarea name="list-of-damaged" class="form-control" id="list-of-damaged" required cols="30" rows="5"></textarea>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-12">
                <label for="detail-damage">Detailed description of the cause and the circumstances of damage:</label>
                <textarea name="detail-damage" class="form-control" id="detail-damage" cols="30" rows="5" required></textarea>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-12">
                <label for="incurred-cost">Evidence of the incurred costs in euros (cost estimate for repairing or purchase receipt of damaged object)</label>
                <input type="text" class="form-control" name="incurred-cost" id="incurred-cost" required/>
            </div>
        </div>
        <p>***Please send PHOTOS of damaged objects to <a href="mailto:service@your-insurancebroker.de">service@your-insurancebroker.de</a>***</p>
        <p>If possible, please keep the damaged objects.  Sometimes the insurance company asks clients to send them to the insurance company for examination.</p>
        <div class="form-group row">
            <div class="col-12">
                <label for="additional-information">Additional information:</label>
                <textarea name="additional-information" class="form-control" id="additional-information" cols="30" rows="5"></textarea>
            </div>
        </div>
        <div class="form-group check-box">
            <input id="initial-information" type="checkbox" name="initial-information" class="checkbox-label">
            <label for="initial-information">I have downloaded and read the data privacy statement <a href="https://www.cleverly-insured-in-germany.com/datenschutz" target="_blank" title="data privacy statement">(“Datenschutz“)</a> at the bottom of this page and agree to the terms.</label>
        </div>
    </div>
    <div class="modal-footer">
        <img src="/assets/images/loading.gif" alt="Loading" class="d-none img-loading"/>
        <input type="hidden" name="id_box" value="household-insurance"/>
        <button type="submit" class="btn btn-primary">Send</button>
    </div>
</form>
<script>
    var Days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    $(document).ready(function () {
      $('#other').change(function () {
        if ($(this).is(':checked')) {
          $('.other-input').removeClass('d-none');
          $('.other-input input').attr('required', true);
        } else {
          $('.other-input input').removeAttr('required');
          $('.other-input').addClass('d-none');
        }
      });
      var option = '<option value="day">day</option>';
      var selectedDay = "day";
      for (var i = 1; i <= Days[0]; i++) { //add option days
        option += '<option value="' + i + '">' + (i < 10 ? '0' + i : i) + '</option>';
      }
      $('#day').append(option);
      $('#day').val(selectedDay);

      $('#day-damage').append(option);
      $('#day-damage').val(selectedDay);

      var option = '<option value="month">month</option>';
      var selectedMon = "month";
      for (var m = 1; m <= 12; m++) {
        option += '<option value="' + m + '">' + (m < 10 ? '0' + m : m) + '</option>';
      }
      $('#month').append(option);
      $('#month').val(selectedMon);

      $('#month-damage').append(option);
      $('#month-damage').val(selectedMon);

      var d = new Date();
      var option = '<option value="year">year</option>';
      selectedYear = "year";
      for (var  y= 1930; y <= d.getFullYear(); y++) {// years start i
        option += '<option value="' + y + '">' + y + '</option>';
      }
      $('#year').append(option);
      $('#year').val(selectedYear);

      $('#year-damage').append(option);
      $('#year-damage').val(selectedYear);
    });

    function isLeapYear(year) {
      year = parseInt(year);
      if (year % 4 !== 0) {
        return false;
      } else if (year % 400 === 0) {
        return true;
      } else return year % 100 !== 0;
    }

    function change_year(select, type) {
      var idM = 'month';
      var idD = 'day';
      if (type) {
        idM = 'month-damage';
        idD = 'day-damage';
      }
      if (isLeapYear($(select).val())) {
        Days[1] = 29;
        if ($("#" + idM).val() === 2) {
          var day = $('#' + idD);
          var val = $(day).val();
          $(day).empty();
          var option = '<option value="day">day</option>';
          for (var i = 1; i <= Days[1]; i++) { //add option days
            option += '<option value="' + i + '">' + i + '</option>';
          }
          $(day).append(option);
          if (val > Days[month]) {
            val = 1;
          }
          $(day).val(val);
        }
      }
      else {
        Days[1] = 28;
      }
    }

    function change_month(select, type) {
      var idD = 'day';
      if (type) {
        idD = 'day-damage';
      }
      var day = $('#' + idD);
      var val = $(day).val();
      $(day).empty();
      var option = '<option value="day">day</option>';
      var month = parseInt($(select).val()) - 1;
      for (var i = 1; i <= Days[month]; i++) { //add option days
        option += '<option value="' + i + '">' + (i < 10 ? '0' + i : i) + '</option>';
      }
      $(day).append(option);
      if (val > Days[month]) {
        val = 1;
      }
      $(day).val(val);
    }
</script>
<style>
    .text-danger {
        color: red !important;
    }
</style>