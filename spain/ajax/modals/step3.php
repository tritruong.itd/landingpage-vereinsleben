<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="step3 text-center">
      <h1>Gracias!</h1>
      <p>Hemos recibido su solicitud y nos comunicaremos con usted a la brevedad por correo.</p>
      <button type="button" class="btn btn-secondary step3" data-dismiss="modal">Cerca</button><br/><br/><br/>
    </div>
</div>