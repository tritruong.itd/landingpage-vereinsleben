<?php
if (!isset($_SESSION)) {
  session_start();
}
include_once 'libraries/functions.php';
$com = (isset($_REQUEST['com'])) ? addslashes($_REQUEST['com']) : "";
$template = 'home';

switch ($com) {
    case 'datenschutz':
        $template = 'datenschutz';
        break;
    case 'inital-information':
        $template = 'erstinformation';
        break;
    case 'impressum':
        $template = 'impressum';
        break;
    case 'claim':
        $template = 'claim';
        break;
    case 'health':
        $template = 'health';
        break;
    default:
        break;
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha384-tsQFqpEReu7ZLhBV2VZlAu7zcOV+rXbYlF2cqB8txI/8aZajjp4Bqd+V6D5IgvKT"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
    <title>Cleverly Insured in Germany</title>
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
    <script>
      window.addEventListener("load", function(){
        window.cookieconsent.initialise({
          "palette": {
            "popup": {
              "background": "#aa0000",
              "text": "#ffdddd"
            },
            "button": {
              "background": "#ff0000"
            }
          },
          "theme": "classic"
        })});
    </script>
</head>
<body>
<div class="wrapper">
    <header class="<?php echo $template ?>">
        <div class="container">
            <div class="logo">
                <img src="assets/images/logo.png" alt="">
            </div>
            <a href="/spain" class="home-url">Home</a>
        </div>
    </header>
    <div class="main-content">
      <?php include('includes/' . $template . '.php') ?>
    </div>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-6">© Martina Martinez. 2019</div>
                <div class="col-md-6">
                    <ul class="menu-footer">
                        <li><a href="inital-information" data-link="erstinformation">Erstinformation</a> |</li>
                        <li><a href="datenschutz" data-link="datenschutz">Datenschutz</a> |</li>
                        <li><a href="impressum" data-link="impressum">Impressum</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="modalFormCenter"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="content"></div>
            </div>
        </div>
    </div>
    <div id="back-to-top"></div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script src="assets/js/jquery.mask.min.js"></script>
<script>
  $().ready(function () {
    $(".datepicker").datepicker();

    function scroll() {
      if ($(window).scrollTop() !== 0) {
        $('#back-to-top').fadeIn();
      } else {
        $('#back-to-top').fadeOut();
      }
    }

    scroll();
    $(window).scroll(function () {
      scroll();
    });
    $('#back-to-top').click(function () {
      $('html, body').animate({scrollTop: 0}, 500);
    });
    $('#modalForm').on('click', '.btn-close', function () {
      sessionStorage.setItem('clickClose', 'true');
    });
    $('#modalForm').on('click', '.btn-next', function () {
      sessionStorage.setItem('clickClose', 'false');
    });
    $('.checkbox-item').change(function () {
      var id = $(this).val();
      var parent = $(this).parents('.item');
      var name = parent.find('h2').html();
      var isChecked = $(this).is(':checked');
      showItem(id, name, isChecked);
    });
    $('.checkbox-item-health').change(function () {
      var id = $(this).val();
      var parent = $(this).parents('.item');
      var name = parent.find('h2').html();
      var isChecked = $(this).is(':checked');
      showItemHealth(id, name, isChecked);
    });
    $('[data-toggle="tooltip"]').tooltip();
    clickItem();

    $('#list_item').on('click', '.btn-submit', function () {
      var id = $(this).attr('data-id');
      $('#modalForm').modal({
        backdrop: 'static',
        keyboard: false  // to prevent closing with Esc button (if you want this too)
      });
      nextStep(id);
    });
    $('#modalForm').on('click', '.btn-close-without-save-data', function () {
      var name = $('#modalForm').find('h2').html();
      $.ajax({
        url: "ajax/process.php",
        method: 'post',
        data: {
          cmd: 'check_data_exist',
          id: $('input[name="id_box"]').val()
        },
        success: function (data) {
          if (data === '0') {
            $('input[name="id_box"]').prop('checked', false);
          }
          $('#modalForm').modal('hide');
        }
      });
      $.ajax({
        url: "ajax/process.php",
        method: 'post',
        data: {
          cmd: 'check_form_filled',
          id: $('input[name="id_box"]').val(),
          data: $('#frm').serializeArray()
        },
        success: function (data) {
          if (data === '1') {
            updateData(name);
          }
          $.ajax({
            url: 'ajax/process.php',
            method: 'post',
            data: {
              cmd: 'keep_data_when_close_modal',
              id: $('input[name="id_box"]').val(),
              name,
              value: $('#frm').serializeArray()
            },
            success: function() {
            }
          })
        }
      });
    });
    $('.item.item-homepage .icon').click(function () {
      var id = $(this).attr('data-id');
      var parent = $(this).parents('.item');
      var name = parent.find('h2').html();
      var isChecked = parent.find('input[type="checkbox"]').is(':checked');
      if (isChecked) {
        parent.find('input[type="checkbox"]').prop('checked', false);
        showItem(id, name, false);
      } else {
        parent.find('input[type="checkbox"]').prop('checked', true);
        showItem(id, name, true);
      }
    });
    $('.item-claim .icon').click(function () {
      var id = $(this).attr('data-id');
      var parent = $(this).parents('.item');
      var name = parent.find('h2').html();
      var isChecked = parent.find('input[type="checkbox"]').is(':checked');
      if (isChecked) {
        parent.find('input[type="checkbox"]').prop('checked', false);
        showItemClaim(id, name, false);
      } else {
        parent.find('input[type="checkbox"]').prop('checked', true);
        showItemClaim(id, name, true);
      }
    });
    $('.item-health .icon').click(function () {
      var id = $(this).attr('data-id');
      var parent = $(this).parents('.item');
      var name = parent.find('h2').html();
      var isChecked = parent.find('input[type="checkbox"]').is(':checked');
      if (isChecked) {
        parent.find('input[type="checkbox"]').prop('checked', false);
        showItemHealth(id, name, false);
      } else {
        parent.find('input[type="checkbox"]').prop('checked', true);
        showItemHealth(id, name, true);
      }
    });
  });
  window.onbeforeunload = function () {
    if ($('#modalForm').hasClass('show')) {
      removeItem($('input[name="id_box"]').val(), false);
    }
  };

  $('.checkbox-item-claim').change(function(){
    var id = $(this).val();
    var parent = $(this).parents('.item');
    var name = parent.find('h2').html();
    var isChecked = $(this).is(':checked');
    showItemClaim(id, name, isChecked);
  });

  function showItem(id, name, isChecked) {
    if (isChecked) {
      $('#modalForm').modal({
        backdrop: 'static',
        keyboard: false  // to prevent closing with Esc button (if you want this too)
      });
      $.ajax({
        url: "ajax/modals/" + id + '.php',
        method: 'get',
        success: function (data) {
          $('#modalForm .modal-content .content').html(data);
          $('input[name="id_box"]').val(id);
          var count = 0;
          $('#frm').validate({
            invalidHandler: function (e, validator) {
              $('p.text-danger').addClass('d-none');
              var errors = validator.numberOfInvalids();
              if ($('.form-group .frm-multiple-checkbox').not('.d-none').length) {
                $('.form-group .frm-multiple-checkbox').each(function () {
                  if ($(this).find('input[type="checkbox"]').is(':checked')) {
                    count++;
                  }
                });
                if (count) {
                  $('p.text-danger').addClass('d-none');
                  $.ajax({
                    url: "ajax/process.php",
                    method: 'post',
                    data: {
                      cmd: 'check_form_filled',
                      id: $('input[name="id_box"]').val(),
                      data: $('#frm').serializeArray()
                    },
                    success: function (data) {
                      if (data === '1') {
                        updateData(name);
                      }
                      $.ajax({
                        url: 'ajax/process.php',
                        method: 'post',
                        data: {
                          cmd: 'keep_data_when_close_modal',
                          id: $('input[name="id_box"]').val(),
                          name,
                          value: $('#frm').serializeArray()
                        },
                        success: function() {
                        }
                      })
                    }
                  });
                } else {
                  $('p.text-danger').removeClass('d-none');
                }
                if (count === 0) {
                  errors += 1;
                }
              }
              if (errors) {
                var message = errors === 1 ? 'Te perdiste 1 campo. Se ha resaltado a continuación' : 'Te perdiste ' + errors +
                  ' campo. Se ha resaltado a continuación.';
                $("div.error span").html(message);
                $("div.error").show();
              } else {
                $("div.error").hide();
              }
            },
            submitHandler: function () {
              if (sessionStorage.getItem('clickClose') === 'false') {
                if ($('.form-group .frm-multiple-checkbox').not('.d-none').length) {
                  $('.form-group .frm-multiple-checkbox').each(function () {
                    if ($(this).find('input[type="checkbox"]').is(':checked')) {
                      count++;
                    }
                  });
                  if (count) {
                    $('p.text-danger').addClass('d-none');
                    $.ajax({
                      url: "ajax/process.php",
                      method: 'post',
                      data: {
                        cmd: 'check_form_filled',
                        id: $('input[name="id_box"]').val(),
                        data: $('#frm').serializeArray()
                      },
                      success: function (data) {
                        if (data === '1') {
                          updateData(name);
                        }
                        $.ajax({
                          url: 'ajax/process.php',
                          method: 'post',
                          data: {
                            cmd: 'keep_data_when_close_modal',
                            id: $('input[name="id_box"]').val(),
                            name,
                            value: $('#frm').serializeArray()
                          },
                          success: function() {
                          }
                        })
                      }
                    });
                  } else {
                    $('p.text-danger').removeClass('d-none');
                    return false;
                  }
                }
                updateData(name);
                nextStep(id);
              } else {
                if ($('.form-group .frm-multiple-checkbox').not('.d-none').length) {
                  $('.form-group .frm-multiple-checkbox').each(function () {
                    if ($(this).find('input[type="checkbox"]').is(':checked')) {
                      count++;
                    }
                  });
                  if (count) {
                    $('p.text-danger').addClass('d-none');
                    $.ajax({
                      url: "ajax/process.php",
                      method: 'post',
                      data: {
                        cmd: 'check_form_filled',
                        id: $('input[name="id_box"]').val(),
                        data: $('#frm').serializeArray()
                      },
                      success: function (data) {
                        if (data === '1') {
                          updateData(name);
                        }
                        $.ajax({
                          url: 'ajax/process.php',
                          method: 'post',
                          data: {
                            cmd: 'keep_data_when_close_modal',
                            id: $('input[name="id_box"]').val(),
                            name,
                            value: $('#frm').serializeArray()
                          },
                          success: function() {
                          }
                        })
                      }
                    });
                    $('#modalForm').modal('hide');
                  } else {
                    $('p.text-danger').removeClass('d-none');
                    return false;
                  }
                } else {
                  $.ajax({
                    url: "ajax/process.php",
                    method: 'post',
                    data: {
                      cmd: 'check_form_filled',
                      id: $('input[name="id_box"]').val(),
                      data: $('#frm').serializeArray()
                    },
                    success: function (data) {
                      if (data === '1') {
                        updateData(name);
                      }
                      $.ajax({
                        url: 'ajax/process.php',
                        method: 'post',
                        data: {
                          cmd: 'keep_data_when_close_modal',
                          id: $('input[name="id_box"]').val(),
                          name,
                          value: $('#frm').serializeArray()
                        },
                        success: function() {
                        }
                      })
                    }
                  });
                  $('#modalForm').modal('hide');
                }
              }
            }
          });
          jQuery.extend(jQuery.validator.messages, {
            required: "este campo es requerido.",
          });
        }
      });
    } else {
      removeItem(id);
    }
  }

  function showItemClaim(id, name, isChecked) {
    if (isChecked) {
      $('#modalForm').modal({
        backdrop: 'static',
        keyboard: false  // to prevent closing with Esc button (if you want this too)
      });
      $.ajax({
        url: "ajax/modals/claims/" + id + '.php',
        method: 'get',
        success: function (data) {
          $('#modalForm .modal-content .content').html(data);
          $('#frm').validate({
            invalidHandler: function (e, validator) {
              var count = 0;
              var count1 = 0;
              var count2 = 0;
              $('p.text-danger').addClass('d-none');
              var errors = validator.numberOfInvalids();
              if ($('.form-group .frm-multiple-checkbox').length) {
                $('.form-group .frm-multiple-checkbox').each(function () {
                  if ($(this).find('input[type="checkbox"]').is(':checked')) {
                    count2++;
                  }
                });
              }
              if ($('#day').length) {
                if ($('#day').val() !== 'day' && $('#month').val() !== 'month' && $('#year').val() !== 'year') {
                  count++;
                }
                if ($('#day-damage').val() !== 'day' && $('#month-damage').val() !== 'month' && $('#year-damage').val() !== 'year') {
                  count1++;
                }
                if (count1 === 0) {
                  $('p.error-damage').removeClass('d-none');
                  errors += 1;
                }
                if (count === 0) {
                  $('p.error-birthday').removeClass('d-none');
                  errors += 1;
                }
                if (count2 === 0) {
                  $('p.error-checkbox').removeClass('d-none');
                  errors += 1;
                }
              }
              if (errors) {
                var message = errors === 1 ? 'Te perdiste 1 campo. Se ha resaltado a continuación' : 'Te perdiste ' + errors +
                  ' campo. Se ha resaltado a continuación.';
                $("div.error span").html(message);
                $("div.error").show();
              } else {
                $("div.error").hide();
              }
            },
            submitHandler: function () {
              if (!$('input[name="initial-information"]').is(':checked')) {
                alert('Please confirm that you have downloaded and read the data privacy statement (“Datenschutz“) at the bottom of this page and agree to the terms.');
                return false;
              }
              $('.img-loading').removeClass('d-none');
              $('.btn.btn-primary').addClass('d-none');
              var id = $('input[name="id_box"]').val();
              if (id.length) {
                $.ajax({
                  url: "ajax/process.php",
                  method: 'post',
                  data: {
                    cmd: 'submit_claim',
                    id: id,
                    data: $('#frm').serializeArray()
                  },
                  success: function () {
                    $('.img-loading').addClass('d-none');
                    $('.btn.btn-primary').removeClass('d-none');
                    $('input[name="id_box"]').prop('checked', false);
                    $.ajax({
                      url: 'ajax/modals/step3.php',
                      method: 'get',
                      success: function (dataxs) {
                        $('#modalForm .modal-content .content').html(dataxs);
                      }
                    });
                  }
                });
              }
            }
          });
        }
      });
    } else {
      $('#' + id).prop('checked', false);
    }
  }

  function showItemHealth(id, name, isChecked) {
    if (isChecked) {
      $('#modalForm').modal({
        backdrop: 'static',
        keyboard: false  // to prevent closing with Esc button (if you want this too)
      });
      $.ajax({
        url: "ajax/modals/healths/" + id + '.php',
        method: 'get',
        success: function (data) {
          $('#modalForm .modal-content .content').html(data);
          $('input[name="id_box"]').val(id);
          $('#frm').validate({
            invalidHandler: function (e, validator) {
              var count = 0;
              var count1 = 0;
              var count2 = 0;
              $('p.text-danger').addClass('d-none');
              var errors = validator.numberOfInvalids();
              if ($('.form-group .frm-multiple-checkbox').length) {
                $('.form-group .frm-multiple-checkbox').each(function () {
                  if ($(this).find('input[type="checkbox"]').is(':checked')) {
                    count2++;
                  }
                });
              }
              if ($('#day').length) {
                if ($('#day').val() !== 'day' && $('#month').val() !== 'month' && $('#year').val() !== 'year') {
                  count++;
                }
                if ($('#day-damage').val() !== 'day' && $('#month-damage').val() !== 'month' && $('#year-damage').val() !== 'year') {
                  count1++;
                }
                if (count1 === 0) {
                  $('p.error-damage').removeClass('d-none');
                  errors += 1;
                }
                if (count === 0) {
                  $('p.error-birthday').removeClass('d-none');
                  errors += 1;
                }
                if (count2 === 0) {
                  $('p.error-checkbox').removeClass('d-none');
                  errors += 1;
                }
              }
              if (errors) {
                var message = errors === 1 ? 'Te perdiste 1 campo. Se ha resaltado a continuación' : 'Te perdiste ' + errors +
                  ' campo. Se ha resaltado a continuación.';
                $("div.error span").html(message);
                $("div.error").show();
              } else {
                $("div.error").hide();
              }
            },
            submitHandler: function () {
              var id = $('input[name="id_box"]').val();
              $.ajax({
                url: 'ajax/process.php',
                method: 'post',
                data: {
                  cmd: 'update_item_health',
                  id: id,
                  name: name,
                  value: $('#frm').serializeArray()
                },
                success: function (datax) {
                  $.ajax({
                    url: 'ajax/modals/healths/complete-information.php',
                    method: 'get',
                    success: function (dataxs) {
                      $('#modalForm .modal-content .content').html(dataxs);
                      $('input[name="id_box"]').val(id);
                      $('#frm-step2').validate({
                        invalidHandler: function (e, validator) {
                          var errors = validator.numberOfInvalids();
                          if (errors) {
                            var message = errors === 1 ? 'Te perdiste 1 campo. Se ha resaltado a continuación' : 'Te perdiste ' + errors + ' campo. Se ha resaltado a continuación.';
                            $("div.error span").html(message);
                            $("div.error").show();
                          } else {
                            $("div.error").hide();
                          }
                        },
                        submitHandler: function () {
                          if ($('input[name=privacy-information]').length <= 0 || !$('input[name="privacy-information"]').is(':checked')) {
                            alert('Please confirm that you read privacy information and agree to the terms.');
                            return false;
                          }
                          if (!$('input[name="initial-information"]').is(':checked')) {
                            alert('Please confirm that you have downloaded and read the initial information (“Erstinformation”) and data privacy statement (“Datenschutz“) at the bottom of this page and agree to the terms.');
                            return false;
                          }
                          $('.img-loading').removeClass('d-none');
                          $('.btn.btn-primary.pull-right').addClass('d-none');
                          $.ajax({
                            url: 'ajax/process.php',
                            method: 'post',
                            data: {
                              cmd: 'submit_form_health',
                              id: id,
                              data: {
                                firstName: $('#first-name').val(),
                                lastName: $('#last-name').val(),
                                postCode: $('#postCode').val(),
                                additionalInformation: $('#additional-information').val(),
                                email: $('#email').val(),
                                day: $('#day').val(),
                                month: $('#month').val(),
                                year: $('#year').val(),
                              }
                            },
                            dataType: 'json',
                            success: function (data) {
                              data.listId.forEach(function (val) {
                                $('#' + val).prop('checked', false);
                              });
                              $('.badge-danger').html(data.total);
                              $.ajax({
                                url: "ajax/listItem.php",
                                method: 'get',
                                success: function (data) {
                                  $('#list_item').html(data);
                                  clickItem();
                                }
                              });
                              $('.img-loading').addClass('d-none');
                              $('.btn.btn-primary.pull-right').removeClass('d-none');
                              $.ajax({
                                url: 'ajax/modals/step3.php',
                                method: 'get',
                                success: function (dataxs) {
                                  $('#modalForm .modal-content .content').html(dataxs);
                                }
                              });
                            },
                            error: function (error) {
                              console.log(error);
                            }
                          });
                        }
                      });
                    }
                  });
                }
              });
            }
          });
        }
      });
    } else {
      $('#' + id).prop('checked', false);
    }
  }

  function clickItem() {
    $('.btn-edit span').click(function () {
      var id = $(this).attr('data-id');
      $('#modalForm').modal({
        backdrop: 'static',
        keyboard: false  // to prevent closing with Esc button (if you want this too)
      });

      $.ajax({
        url: "ajax/modals/" + id + '.php',
        method: 'get',
        success: function (data) {
          $('#modalForm .modal-content .content').html(data);
          $('input[name="id_box"]').val(id);
          $('#frm').validate({
            invalidHandler: function (e, validator) {
              var errors = validator.numberOfInvalids();
              if (errors) {
                var message = errors === 1 ? 'Te perdiste 1 campo. Se ha resaltado a continuación' : 'Te perdiste ' + errors + ' campo. Se ha resaltado a continuación.';
                $("div.error span").html(message);
                $("div.error").show();
              } else {
                $("div.error").hide();
              }
            },
            submitHandler: function () {
              var count = 0;
              if (sessionStorage.getItem('clickClose') === 'false') {
                if ($('.form-group .frm-multiple-checkbox').not('.d-none').length) {
                  $('.form-group .frm-multiple-checkbox').each(function () {
                    if ($(this).find('input[type="checkbox"]').is(':checked')) {
                      count++;
                    }
                  });
                  if (count) {
                    $('p.text-danger').addClass('d-none');
                    updateData(name);
                  } else {
                    $('p.text-danger').removeClass('d-none');
                    return false;
                  }
                }
                updateData(name);
                nextStep(id);
              } else {
                if ($('.form-group .frm-multiple-checkbox').not('.d-none').length) {
                  $('.form-group .frm-multiple-checkbox').each(function () {
                    if ($(this).find('input[type="checkbox"]').is(':checked')) {
                      count++;
                    }
                  });
                  if (count) {
                    $('p.text-danger').addClass('d-none');
                    updateData(name);
                    $('#modalForm').modal('hide');
                  } else {
                    $('p.text-danger').removeClass('d-none');
                    return false;
                  }
                } else {
                  updateData(name);
                  $('#modalForm').modal('hide');
                }
              }
            }
          });
        }
      });
    });
  }

  function updateData(name) {
    if (name === undefined) {
      name = '';
    }
    var id = $('input[name="id_box"]').val();
    $.ajax({
      url: 'ajax/process.php',
      method: 'post',
      data: {
        cmd: 'update_item',
        id: id,
        name: name,
        value: $('#frm').serializeArray()
      },
      success: function (data) {
        $('.badge-danger').html(data);
        $('#' + id).prop('checked', true);
        $.ajax({
          url: "ajax/listItem.php",
          method: 'get',
          success: function (data) {
            $('#list_item').html(data);
            clickItem();
          }
        });
      }
    });
  }

  function removeItem(id, hasRemove) {
    $.ajax({
      url: 'ajax/process.php',
      method: 'post',
      data: {
        cmd: 'remove_item',
        id: id
      },
      success: function (data) {
        $('.badge-danger').html(data);
        $.ajax({
          url: "ajax/listItem.php",
          method: 'get',
          success: function (data) {
            $('#list_item').html(data);
            clickItem();
          }
        });
        if (hasRemove) {
          $('#' + id).prop('checked', false);
        }
      }
    });
  }

  function nextStep(id) {
    $.ajax({
      url: 'ajax/modals/step2.php',
      method: 'get',
      success: function (datax) {
        $('#modalForm .modal-content .content').html(datax);
        $('input[name="id_box"]').val(id);
        $('#frm-step2').validate({
          invalidHandler: function (e, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
              var message = errors === 1 ? 'Te perdiste 1 campo. Se ha resaltado a continuación.' : 'Te perdiste ' + errors + ' campo. Se ha resaltado a continuación';
              $("div.error span").html(message);
              $("div.error").show();
            } else {
              $("div.error").hide();
            }
          },
          submitHandler: function () {
            if (!$('input[name="initial-information"]').is(':checked')) {
              alert('Confirme que ha descargado y leído la información inicial ("Erstinformation") y la declaración de privacidad de datos ("Datenschutz") en la parte inferior de esta página y acepta los términos.');
              return false;
            }
            $('.img-loading').removeClass('d-none');
            $('.btn.btn-primary.pull-right').addClass('d-none');
            $.ajax({
              url: 'ajax/process.php',
              method: 'post',
              data: {
                cmd: 'submit_form',
                id: $('input[name="id_box"]').val(),
                data: {
                  name: $('#name').val(),
                  firstName: $('#firstName').val(),
                  streetAddress: $('#streetAddress').val(),
                  postCode: $('#postCode').val(),
                  place: $('#place').val(),
                  country: $('#country').val(),
                  email: $('#email').val(),
                  day: $('#day').val(),
                  month: $('#month').val(),
                  year: $('#year').val(),
                  initialInformation: $('#initial-information').val(),
                  dataProtection: $('#data-protection').val(),
                  martitalStatus: $('#martital-status').val(),
                  occupationGroup: $('#occupation-group').val(),
                  pracitsedProfession: $('#pracitsed-profession').val(),
                  additionalInformation: $('textarea#additional-information-question').val()
                }
              },
              dataType: 'json',
              success: function (data) {
                data.listId.forEach(function (val) {
                  $('#' + val).prop('checked', false);
                });
                $('.badge-danger').html(data.total);
                $.ajax({
                  url: "ajax/listItem.php",
                  method: 'get',
                  success: function (data) {
                    $('#list_item').html(data);
                    clickItem();
                  }
                });
                $('.img-loading').addClass('d-none');
                $('.btn.btn-primary.pull-right').removeClass('d-none');
                $.ajax({
                  url: 'ajax/modals/step3.php',
                  method: 'get',
                  success: function (dataxs) {
                    $('#modalForm .modal-content .content').html(dataxs);
                  }
                });
              },
              error: function (error) {
                console.log(error);
              }
            });
          }
        });
      }
    });
  }
</script>
</body>
</html>