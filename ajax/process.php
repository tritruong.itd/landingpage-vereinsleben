<?php
session_start();
include_once '../libraries/functions.php';

$cmd = $_POST['cmd'];
$id = $_POST['id'];
if (isset($_POST['data'])) {
    $data = $_POST['data'];
}
if (isset($_POST['name'])) {
    $name = trim(strip_tags($_POST['name']));
}

if (isset($_POST['value'])) {
    $value = array();
    $ignoreElements = array('name', 'firstName', 'streetAddress', 'postCode', 'place', 'country', 'email', 'id_box');
    $idx = 0;
    foreach ($_POST['value'] as $val) {
        $names = explode('_', $val['name']);
        if (count($names) >= 2) {
            if (!strpos($names[count($names) - 1], '[]')) {
                $idx = +$names[count($names) - 1];
            }
        }
        $index_free = strpos($val['name'], 'free_of_complaint_' . $idx . '_');
        $index_treatment = strpos($val['name'], 'treatment_finished_' . $idx . '_');
        if (!in_array($val['name'], $ignoreElements)) {
            if ($val['name'] == 'cover-law[]') {
                if (isset($value['cover-law'])) {
                    if (strlen($value['cover-law']) <= 0) {
                        $value['cover-law'] = $val['value'];
                    } else {
                        $value['cover-law'] .= ' - ' . $val['value'];
                    }
                } else {
                    $value['cover-law'] = $val['value'];
                }
            } else if ($val['name'] == 'travel-health-insurance[]') {
                if (isset($value['travel-health-insurance'])) {
                    if (strlen($value['travel-health-insurance']) <= 0) {
                        $value['travel-health-insurance'] = $val['value'];
                    } else {
                        $value['travel-health-insurance'] .= ' - ' . $val['value'];
                    }
                } else {
                    $value['travel-health-insurance'] = $val['value'];
                }
            } else if ($val['name'] == 'outpatient-coverage[]') {
                if (isset($value['outpatient-coverage'])) {
                    if (strlen($value['outpatient-coverage']) <= 0) {
                        $value['outpatient-coverage'] = $val['value'];
                    } else {
                        $value['outpatient-coverage'] .= ' - ' . $val['value'];
                    }
                } else {
                    $value['outpatient-coverage'] = $val['value'];
                }
            } else if ($val['name'] == 'additional_'. $idx .'[]') {
                if (!isset($value['question_additional_' . $idx])) {
                    $value['question_additional_' . $idx] = array();
                }
                array_push($value['question_additional_' . $idx], $val['value']);
            } else if ($val['name'] == 'number_teeth_'. $idx .'') {
                if (!isset($value['question_number_teeth_' . $idx])) {
                    $value['question_number_teeth_' . $idx] = array();
                }
                array_push($value['question_number_teeth_' . $idx], $val['value']);
            } else if ($val['name'] == 'additional_'. $idx) {
                if (!isset($value['question_additional_' . $idx])) {
                    $value['question_additional_' . $idx] = array();
                }
                array_push($value['question_additional_' . $idx], $val['value']);
            } else if ($val['name'] == 'treat_from_'. $idx .'[]') {
                if (!isset($value['question_treat_from_' . $idx])) {
                    $value['question_treat_from_' . $idx] = array();
                }
                array_push($value['question_treat_from_' . $idx], $val['value']);
            } else if ($val['name'] == 'treat_to_'. $idx .'[]') {
                if (!isset($value['question_treat_to_' . $idx])) {
                    $value['question_treat_to_' . $idx] = array();
                }
                array_push($value['question_treat_to_' . $idx], $val['value']);
            } else if ($val['name'] == 'treatment_'. $idx .'[]') {
                if (!isset($value['question_treatment_' . $idx])) {
                    $value['question_treatment_' . $idx] = array();
                }
                array_push($value['question_treatment_' . $idx], $val['value']);
            } else if ($val['name'] == 'medication_'. $idx .'[]') {
                if (!isset($value['question_medication_' . $idx])) {
                    $value['question_medication_' . $idx] = array();
                }
                array_push($value['question_medication_' . $idx], $val['value']);
            } else if ($val['name'] == 'medication_from_'. $idx .'[]') {
                if (!isset($value['question_medication_from_' . $idx])) {
                    $value['question_medication_from_' . $idx] = array();
                }
                array_push($value['question_medication_from_' . $idx], $val['value']);
            } else if ($val['name'] == 'medication_to_'. $idx .'[]') {
                if (!isset($value['question_medication_to_' . $idx])) {
                    $value['question_medication_to_' . $idx] = array();
                }
                array_push($value['question_medication_to_' . $idx], $val['value']);
            } else if ($val['name'] == 'treatment_finished_addition_info_'. $idx .'[]') {
                if (!isset($value['question_treatment_finished_addition_info_' . $idx])) {
                    $value['question_treatment_finished_addition_info_' . $idx] = array();
                }
                array_push($value['question_treatment_finished_addition_info_' . $idx], $val['value']);
            } else if ($val['name'] == 'free_of_complaint_addition_info_'. $idx .'[]') {
                if (!isset($value['question_free_of_complaint_addition_info_' . $idx])) {
                    $value['question_free_of_complaint_addition_info_' . $idx] = array();
                }
                array_push($value['question_free_of_complaint_addition_info_' . $idx], $val['value']);
            } else if (strlen($index_treatment)) {
                if (!isset($value['question_treatment_finished_' . $idx])) {
                    $value['question_treatment_finished_' . $idx] = array();
                }
                array_push($value['question_treatment_finished_' . $idx], $val['value']);
            } else if (strlen($index_free)) {
                if (!isset($value['question_free_of_complaint_' . $idx])) {
                    $value['question_free_of_complaint_' . $idx] = array();
                }
                array_push($value['question_free_of_complaint_' . $idx], $val['value']);
            } else {
                $value[$val['name']] = $val['value'];
            }
        }
    }
}

switch ($cmd) {
    case 'add_item':
        addItem($id, $name, $value);
        break;
    case 'update_item':
        updateItem($id, $name, $value);
        break;
    case 'remove_item':
        removeItem($id);
        break;
    case 'submit_form':
        submitForm($data);
        break;
    case 'check_data_exist':
        checkExistItem($id);
        break;
    case 'check_form_filled':
        checkFormFilled($id, $data);
        break;
    case 'keep_data_when_close_modal':
        keepDataWhenCloseModal($id, $name, $value);
        break;
    case 'submit_claim':
        submitClaim($id, $data);
        break;
    case 'update_item_health':
        updateItemHealth($id, $name, $value);
        break;
    case 'submit_form_health':
        submitHealth($id, $data);
        break;
    default:
        break;
}

function checkExistItem($id) {
  $ret = 0;
  if (!isset($_SESSION['cart'])) $ret = 0;
  foreach ($_SESSION['cart'] as $cart) {
    if ($cart['id'] == $id) {
      $ret = 1;
    }
  }
  echo $ret;
}

function submitForm($data) {
    $subject = 'Clever insured in Germany';
    $body = '<p>Fist Name ' . $data['firstName'] . '</p>';
    $body .= '<p>Last Name ' . $data['name'] . '</p>';
    $body .= '<p>Street, house number: ' . $data['streetAddress'] . '</p>';
    $body .= '<p>Postcode: ' . $data['postCode'] . '</p>';
    $body .= '<p>Place: ' . $data['place'] . '</p>';
    $body .= '<p>Country: ' . $data['country'] . '</p>';
    $body .= '<p>E-Mail: ' . $data['email'] . '</p>';
    $body .= '<p>Date of birth: ' . $data['day'] . '/' . $data['month'] . '/' . $data['year'] . '</p>';
    $body .= '<p>Marital Status: ' . $data['martitalStatus'] . '</p>';
    $body .= '<p>Occupation group: ' . $data['occupationGroup'] . '</p>';
    $body .= '<p>Current practised profession: ' . $data['pracitsedProfession'] . '</p>';
    $body .= '<p>Additional information/question: ' . $data['additionalInformation'] . '</p>';
    $body .= '<hr>';
    $body .= '<p>Your clever insured</p>';
    $arrCart = array();
    if (count($_SESSION['cart'])) {
        foreach ($_SESSION['cart'] as $key => $cart) {
            $body .= getBodyMail($key, $cart);
            $arrCart[] = $cart['id'];
        }
    }
    sendEmail(_MAIL_USER, $subject, $body, $data['name'] . ' ' . $data['firstName']);
    removeItems($arrCart);
}


function submitClaim($id, $data) {
    $typeOfDamages = array(
        'fire-damage' => 'Fire damage',
        'mains-water-damage' => 'Mains water damage',
        'storm-damage' => 'Storm damage',
        'hail-damage' => 'Hail damage',
        'burglary-robbery-vandalism' => 'Burglary / Robbery / Vandalism',
        'glass-breakage' => 'Glass Breakage',
        'bicycle-theft' => 'Bicycle Theft',
        'natural-hazards' => 'Natural Hazards',
        'other' => 'Others (Eingabefeld für andere Schäden)',
    );
    $subject = 'Clever insured in Germany';
    $body =  '<p><strong>INSURED PERSON</strong></p>';
    $body .= '<p>Full Name: ' . getValueByKey($data, 'first-name') . ' ' . getValueByKey($data, 'last-name') . '</p>';
    $body .= '<p>Date of birth: ' . getValueByKey($data, 'yyyy') . '/' . getValueByKey($data, 'mm') . '/' . getValueByKey($data, 'dd') . '</p>';
    $body .= '<p>Street: ' . getValueByKey($data, 'street') . '</p>';
    $body .= '<p>Postcode, City: ' . getValueByKey($data, 'postcode') . '</p>';
    $body .= '<p>E-Mail: ' . getValueByKey($data, 'email') . '</p>';
    if ($id == 'household-insurance') {
        $body .= '<p><strong>TYPE OF DAMAGE</strong></p>';
        $body .= '<p>:' . (getValueByKey($data, 'type-of-damage') == "other") ? getValueByKey($data, 'other-detail') : $typeOfDamages[getValueByKey($data, 'type-of-damage')] . '</p>';
    } else {
        $body .= '<p><strong>DAMAGED PARTY</strong></p>';
        $body .= '<p>Name ' . getValueByKey($data, 'name') . '</p>';
        $body .= '<p>Street: ' . getValueByKey($data, 'street-damaged') . '</p>';
        $body .= '<p>Postcode, City: ' . getValueByKey($data, 'postcode-damaged') . '</p>';
        $body .= '<p>Phone: ' . getValueByKey($data, 'phone-damaged') . '</p>';
    }
    $body .= '<p><strong>DETAILS OF DAMAGE</strong></p>';
    $body .= '<p>Date of damage: ' . getValueByKey($data, 'yyyy-damage') . '/' . getValueByKey($data, 'mm-damage') . '/' . getValueByKey($data, 'dd-damage') . '</p>';
    $body .= '<p>Damage location: ' . getValueByKey($data, 'damage-location') . '</p>';
    $body .= '<p>Expected lost in euros: ' . getValueByKey($data, 'expected-lost-in-euros') . '</p>';
    if ($id == 'household-insurance') {
        $body .= '<p>List of damaged objects: ' . getValueByKey($data, 'list-of-damaged') . '</p>';
    } else {
        $body .= '<p>List of damaged objects or injured person: ' . getValueByKey($data, 'list-of-damaged') . '</p>';
    }
    $body .= '<p>Detailed description of the cause and the circumstances of damage: ' . getValueByKey($data, 'detail-damage') . '</p>';
    $body .= '<p>Additional information: ' . getValueByKey($data, 'detail-damage') . '</p>';
    $body .= '<p>Evidence of the incurred costs in euros: ' . getValueByKey($data, 'incurred-cost') . '</p>';
    sendEmail('mm@martina-martinez.de', $subject, $body, getValueByKey($data, 'first-name') . ' ' . getValueByKey($data, 'last-name'));
}

function submitHealth($id, $data) {
    $array_questions = array(
        '1)... of the heart, circulatory system, vessels (e.g., pain in the cardiac region, heart attack, heart failure, dysrhythmia, physician-determined high blood pressure, stroke, vascular changes, circulatory disorders, thrombosis)?',
        '2)... the lungs, bronchi, trachea, nose (e.g., inflammation, asthma, chronic bronchitis, sleep apnea)',
        '3)... the esophagus, stomach, intestine, liver, pancreas, bile, spleen (e.g., inflammation, ulcer, bleeding, high liver  values)?',
        '4)... the kidneys, urinary tract, reproductive organs, breast (e.g., inflammation, stone formation, cystic kidney, blood or protein   secretion)?',
        '5)... the metabolism, blood, the thyroid (e.g., diabetes, increased lipid levels, gout, anemia, leukemia, lymph nodes)?',
        '6) ... of the brain, nervous system, psyche (e.g., multiple sclerosis, epilepsy, migraine, paralysis, fatigue syndrome, chronic pain, dizziness, anxiety disorder, depression, attention deficit syndrome, eating disorder, psychosomatic disorder, addiction, suicide   attempt)?',
        '7) ... the bones, joints, muscles, tendons or ligaments, as well as the spine, intervertebral discs (e.g., movement restrictions, misalignments, wear, inflammation, rheumatism,   fibromyalgia)?',
        '8) ... of the eyes (e.g., visual disturbances, retinal diseases, increased eye pressure, lack of vision of more than 6 diopters)?',
        '9) ... of the ears (e.g., hearing loss, sudden deafness, ear noises, balance disorders)?',
        '10) ... the skin or allergies (for example, inflammation, eczema, atopic dermatitis, psoriasis, house dust, pollen, pet hair, food or occupational allergy)?',
        '11) ... such as infectious diseases, sexually transmitted diseases, tropical diseases (e.g., malaria, hepatitis, tuberculosis, borreliosis)?',
        '12) ... the psyche (eg, depression, anxiety disorders, attention deficit syndrome, chronic fatigue syndrome, psychosomatic disorder), addiction (e.g., taking drugs, consequences of drinking alcohol)?',
        '13) ... like benign or malignant tumors (e.g., cancer, cyst, adenoma), was there a suicide attempt or was an HIV infection detected (positive AIDS test)?',
        '14) Is or has there been an incapacity to work or severe disability in the last 5 years?',
        '15) Have you been examined, advised or treated in the past 5 years on an outpatient or inpatient basis by doctors or other practitioners (e.g.,  Alternative Practitioner, Psychotherapist)?',
        '16) Have any medicines been prescribed or taken in the last 12 months?',
        '17) Have you undergone surgery, hospitalization or stay at a health resort, or have you had an accident, injury or poisoning over the past 10 years?'
    );
    $array_questions_2 = array(
        '1)... of the heart, circulatory system, vessels (e.g., pain in the cardiac region, heart attack, heart failure, dysrhythmia, physician-determined high blood pressure, stroke, vascular changes, circulatory disorders, thrombosis)?',
        '2)... the lungs, bronchi, trachea, nose (e.g., inflammation, asthma, chronic bronchitis, sleep apnea)',
        '3)... the esophagus, stomach, intestine, liver, pancreas, bile, spleen (e.g., inflammation, ulcer, bleeding, high liver  values)?',
        '4)... the kidneys, urinary tract, reproductive organs, breast (e.g., inflammation, stone formation, cystic kidney, blood or protein   secretion)?',
        '5)... the metabolism, blood, the thyroid (e.g., diabetes, increased lipid levels, gout, anemia, leukemia, lymph nodes)?',
        '6) ... of the brain, nervous system, psyche (e.g., multiple sclerosis, epilepsy, migraine, paralysis, fatigue syndrome, chronic pain, dizziness, anxiety disorder, depression, attention deficit syndrome, eating disorder, psychosomatic disorder, addiction, suicide   attempt)?',
        '7) ... the bones, joints, muscles, tendons or ligaments, as well as the spine, intervertebral discs (e.g., movement restrictions, misalignments, wear, inflammation, rheumatism,   fibromyalgia)?',
        '8) ... of the eyes (e.g., visual disturbances, retinal diseases, increased eye pressure, lack of vision of more than 6 diopters)?',
        '9) ... of the ears (e.g., hearing loss, sudden deafness, ear noises, balance disorders)?',
        '10) ... the skin or allergies (for example, inflammation, eczema, atopic dermatitis, psoriasis, house dust, pollen, pet hair, food or occupational allergy)?',
        '11) ... such as infectious diseases, sexually transmitted diseases, tropical diseases (e.g., malaria, hepatitis, tuberculosis, borreliosis)?',
        '12) ... the psyche (eg, depression, anxiety disorders, attention deficit syndrome, chronic fatigue syndrome, psychosomatic disorder), addiction (e.g., taking drugs, consequences of drinking alcohol)?',
        '13) ... like benign or malignant tumors (e.g., cancer, cyst, adenoma), was there a suicide attempt or was an HIV infection detected (positive AIDS test)?',
        '14) Is or has there been an incapacity to work or severe disability in the last 5 years?',
        '15) Have you been examined, advised or treated in the past 5 years on an outpatient or inpatient basis by doctors or other practitioners (e.g.,  Alternative Practitioner, Psychotherapist)?',
        '16) Have any medicines been prescribed or taken in the last 12 months?',
        '17) Have you undergone surgery, hospitalization or stay at a health resort, or have you had an accident, injury or poisoning over the past 10 years?',
        '18) Is there currently a dental treatment, the preparation or renewal of dentures, a periodontal treatment or a jaw (tooth) regulation, or have such measures been inteded or advised?',
        '19) How many teeth are there missing that have not yet been replaced (except for milk and wisdom teeth, as well as teeth where the gaps have been closed by adjacent teeth)?'
    );
    $health = getHealthDataById($id);

    $subject = 'Clever insured in Germany';
    $body =  '<p><strong>Health Questionnaire</strong></p>';
    $body .= '<p>First Name: ' . $data['firstName'] . '</p>';
    $body .= '<p>Last Name: ' . $data['lastName'] . '</p>';
    $body .= '<p>Full Name: ' . $data['firstName'] . ' ' . $data['lastName'] . '</p>';
    $body .= '<p>Date of birth: ' . $data['day'] . '.' . $data['month'] . '.' . $data['year'] . '</p>';
    $body .= '<p>E-Mail: ' . $data['email'] . '</p>';
    $body .= '<p>Additional Information: ' . $data['additionalInformation'] . '</p>';
    $body .= '<p>Height: ' . getAnswerByKey($health['value'], 'height') . '</p>';
    $body .= '<p>Weight: ' . getAnswerByKey($health['value'], 'weight') . '</p>';
    $body .= '<p><strong>'. trim(strip_tags($health['name'])) . '</strong></p>';
    if ($id == 'life-insurance' || $id == 'disability-insurance') {
        foreach ($array_questions as $index => $question) {
            $body .= '<p>' . $question . ': ' . getAnswerByKey($health['value'], 'question_' . $index) . '</p>';
            if (getAnswerByKey($health['value'], 'question_' . $index) == 'yes') {
                $datas = getAnswerByKey($health['value'], 'question_additional_' . $index);
                foreach($datas as $idx => $add) {
                    if ($index == 13) {
                        $body .= '<div style="padding-left: 15px; '. ($idx ? 'padding-top: 10px; margin-top: 10px; border-top: 1px solid #ccc' : '') .'"><p>Additional information: ' . $health['value']['question_additional_' . $index][$idx] . '<br/>';
                    } else if ($index == 15) {
                        $body .= '<div style="padding-left: 15px; '. ($idx ? 'padding-top: 10px; margin-top: 10px; border-top: 1px solid #ccc' : '') .'"><p>Diagnosis: ' . $health['value']['question_additional_' . $index][$idx] . '<br/>';
                        $body .= 'Treatment from: ' . $health['value']['question_medication_from_' . $index][$idx] . ' to '. $health['value']['question_medication_to_' . $index][$idx] .'<br/>';
                        $body .= 'Medication, dosage: ' . $health['value']['question_medication_' . $index][$idx] . '<br/>';
                        if ($health['value']['question_free_of_complaint_' . $index][$idx] == 'yes') {
                            $body .= 'Free of complaints? ' . $health['value']['question_free_of_complaint_' . $index][$idx] . '<br/>';
                            $body .= 'Since when ' . $health['value']['question_free_of_complaint_addition_info_' . $index][$idx] . '</p></div>';
                        } else {
                            $body .= 'Free of complaints? ' . $health['value']['question_free_of_complaint_' . $index][$idx] . '</p></div>';
                        }
                    } else {
                        $body .= '<div style="padding-left: 15px; '. ($idx ? 'padding-top: 10px; margin-top: 10px; border-top: 1px solid #ccc' : '') .'"><p>Diagnosis: ' . $health['value']['question_additional_' . $index][$idx] . '<br/>';
                        $body .= 'Treatment from: ' . $health['value']['question_treat_from_' . $index][$idx] . ' to '. $health['value']['question_treat_to_' . $index][$idx] .'<br/>';
                        $body .= 'Which treatment?: ' . $health['value']['question_treatment_' . $index][$idx] . '<br/>';
                        $body .= 'Medication, dosage: ' . $health['value']['question_medication_' . $index][$idx] . '<br/>';
                        if ($health['value']['question_free_of_complaint_' . $index][$idx] == 'yes') {
                            $body .= 'Treatment finished? ' . $health['value']['question_treatment_finished_' . $index][$idx] . '<br/>';
                            $body .= 'Since when ' . $health['value']['question_treatment_finished_addition_info_' . $index][$idx] . '<br/>';
                        } else {
                            $body .= 'Treatment finished? ' . $health['value']['question_treatment_finished_' . $index][$idx] . '<br/>';
                        }
                        if ($health['value']['question_free_of_complaint_' . $index][$idx] == 'yes') {
                            $body .= 'Free of complaints? ' . $health['value']['question_free_of_complaint_' . $index][$idx] . '<br/>';
                            $body .= 'Since when ' . $health['value']['question_free_of_complaint_addition_info_' . $index][$idx] . '</p></div>';
                        } else {
                            $body .= 'Free of complaints? ' . $health['value']['question_free_of_complaint_' . $index][$idx] . '</p></div>';
                        }
                    }
                }
                $body .= '<hr>';
            }
        }
        $body .= '<p>I am: ' . getAnswerByKey($health['value'], 'question_' . count($array_questions)) . '</p>';
    } else {
        foreach ($array_questions_2 as $index => $question) {
            $body .= '<p>' . $question . ': ' . getAnswerByKey($health['value'], 'question_' . $index) . '</p>';
            if (getAnswerByKey($health['value'], 'question_' . $index) == 'yes') {
                $datas = getAnswerByKey($health['value'], 'question_additional_' . $index);
                foreach($datas as $idx => $add) {
                    if ($index == 13) {
                        $body .= '<div style="padding-left: 15px; '. ($idx ? 'padding-top: 10px; margin-top: 10px; border-top: 1px solid #ccc' : '') .'"><p>Additional information: ' . $health['value']['question_additional_' . $index][$idx] . '<br/>';
                    } else if ($index == 15) {
                        $body .= '<div style="padding-left: 15px; '. ($idx ? 'padding-top: 10px; margin-top: 10px; border-top: 1px solid #ccc' : '') .'"><p>Diagnosis: ' . $health['value']['question_additional_' . $index][$idx] . '<br/>';
                        $body .= 'Treatment from: ' . $health['value']['question_treat_from_' . $index][$idx] . ' to '. $health['value']['question_treat_to_' . $index][$idx] .'<br/>';
                        $body .= 'Medication, dosage: ' . $health['value']['question_medication_' . $index][$idx] . '<br/>';
                        if ($health['value']['question_free_of_complaint_' . $index][$idx] == 'yes') {
                            $body .= 'Free of complaints? ' . $health['value']['question_free_of_complaint_' . $index][$idx] . '<br/>';
                            $body .= 'Since when ' . $health['value']['question_free_of_complaint_addition_info_' . $index][$idx] . '</p></div>';
                        } else {
                            $body .= 'Free of complaints? ' . $health['value']['question_free_of_complaint_' . $index][$idx] . '</p></div>';
                        }
                    } else {
                        $body .= '<div style="padding-left: 15px; '. ($idx ? 'padding-top: 10px; margin-top: 10px; border-top: 1px solid #ccc' : '') .'"><p>Diagnosis: ' . $health['value']['question_additional_' . $index][$idx] . '<br/>';
                        $body .= 'Treatment from: ' . $health['value']['question_treat_from_' . $index][$idx] . ' to '. $health['value']['question_treat_to_' . $index][$idx] .'<br/>';
                        $body .= 'Which treatment?: ' . $health['value']['question_treatment_' . $index][$idx] . '<br/>';
                        $body .= 'Medication, dosage: ' . $health['value']['question_medication_' . $index][$idx] . '<br/>';
                        if ($health['value']['question_free_of_complaint_' . $index][$idx] == 'yes') {
                            $body .= 'Treatment finished? ' . $health['value']['question_treatment_finished_' . $index][$idx] . '<br/>';
                            $body .= 'Since when ' . $health['value']['question_treatment_finished_addition_info_' . $index][$idx] . '<br/>';
                        } else {
                            $body .= 'Treatment finished? ' . $health['value']['question_treatment_finished_' . $index][$idx] . '<br/>';
                        }
                        if ($health['value']['question_free_of_complaint_' . $index][$idx] == 'yes') {
                            $body .= 'Free of complaints? ' . $health['value']['question_free_of_complaint_' . $index][$idx] . '<br/>';
                            $body .= 'Since when ' . $health['value']['question_free_of_complaint_addition_info_' . $index][$idx] . '</p></div>';
                        } else {
                            $body .= 'Free of complaints? ' . $health['value']['question_free_of_complaint_' . $index][$idx] . '</p></div>';
                        }
                    }
                }
                $body .= '<hr>';
            }
            if ($index == 18) {
                $body .= '<div style="padding-left: 15px; '. ($idx ? 'padding-top: 10px; margin-top: 10px; border-top: 1px solid #ccc' : '') .'"><p>' . $health['value']['question_number_teeth_' . $index][0] . '</p></div>';
            }
        }
    }
    sendEmail(_MAIL_USER, $subject, $body, getValueByKey($data, 'firstName') . ' ' . getValueByKey($data, 'lastName'));
    $arrCart = array();
    if (count($_SESSION['cart_health'])) {
        foreach ($_SESSION['cart_health'] as $key => $cart) {
            $arrCart[] = $cart['id'];
        }
    }
    unset($_SESSION['cart_health']);
    echo json_encode(array('listId' => $arrCart));
}