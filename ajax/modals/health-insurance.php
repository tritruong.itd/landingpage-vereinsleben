<?php
include_once '../../libraries/functions.php';
if(!isset($_SESSION))  {
    session_start();
}
$cart = getCartById('health-insurance');
$cart_tmp = getCartDataById('health-insurance');
if ($cart) {
    $privateHealthInsurance = isset($cart['value']) ? isset($cart['value']['private-health-insurance']) ? $cart['value']['private-health-insurance'] : 'no' : '';
    $currentHealthInsurancePrivate = isset($cart['value']) ? $cart['value']['current-health-insurance-private'] : '';
    $occupationGroupPrivate = isset($cart['value']) ? $cart['value']['occupation-group-private'] : '';
    $iHave = isset($cart['value']) ? $cart['value']['i-have'] : '';
    $hospitalAccommodation = isset($cart['value']) ? $cart['value']['hospital-accommodation'] : '';
    $dailyHospitalAllowance = isset($cart['value']) ? $cart['value']['daily-hospital-allowance'] : '';
    $dentalConverage = isset($cart['value']) ? $cart['value']['dental-converage'] : '';
    $sicknessPayPerMonth = isset($cart['value']) ? $cart['value']['sickness-pay-per-month'] : '';
    $sicknessPayPerMonthAdd = isset($cart['value']) ? $cart['value']['sickness-pay-per-month-add'] : '';
    $deductiblePerYear = isset($cart['value']) ? $cart['value']['deductible-per-year'] : '';
    $occupationGroupPublic = isset($cart['value']) ? $cart['value']['occupation-group-public'] : '';
    $grossIncomePerMonth = isset($cart['value']) ? $cart['value']['gross-income-per-month'] : '';
    $currentHealthInsurancePublic = isset($cart['value']) ? $cart['value']['current-health-insurance-public'] : '';
    $iHaveChildren = isset($cart['value']) ? $cart['value']['i-have-children'] : '';
    $supplementaryHealthInsurance = isset($cart['value']) ? isset($cart['value']['supplementary-health-insurance']) ? $cart['value']['supplementary-health-insurance'] : '' : '';
    $publicHealthInsurance = isset($cart['value']) ? isset($cart['value']['public-health-insurance']) ? $cart['value']['public-health-insurance'] : '' : '';
    $supplementaryHospitalAccommodation = isset($cart['value']) ? $cart['value']['supplementary-hospital-accommodation'] ? $cart['value']['supplementary-hospital-accommodation'] : 'no' : '';
    $supplementaryHospitalAccommodationValue = isset($cart['value']) ? $cart['value']['supplementary-hospital-accommodation-value'] : '';
    $supplementarydentalCoverage = isset($cart['value']) ? $cart['value']['supplementary-dental-coverage'] ? $cart['value']['supplementary-dental-coverage'] : 'no' : '';
    $dentalCoverage = isset($cart['value']) ? $cart['value']['dental-coverage'] : '';
    $financialAid = isset($cart['value']) ? isset($cart['value']['financial-aid']) ? $cart['value']['financial-aid'] : '' : '';
    $dailySickness = isset($cart['value']) ? isset($cart['value']['daily-sickness']) ? $cart['value']['daily-sickness'] : '' : '';
    $dailyHospital = isset($cart['value']) ? isset($cart['value']['daily-hospital']) ? $cart['value']['daily-hospital'] : '' : '';
    $dailyHospitalCost = isset($cart['value']) ? $cart['value']['daily-hospital-cost'] : '';
    $dailySicknessCost = isset($cart['value']) ? $cart['value']['daily-sickness-cost'] : '';
    $additionalInformation = isset($cart['value']) ? $cart['value']['additional-information-health'] : '';
    $outpatientCoverage = isset($cart['value']) ? isset($cart['value']['outpatient-coverage']) ? explode(' - ', $cart['value']['outpatient-coverage']) : array() : array();
} else if ($cart_tmp) {
    $privateHealthInsurance = isset($cart_tmp['value']) ? isset($cart_tmp['value']['private-health-insurance']) ? $cart_tmp['value']['private-health-insurance'] : 'no' : '';
    $currentHealthInsurancePrivate = isset($cart_tmp['value']) ? $cart_tmp['value']['current-health-insurance-private'] : '';
    $occupationGroupPrivate = isset($cart_tmp['value']) ? $cart_tmp['value']['occupation-group-private'] : '';
    $iHave = isset($cart_tmp['value']) ? $cart_tmp['value']['i-have'] : '';
    $hospitalAccommodation = isset($cart_tmp['value']) ? $cart_tmp['value']['hospital-accommodation'] : '';
    $dailyHospitalAllowance = isset($cart_tmp['value']) ? $cart_tmp['value']['daily-hospital-allowance'] : '';
    $dentalConverage = isset($cart_tmp['value']) ? $cart_tmp['value']['dental-converage'] : '';
    $sicknessPayPerMonth = isset($cart_tmp['value']) ? $cart_tmp['value']['sickness-pay-per-month'] : '';
    $sicknessPayPerMonthAdd = isset($cart_tmp['value']) ? $cart_tmp['value']['sickness-pay-per-month-add'] : '';
    $deductiblePerYear = isset($cart_tmp['value']) ? $cart_tmp['value']['deductible-per-year'] : '';
    $occupationGroupPublic = isset($cart_tmp['value']) ? $cart_tmp['value']['occupation-group-public'] : '';
    $grossIncomePerMonth = isset($cart_tmp['value']) ? $cart_tmp['value']['gross-income-per-month'] : '';
    $currentHealthInsurancePublic = isset($cart_tmp['value']) ? $cart_tmp['value']['current-health-insurance-public'] : '';
    $iHaveChildren = isset($cart_tmp['value']) ? $cart_tmp['value']['i-have-children'] : '';
    $supplementaryHealthInsurance = isset($cart_tmp['value']) ? isset($cart_tmp['value']['supplementary-health-insurance']) ? $cart_tmp['value']['supplementary-health-insurance'] : '' : '';
    $publicHealthInsurance = isset($cart_tmp['value']) ? isset($cart_tmp['value']['public-health-insurance']) ? $cart_tmp['value']['public-health-insurance'] : '' : '';
    $supplementaryHospitalAccommodation = isset($cart_tmp['value']) ? $cart_tmp['value']['supplementary-hospital-accommodation'] ? $cart_tmp['value']['supplementary-hospital-accommodation'] : 'no' : '';
    $supplementaryHospitalAccommodationValue = isset($cart_tmp['value']) ? $cart_tmp['value']['supplementary-hospital-accommodation-value'] : '';
    $supplementarydentalCoverage = isset($cart_tmp['value']) ? $cart_tmp['value']['supplementary-dental-coverage'] ? $cart_tmp['value']['supplementary-dental-coverage'] : 'no' : '';
    $dentalCoverage = isset($cart_tmp['value']) ? $cart_tmp['value']['dental-coverage'] : '';
    $financialAid = isset($cart_tmp['value']) ? isset($cart_tmp['value']['financial-aid']) ? $cart_tmp['value']['financial-aid'] : '' : '';
    $dailySickness = isset($cart_tmp['value']) ? isset($cart_tmp['value']['daily-sickness']) ? $cart_tmp['value']['daily-sickness'] : '' : '';
    $dailyHospital = isset($cart_tmp['value']) ? isset($cart_tmp['value']['daily-hospital']) ? $cart_tmp['value']['daily-hospital'] : '' : '';
    $dailyHospitalCost = isset($cart_tmp['value']) ? $cart_tmp['value']['daily-hospital-cost'] : '';
    $dailySicknessCost = isset($cart_tmp['value']) ? $cart_tmp['value']['daily-sickness-cost'] : '';
    $additionalInformation = isset($cart_tmp['value']) ? $cart_tmp['value']['additional-information-health'] : '';
    $outpatientCoverage = isset($cart_tmp['value']) ? isset($cart_tmp['value']['outpatient-coverage']) ? explode(' - ', $cart_tmp['value']['outpatient-coverage']) : array() : array();
} else {
    $privateHealthInsurance = '';
    $currentHealthInsurancePrivate = '';
    $occupationGroupPrivate = '';
    $iHave = '';
    $hospitalAccommodation = '';
    $dailyHospitalAllowance = '';
    $dentalConverage = '';
    $sicknessPayPerMonth = '';
    $sicknessPayPerMonthAdd = '';
    $deductiblePerYear = '';
    $occupationGroupPublic = '';
    $grossIncomePerMonth = '';
    $currentHealthInsurancePublic = '';
    $iHaveChildren = '';
    $supplementaryHealthInsurance = '';
    $publicHealthInsurance = '';
    $supplementaryHospitalAccommodation = '';
    $supplementaryHospitalAccommodationValue = '';
    $supplementarydentalCoverage = '';
    $dentalCoverage = '';
    $financialAid = '';
    $dailySickness = '';
    $dailyHospital = '';
    $dailyHospitalCost = '';
    $dailySicknessCost = '';
    $additionalInformation = '';
    $outpatientCoverage = array();
}
$occupationGroupPrivateList = array(
    "Employee with gross salary > €60,750 per year",
    "Self-employed",
    "Student",
    "Not working",
    "Civil servant"
);
$iHaveList = array(
    "A limited visa",
    "A permanent residence permit",
    "German citizenship",
    "Other"
);
$hospitalAccommodationList = array(
    "Shared room and ward physician",
    "Double room and chief physician",
    "Single room and chief physician"
);
$dentalConverageList = array(
    "Basic (dental treatment < 80%, dentures < 60%)",
    "Comfort (dental treatment > 80%, dentures > 60%)",
    "Premium (dental treatment > 90%, dentures > 75%)"
);
$sicknessPayPerMonthList = array(
    "€1500",
    "€1650",
    "€1800",
    "€1950",
    "€2100",
    "€2250",
    "€2400",
    "€2550",
    "€2700",
    "€3000",
    "€3150",
    "€3300",
    "€3450",
    "€3600",
    "€3750",
    "€3900",
    "€4050",
    "€4200",
    "€4350",
    "€4500",
    "Individual sickness pay"
);
$deductiblePerYearList = array(
    "€0 (most expensive choice)",
    "Up to €400",
    "Up to €800",
    "Up to €1500",
    "> €1500"
);
$occupationGroupPublicList = array(
    "Employee",
    "Self-employed",
    "Student",
    "Not working",
    "Official",
    "Civil servant"
);
$currentHealthInsurancePublicList = array(
    "actimonda krankenkasse",
    "AGIDA - Die Direkte der AOK Hessen (Zweitmarke der AOK Hessen)",
    "AOK - Die Gesundheitskasse für Niedersachsen",
    "AOK Baden-Württemberg",
    "AOK Bayern",
    "AOK Bremen/Bremerhaven",
    "AOK Hessen",
    "AOK Nordost",
    "AOK NORDWEST",
    "AOK PLUS",
    "AOK Rheinland-Pfalz/Saarland",
    "AOK Rheinland/Hamburg",
    "AOK Sachsen-Anhalt",
    "atlas BKK ahlmann",
    "Audi BKK",
    "BAHN-BKK",
    "BARMER",
    "BERGISCHE KRANKENKASSE",
    "Bertelsmann BKK",
    "Betriebskrankenkasse Mobil Oil",
    "Betriebskrankenkasse WMF",
    "BIG direkt gesund",
    "BKK Achenbach Buschhütten",
    "BKK advita",
    "BKK Akzo Nobel Bayern",
    "BKK Diakonie",
    "BKK DürkoppAdler",
    "BKK EUREGIO",
    "BKK exklusiv",
    "BKK Faber-Castell & Partner",
    "BKK firmus",
    "BKK Freudenberg",
    "BKK GILDEMEISTER SEIDENSTICKER",
    "BKK HENSCHEL Plus",
    "BKK Herkules",
    "BKK HMR",
    "BKK Linde",
    "BKK Melitta Plus",
    "BKK MEM",
    "BKK PFAFF",
    "BKK Pfalz",
    "BKK ProVita",
    "BKK Public",
    "BKK Scheufelen",
    "BKK Schwarzwald-Baar-Heuberg",
    "BKK Technoform",
    "BKK Textilgruppe Hof",
    "BKK TUI",
    "BKK VBU",
    "BKK VDN",
    "BKK VerbundPlus",
    "BKK Vital",
    "BKK Werra-Meissner",
    "BKK Wirtschaft & Finanzen",
    "BKK ZF & Partner",
    "BKK24",
    "Bosch BKK",
    "Brandenburgische BKK",
    "Continentale Betriebskrankenkasse",
    "DAK-Gesundheit",
    "Debeka BKK",
    "Die Schwenninger Krankenkasse",
    "energie-BKK",
    "Heimat Krankenkasse",
    "HEK - Hanseatische Krankenkasse",
    "hkk Krankenkasse",
    "IKK Brandenburg und Berlin",
    "IKK classic",
    "IKK gesund plus",
    "IKK Nord",
    "IKK Südwest",
    "KKH Kaufmännische Krankenkasse",
    "KNAPPSCHAFT",
    "Metzinger BKK",
    "mhplus Betriebskrankenkasse",
    "Novitas BKK",
    "pronova BKK",
    "R+V Betriebskrankenkasse",
    "Salus BKK",
    "SBK",
    "SECURVITA Krankenkasse",
    "SIEMAG BKK",
    "SKD BKK",
    "TBK Thüringer Betriebskrankenkasse",
    "TK - Techniker Krankenkasse",
    "VIACTIV Krankenkasse"
);
$supplementaryHospitalAccommodationList = array(
    "Single room",
    "Double room",
    "Single room and chief physician",
    "Double room and chief physician"
);
$dentalCoverageList = array(
    "Comfort (dental treatment > 80%, dentures > 60%)",
    "Premium (dental treatment > 90%, dentures > 75%)"
);
$currentHealthInsurancePrivateList = array(
    "Public health insurance",
    "Private health insurance"
);
?>
<form method="post" id="frm" novalidate>
    <div class="modal-header">
        <button type="button" class="close btn-close-without-save-data" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="mb-40 text-center">
            <h2>Health Insurance</h2>
        </div>
        <div class="form-group">
            <div class="error"><span></span></div>
        </div>
        <p><strong><u>Desired offer for:</u></strong></p>
        <div class="form-group form-check">
            <input type="checkbox" class="form-check-input" id="private-health-insurance" name="private-health-insurance" <?php echo (isset($privateHealthInsurance) && $privateHealthInsurance == 'on') ? 'checked' : ''?>/>
            <label class="form-check-label label-control" for="private-health-insurance">Private health insurance</label>
        </div>
        <div class="private-box <?php echo (isset($privateHealthInsurance) && $privateHealthInsurance == 'on') ? '' : 'd-none'?>">
            <div class="form-group">
                <label for="current-health-insurance-private" class="label-control">Current health insurance:</label>
                <select name="current-health-insurance-private" id="current-health-insurance-private" class="form-control">
                    <?php foreach($currentHealthInsurancePrivateList as $item) { ?>
                        <option value="<?php echo $item?>" <?php echo $currentHealthInsurancePrivate == $item ? 'selected' : ''?>><?php echo $item?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="occupation-group-private" class="label-control">Occupation group: <i class="fa fa-question-circle icon-question" aria-hidden="true" data-toggle="tooltip" data-html="true" title="To be qualified for private health insurance, employees need a minimum gross income of >€60,750 per year.<br/>Other occupation groups may have lower salary."></i></label>
                <select name="occupation-group-private" id="occupation-group-private" class="form-control">
                    <?php foreach($occupationGroupPrivateList as $item) { ?>
                        <option value="<?php echo $item?>"  <?php echo ($occupationGroupPrivate == $item ? 'selected' : '')?>><?php echo $item?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="i-have" class="label-control">I have…</label>
                <select name="i-have" id="i-have" class="form-control">
                    <?php foreach($iHaveList as $item) { ?>
                        <option value="<?php echo $item?>" <?php echo ($iHave == $item ? 'selected' : '')?>><?php echo $item?></option>
                    <?php } ?>
                </select>
            </div>
            <p><strong><u>Your desired coverage:</u></strong></p>
            <div class="form-group">
                <label for="hospital-accommodation" class="label-control">Hospital accommodation</label>
                <select name="hospital-accommodation" id="hospital-accommodation" class="form-control">
                    <?php foreach($hospitalAccommodationList as $item) { ?>
                        <option value="<?php echo $item?>" <?php echo ($hospitalAccommodation == $item ? 'selected' : '')?>><?php echo $item?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="daily-hospital-allowance" class="label-control">Daily hospital allowance</label>
                <select name="daily-hospital-allowance" id="daily-hospital-allowance" class="form-control">
                    <?php for($i = 0; $i <= 10; $i++) { ?>
                        <option value="<?php echo $i * 10?>" <?php echo ($dailyHospitalAllowance == $i * 10 ? 'selected' : '')?>>€<?php echo $i * 10?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="dental-converage" class="label-control">Dental coverage</label>
                <select name="dental-converage" id="dental-converage" class="form-control">
                    <?php foreach($dentalConverageList as $item) { ?>
                        <option value="<?php echo $item?>" <?php echo ($dentalConverage == $item ? 'selected' : '')?>><?php echo $item?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="sickness-pay-per-month" class="label-control">Sickness pay per month:</label>
                <select name="sickness-pay-per-month" id="sickness-pay-per-month" class="form-control">
                    <?php foreach($sicknessPayPerMonthList as $item) { ?>
                        <option value="<?php echo $item?>" <?php echo ($sicknessPayPerMonth == $item ? 'selected' : '')?>><?php echo $item?></option>
                    <?php } ?>
                </select>
                <div class="form-group mt-10 <?php echo ($sicknessPayPerMonth == 'Individual sickness pay' ? '' : 'd-none') ?>" id="sickness-pay">
                    <label for="sickness-pay-per-month" class="label-control">Please specify under additional information:</label>
                    <input type="text" class="form-control" id="sickness-pay-per-month" name="sickness-pay-per-month-add" value="<?php echo isset($sicknessPayPerMonthAdd) ? $sicknessPayPerMonthAdd : ''?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="deductible-per-year" class="label-control">Deductible per year: <i class="fa fa-question-circle icon-question" aria-hidden="true" data-toggle="tooltip" data-html="true" title="(deductible means up to this amount per year you pay your medical bills yourself; the insurance pays all bills which exceed the deductible)"></i></label>
                <select name="deductible-per-year" id="deductible-per-year" class="form-control">
                    <?php foreach($deductiblePerYearList as $item) { ?>
                        <option value="<?php echo $item?>" <?php echo ($deductiblePerYear == $item ? 'selected' : '')?>><?php echo $item?></option>
                    <?php } ?>
                </select>
            </div>
        </div>

        <div class="form-group form-check">
            <input type="checkbox" class="form-check-input" id="public-health-insurance" name="public-health-insurance"
                <?php echo (isset($publicHealthInsurance) && $publicHealthInsurance == 'on') ? 'checked' : ''?>/>
            <label class="form-check-label label-control" for="public-health-insurance">Public health insurance</label>
        </div>
        <div class="public-box <?php echo (isset($publicHealthInsurance) && $publicHealthInsurance == 'on') ? '' : 'd-none'?>">
            <div class="form-group">
                <label for="occupation-group-public" class="label-control">Occupation group:</label>
                <select name="occupation-group-public" id="occupation-group-public" class="form-control" required>
                    <option value="">Please select</option>
                    <?php foreach($occupationGroupPublicList as $item) { ?>
                        <option value="<?php echo $item?>" <?php echo ($occupationGroupPublic == $item ? 'selected' : '')?>><?php echo $item?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group form-inline">
                <label for="gross-income-per-month" class="label-control">Gross income per month: €</label>&nbsp;&nbsp;
                <input type="text" class="form-control" id="gross-income-per-month" name="gross-income-per-month" value="<?php echo $grossIncomePerMonth?>"/>&nbsp;&nbsp;
            </div>
            <div class="form-group">
                <label for="i-have-children" class="label-control">I have children:</label>
                <select name="i-have-children" id="i-have-children" class="form-control" required>
                    <option value="">Please select</option>
                    <option value="yes" <?php echo ($iHaveChildren == 'yes' ? 'selected' : '')?>>yes</option>
                    <option value="no" <?php echo ($iHaveChildren == 'no' ? 'selected' : '')?>>no</option>
                </select>
            </div>
            <div class="form-group">
                <label for="current-health-insurance-public" class="label-control">Current health insurance</label>
                <select name="current-health-insurance-public" id="current-health-insurance-public" class="form-control" required>
                    <option value="">Please select</option>
                    <?php foreach($currentHealthInsurancePublicList as $item) { ?>
                        <option value="<?php echo $item?>" <?php echo ($currentHealthInsurancePublic == $item ? 'selected' : '')?>><?php echo $item?></option>
                    <?php } ?>
                </select>
            </div>
        </div>

        <div class="form-group form-check">
            <input type="checkbox" class="form-check-input" id="supplementary-health-insurance" name="supplementary-health-insurance"
                <?php echo (isset($supplementaryHealthInsurance) && $supplementaryHealthInsurance == 'on') ? 'checked' : ''?>/>
            <label class="form-check-label label-control" for="supplementary-health-insurance">Supplementary health insurance</label>
        </div>

        <div class="supplementary-box <?php echo (isset($supplementaryHealthInsurance) && $supplementaryHealthInsurance == 'on') ? '' : 'd-none'?>">
            <p><strong><u>Your desired coverage:</u></strong></p>
            <div class="form-group form-check">
                <input type="checkbox" class="form-check-input" id="supplementary-hospital-accommodation" name="supplementary-hospital-accommodation"
                    <?php echo (isset($supplementaryHospitalAccommodation) && $supplementaryHospitalAccommodation == 'on') ? 'checked' : ''?>/>
                <label class="form-check-label" for="supplementary-hospital-accommodation">Hospital accommodation:</label>
            </div>
            <div class="form-group <?php echo (isset($supplementaryHospitalAccommodation) && $supplementaryHospitalAccommodation == 'on') ? '' : 'd-none'?>" id="show-supplementary-hospital-accommodation">
                <select name="supplementary-hospital-accommodation-value" id="supplementary-hospital-accommodation-value" class="form-control">
                    <?php foreach($supplementaryHospitalAccommodationList as $item) { ?>
                        <option value="<?php echo $item?>" <?php echo ($supplementaryHospitalAccommodationValue == $item ? 'selected' : '')?>><?php echo $item?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group form-check">
                <input type="checkbox" class="form-check-input" id="supplementary-dental-coverage" name="supplementary-dental-coverage"
                    <?php echo (isset($supplementarydentalCoverage) && $supplementarydentalCoverage == 'on') ? 'checked' : ''?>/>
                <label class="form-check-label" for="supplementary-dental-coverage">Dental coverage:</label>
            </div>
            <div class="form-group <?php echo (isset($supplementarydentalCoverage) && $supplementarydentalCoverage == 'on') ? '' : 'd-none'?>" id="show-supplementary-dental-coverage">
                <select name="dental-coverage" id="dental-coverage" class="form-control">
                    <?php foreach($dentalCoverageList as $item) { ?>
                        <option value="<?php echo $item?>" <?php echo ($dentalCoverage == $item ? 'selected' : '')?>><?php echo $item?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="cover">Outpatient coverage</label>
                <div class="form-check frm-multiple-checkbox <?php echo (isset($supplementaryHealthInsurance) && $supplementaryHealthInsurance == 'on') ? '' : 'd-none'?>">
                    <input class="form-check-input" name="outpatient-coverage[]" type="checkbox" value="spectacles/contact lenses" id="civil-law" <?php echo ($outpatientCoverage && in_array('spectacles/contact lenses', $outpatientCoverage) ? 'checked' : '')?>>
                    <label class="form-check-label" for="civil-law">Spectacles/contact lenses</label>
                </div>
                <div class="form-check frm-multiple-checkbox <?php echo (isset($supplementaryHealthInsurance) && $supplementaryHealthInsurance == 'on') ? '' : 'd-none'?>">
                    <input class="form-check-input" name="outpatient-coverage[]" type="checkbox" value="alternative practitioner" id="labour-law" <?php echo ($outpatientCoverage && in_array('alternative practitioner', $outpatientCoverage) ? 'checked' : '')?>>
                    <label class="form-check-label" for="labour-law">Alternative practitioner</label>
                </div>
                <div class="form-check frm-multiple-checkbox <?php echo (isset($supplementaryHealthInsurance) && $supplementaryHealthInsurance == 'on') ? '' : 'd-none'?>">
                    <input class="form-check-input" name="outpatient-coverage[]" type="checkbox" value="coverage of co-payments from public health insurance (i.e. for prescribed medicines)" id="traffic-law" <?php echo ($outpatientCoverage && in_array('coverage of co-payments from public health insurance (i.e. for prescribed medicines)', $outpatientCoverage) ? 'checked' : '')?>>
                    <label class="form-check-label" for="traffic-law">Coverage of co-payments from public health insurance (i.e. for prescribed medicines)</label>
                </div>
                <p class="text-danger d-none"><small>Please select at least 1 case</small></p>
            </div>
            <div class="form-group form-check">
                <input type="checkbox" class="form-check-input" id="financial-aid" name="financial-aid" <?php echo $financialAid == 'on' ? 'checked' : ''?>/>
                <label class="form-check-label" for="financial-aid">Financial aid</label>
                <div class="form-group form-check financial-aid  <?php echo $financialAid == 'on' ? '' : 'd-none'?>">
                    <input type="checkbox" class="form-check-input" id="daily-sickness" name="daily-sickness" <?php echo $dailySickness == 'on' ? 'checked' : ''?>/>
                    <label class="form-check-label" for="daily-sickness">daily sickness allowance (closes the gap between sickness pay of public health insurance and your usual net income)</label>
                </div>
                <div class="form-group daily-sickness-cost <?php echo $dailySickness == 'on' ? '' : 'd-none'?>">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">€</span>
                        </div>
                        <input type="text" id="daily-sickness-cost" class="daily-sickness-cost form-control" title="Daily Sickness Cost" name="daily-sickness-cost" value="<?php echo $dailySicknessCost?>"/>
                    </div>
                </div>
                <div class="form-group form-check financial-aid <?php echo $financialAid == 'on' ? '' : 'd-none'?>">
                    <input type="checkbox" class="form-check-input" id="daily-hospital" name="daily-hospital" <?php echo $dailyHospital == 'on' ? 'checked' : ''?>/>
                    <label class="form-check-label" for="daily-hospital">daily hospital allowance (pays every day you stay in hospital to cover the additional costs of your hospital stay)</label>
                </div>
                <div class="form-group daily-hospital-cost <?php echo $dailyHospital == 'on' ? '' : 'd-none'?>">
                    <select name="daily-hospital-cost" id="daily-hospital-cost" class="form-control" title="Daily Hospital Cost">
                        <?php for($i = 0; $i <= 10; $i++) { ?>
                            <option value="<?php echo $i * 10?>" <?php echo $dailyHospitalCost == ($i * 10) ? 'selected' : ''?>>€<?php echo $i * 10?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="form-check-label label-control" for="additional-information-health">Additional information:</label>
                <textarea name="additional-information-health" id="additional-information-health" cols="30" rows="5" class="form-control"><?php echo trim($additionalInformation)?></textarea>
            </div>
        </div>
    </div>
    <div class="modal-footer clearfix">
        <input type="hidden" name="id_box">
        <button type="submit" class="btn btn-secondary pull-left step1 btn-close">Additional Offers</button>
        <button type="submit" class="btn btn-primary pull-right btn-next step1">Next</button>
    </div>
</form>

<script>
  $().ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('#daily-hospital').change(function () {
      if ($(this).is(':checked')) {
        $('.daily-hospital-cost').removeClass('d-none');
      } else {
        $('.daily-hospital-cost').addClass('d-none');
      }
    });
    $('#daily-sickness').change(function () {
      if ($(this).is(':checked')) {
        $('.daily-sickness-cost').removeClass('d-none');
      } else {
        $('.daily-sickness-cost').addClass('d-none');
      }
    });
    $('#financial-aid').change(function () {
      if ($(this).is(':checked')) {
        $('.financial-aid').removeClass('d-none');
      } else {
        $('.daily-hospital-cost').addClass('d-none');
        $('.financial-aid').addClass('d-none');
      }
    });
    $('#private-health-insurance').change(function () {
      if ($(this).is(':checked')) {
        $('.private-box').removeClass('d-none');
      } else {
        $('.private-box').addClass('d-none');
      }
    });
    $('#public-health-insurance').change(function () {
      if ($(this).is(':checked')) {
        $('.public-box').removeClass('d-none');
        $('#gross-income-per-month').attr('required', true);
      } else {
        $('.public-box').addClass('d-none');
        $('#gross-income-per-month').removeAttr('required');
      }
    });
    $('#supplementary-health-insurance').change(function () {
      if ($(this).is(':checked')) {
        $('.supplementary-box, .frm-multiple-checkbox').removeClass('d-none');
      } else {
        $('.supplementary-box, .frm-multiple-checkbox').addClass('d-none');
      }
    });
    $('#sickness-pay-per-month').change(function () {
      var sickness = $(this).val();
      if (sickness === 'Individual sickness pay') {
        $('#sickness-pay').removeClass('d-none');
      } else {
        $('#sickness-pay').addClass('d-none');
      }
    });
    $('#supplementary-hospital-accommodation').change(function () {
      if ($(this).is(':checked')) {
        $('#show-supplementary-hospital-accommodation').removeClass('d-none');
      } else {
        $('#show-supplementary-hospital-accommodation').addClass('d-none');
      }
    });
    $('#supplementary-dental-coverage').change(function () {
      if ($(this).is(':checked')) {
        $('#show-supplementary-dental-coverage').removeClass('d-none');
      } else {
        $('#show-supplementary-dental-coverage').addClass('d-none');
      }
    })
  })
</script>
