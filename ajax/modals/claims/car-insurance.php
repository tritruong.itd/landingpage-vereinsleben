<div class="modal-header">
    <button type="button" class="close btn-close-without-save-data" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="text-center">
        <h2>Car Insurance</h2>
    </div>
<p>In the event of a claim related to your car insurance please contact directly the claims
    hotline of your insurance provider. There you will be guided with the next steps.
    You find the claims hotline (in German: Schadenhotline) in your documents of the car
    insurance or you can search it by entering in Google “Schadenhotline *name of your
    car insurance*”</p>
</div>
<div class="modal-footer clearfix">
    <input type="hidden" name="id_box" value=""/>
    <button type="button" class="btn btn-primary btn-close-without-save-data">Close</button>
</div>