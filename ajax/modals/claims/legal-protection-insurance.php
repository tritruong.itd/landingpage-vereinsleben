<div class="modal-header">
    <button type="button" class="close btn-close-without-save-data" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="text-center">
        <h2>Legal Expenses Insurance</h2>
    </div>
    <p>If you need legal advice and would like to use your insurance, please do the following steps:</p>
    <p>1) <strong><u>BEFORE</u></strong> you go to a lawyer you need to check whether your legal case is covered under your insurance. Please contact directly the claims hotline of your insurance provider.</p>
    <p>2) Describe your issue to the legal insurance. There you could get a first evaluation, if you have a chance to win your legal case and if your issue is covered. If so, you get the approval regarding the legal costs.</p>
    <p>You find the claims hotline (in German: Schadenhotline) in your documents of the legal insurance or you can search it by entering in Google “Schadenhotline *name of your legal insurance*”</p>
</div>
<div class="modal-footer clearfix">
    <input type="hidden" name="id_box" value=""/>
    <button type="button" class="btn btn-primary btn-close-without-save-data">Close</button>
</div>