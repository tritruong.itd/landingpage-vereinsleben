<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="step3 text-center">
      <h1>Thank you!</h1>
      <p>We have received your request and will contact you promptly by mail.</p>
      <button type="button" class="btn btn-secondary step3" data-dismiss="modal">Close</button><br/><br/><br/>
    </div>
</div>