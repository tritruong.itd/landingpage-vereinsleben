<?php
include_once '../../libraries/functions.php';
if(!isset($_SESSION))  {
    session_start();
}
$cart = getCartById('car-insurance');
$cart_tmp = getCartDataById('car-insurance');

if ($cart) {
    $theCar = isset($cart['value']) ? $cart['value']['the-car'] : '';
    $desiredCoverage = isset($cart['value']) ? $cart['value']['desired-coverage'] : '';
} else if ($cart_tmp) {
    $theCar = isset($cart_tmp['value']) ? $cart_tmp['value']['the-car'] : '';
    $desiredCoverage = isset($cart_tmp['value']) ? $cart_tmp['value']['desired-coverage'] : '';
} else {
    $theCar = '';
    $desiredCoverage = '';
}
$desiredCoverages = array(
    'only-liability' => 'Only liability',
    'liability-and-partial-cover' => 'Liability and partial cover',
    'liability-and-comprehensive-cover' => 'Liability and comprehensive cover'
);
?>
<form method="post" id="frm" novalidate>
    <div class="modal-header">
        <button type="button" class="close btn-close-without-save-data" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="mb-40 text-center">
            <h2>Car Insurance</h2>
        </div>
        <div class="form-group">
            <div class="error"><span></span></div>
        </div>
        <div class="form-group">
            <label for="payment" class="label-control">The car...</label>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="the-car" id="is-already-insurance" value="is already insured in my name (insurance change)" <?php echo (isset($theCar) && $theCar == 'is already insured in my name (insurance change)') ? 'checked' : "checked"; ?>/>
                <label class="form-check-label" for="is-already-insurance">Is already insured in my name (insurance change)</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="the-car" id="will-already-insurance" value="will be registered in my name now (purchase of new car or change of car holder)" <?php echo (isset($theCar) && $theCar == 'will be registered in my name now (purchase of new car or change of car holder)') ? 'checked' : ''; ?>/>
                <label class="form-check-label" for="will-already-insurance">Will be registered in my name now (purchase of new car or change of car holder)</label>
            </div>
        </div>
        <div class="form-group">
            <label for="desired-coverage" class="label-control">Desired coverage</label>
            <select name="desired-coverage" id="desired-coverage" class="form-control">
                <?php foreach($desiredCoverages as $key => $value) { ?>
                    <option value="<?php echo $key?>" <?php echo ($desiredCoverage == $key ? 'selected' : '')?>><?php echo $value?></option>
                <?php } ?>
            </select>
        </div>
        <p>Car insurances require further information. Therefore, we will get in contact with you to clear the additional details.</p>
    </div>
    <div class="modal-footer clearfix">
        <input type="hidden" name="id_box">
        <button type="submit" class="btn btn-secondary pull-left step1 btn-close">Additional Offers</button>
        <button type="submit" class="btn btn-primary pull-right btn-next step1">Next</button>
    </div>
</form>