<?php
include_once '../../libraries/functions.php';
if(!isset($_SESSION))  {
    session_start();
}
$cart = getCartById('accident-disability-insurance');
$cart_tmp = getCartDataById('accident-disability-insurance');
if ($cart) {
    $disability = isset($cart['value']) ? $cart['value']['disability'] : '';
    $frmDisability = isset($cart['value']) ? $cart['value']['frm-disability'] : '';
    $iam = isset($cart['value']) ? $cart['value']['iam'] : '';
    $frmAccident = isset($cart['value']) ? $cart['value']['frm-accident'] : '';
    $disabilityPension = isset($cart['value']) ? $cart['value']['disability-pension'] : '';
    $desiredDisabilityBenefitCheck = isset($cart['value']) ? isset($cart['value']['desired-disability-benefit-check']) : '';
    $desiredDisabilityBenefit = isset($cart['value']) ? $cart['value']['desired-disability-benefit'] : '';
    $desiredDeathBenefit = isset($cart['value']) ? $cart['value']['desired-death-benefit'] : '';
    $desiredDailyHospitalAllowance = isset($cart['value']) ? $cart['value']['desired-daily-hospital-allowance'] : '';
    $desiredMonthlyAccidentPension = isset($cart['value']) ? $cart['value']['desired-monthly-accident-pension'] : '';
} else if ($cart_tmp) {
    $iam = isset($cart_tmp['value']) ? $cart_tmp['value']['iam'] : '';
    $disability = isset($cart_tmp['value']) ? $cart_tmp['value']['disability'] : '';
    $frmDisability = isset($cart_tmp['value']) ? $cart_tmp['value']['frm-disability'] : '';
    $frmAccident = isset($cart_tmp['value']) ? $cart_tmp['value']['frm-accident'] : '';
    $disabilityPension = isset($cart_tmp['value']) ? $cart_tmp['value']['disability-pension'] : '';
    $desiredDisabilityBenefitCheck = isset($cart_tmp['value']) ? isset($cart_tmp['value']['desired-disability-benefit-check']) : '';
    $desiredDisabilityBenefit = isset($cart_tmp['value']) ? $cart_tmp['value']['desired-disability-benefit'] : '';
    $desiredDeathBenefit = isset($cart_tmp['value']) ? $cart_tmp['value']['desired-death-benefit'] : '';
    $desiredDailyHospitalAllowance = isset($cart_tmp['value']) ? $cart_tmp['value']['desired-daily-hospital-allowance'] : '';
    $desiredMonthlyAccidentPension = isset($cart_tmp['value']) ? $cart_tmp['value']['desired-monthly-accident-pension'] : '';
} else {
    $disability = '';
    $frmDisability = '';
    $frmAccident = '';
    $disabilityPension = '';
    $desiredDisabilityBenefitCheck = '';
    $desiredDisabilityBenefit = '';
    $desiredDeathBenefit = '';
    $desiredDailyHospitalAllowance = '';
    $desiredMonthlyAccidentPension = '';
}
?>
<form method="post" id="frm" novalidate>
    <div class="modal-header">
        <button type="button" class="close btn-close-without-save-data" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="mb-40 text-center">
            <h2>Accident/Disability Insurance</h2>
        </div>
        <div class="form-group">
            <div class="error"><span></span></div>
        </div>
        <div class="form-check">
            <input class="form-check-input toggle-btn" type="checkbox" name="frm-disability" id="frm-disability" <?php echo (isset($frmDisability) && $frmDisability == 'on') ? 'checked' : ''; ?>/>
            <label class="form-check-label" for="frm-disability">For Disability insurance</label>
        </div>
        <div class="frm-disability <?php echo (isset($frmDisability) && $frmDisability == 'on') ? '' : 'd-none'?>">
            <div class="form-group">
                <label for="payment" class="label-control">I am</label>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="iam" id="non-smoker" value="non-smoker" <?php echo (isset($iam) && $iam == 'non-smoker') ? 'checked' : 'checked'; ?>/>
                    <label class="form-check-label" for="non-smoker">Non-smoker</label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="iam" id="smoker" value="smoker" <?php echo (isset($iam) && $iam == 'smoker') ? 'checked' : ''; ?>/>
                    <label class="form-check-label" for="smoker">Smoker</label>
                </div>
            </div>
            <div class="form-group form-inline">
                <label for="disability" class="label-control">Desired monthly disability pension in €:</label>&nbsp;&nbsp;
                <input class="form-control" type="text" name="disability" id="disability" value="<?php echo $disability?>"/>
            </div>
            <div class="form-group">
                <label for="disability-pension" class="label-control">The disability pension should be paid until the age of:</label>
                <select name="disability-pension" id="disability-pension" class="form-control">
                    <?php for($i = 60; $i <= 67; $i++) { ?>
                        <option value="<?php echo $i?>" <?php echo $disabilityPension == $i ? 'selected' : ''?>><?php echo $i?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="form-check">
            <input class="form-check-input toggle-btn" type="checkbox" name="frm-accident" id="frm-accident" <?php echo (isset($frmAccident) && $frmAccident == 'on') ? 'checked' : ''; ?>/>
            <label class="form-check-label" for="frm-accident">For accident insurance</label>
        </div>
        <p class="toggle-btn" data-type="frm-accident"><strong><u></u></strong></p>
        <div class="frm-accident <?php echo (isset($frmAccident) && $frmAccident == 'on') ? '' : 'd-none'?>">
            <div class="form-group">
                <label for="payment" class="label-control">Desired disability benefit:</label>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="desired-disability-benefit-check" id="i-dont-know" value="I don´t know. Please suggest a disability benefit" <?php echo (isset($desiredDisabilityBenefitCheck) && $desiredDisabilityBenefitCheck == 'on' && !isset($desiredDisabilityBenefit)) ? 'checked' : ''; ?>/>
                    <label class="form-check-label" for="i-dont-know">I don´t know. Please suggest a disability benefit</label>
                </div>
                <div class="form-check form-inline form-desired">
                    €&nbsp;<input class="form-control" type="text" name="desired-disability-benefit" id="benefit-value" value="<?php echo $desiredDisabilityBenefit ?>" <?php echo (isset($desiredDisabilityBenefitCheck) && $desiredDisabilityBenefitCheck == 'on') ? 'disabled' : ''; ?>/>&nbsp;
                    <label class="form-check-label" for="benefit-value">disability benefit</label>
                </div>
            </div>
            <div class="form-group">
                <label for="desired-death-benefit" class="label-control">Desired death benefit:</label>
                <select name="desired-death-benefit" id="desired-death-benefit" class="form-control">
                    <?php for($i = 0; $i <= 15; $i++) { ?>
                        <option value="<?php echo ($i * 10000)?>" <?php echo $desiredDeathBenefit == ($i * 10000) ? 'selected' : ''?>>€<?php echo number_format($i * 10000)?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="desired-daily-hospital-allowance" class="label-control">Desired daily hospital allowance:</label>
                <select name="desired-daily-hospital-allowance" id="desired-daily-hospital-allowance" class="form-control">
                    <?php for($i = 0; $i <= 7; $i++) { ?>
                        <option value="<?php echo ($i * 10)?>" <?php echo $desiredDailyHospitalAllowance == ($i * 10) ? 'selected' : ''?>>€<?php echo number_format($i * 10)?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="desired-monthly-accident-pension" class="label-control">Desired monthly accident pension:</label>
                <select name="desired-monthly-accident-pension" id="desired-monthly-accident-pension" class="form-control">
                    <?php for($i = 0; $i < 5; $i++) { ?>
                        <option value="<?php echo ($i * 500)?>" <?php echo $desiredMonthlyAccidentPension == ($i * 500) ? 'selected' : ''?>>€<?php echo number_format($i * 500)?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
    <div class="modal-footer clearfix">
        <input type="hidden" name="id_box">
        <button type="submit" class="btn btn-secondary pull-left step1 btn-close">Additional Offers</button>
        <button type="submit" class="btn btn-primary pull-right btn-next step1">Next</button>
    </div>
</form>
<script>
    $().ready(function () {
        $('#i-dont-know').change(function() {
            if ($(this).is(':checked')) {
                $('#benefit-value').prop('disabled', true).val('');
            } else {
                $('#benefit-value').removeAttr('disabled');
            }
        });
        $('.toggle-btn').change(function() {
          var type = $(this).attr('id');
          if ($(this).hasClass('active')) {
            $('.' + type).addClass('d-none');
            $(this).removeClass('active');
            if (type === 'frm-disability') {
              $('#disability').removeAttr('required');
            }
            if (type === 'frm-accident') {
              $('#benefit-value').removeAttr('required');
            }
          } else {
            $('.' + type).removeClass('d-none');
            $(this).addClass('active');
            if (type === 'frm-disability') {
              $('#disability').attr('required', true);
            }
            if (type === 'frm-accident') {
              $('#benefit-value').attr('required', true);
            }
          }
        });
    })
</script>
<style>
    .toggle-btn {
        cursor: pointer;
    }
    .toggle-btn.active {
        color: #0069d9;
    }
</style>