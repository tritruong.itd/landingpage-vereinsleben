<?php
include_once '../../libraries/functions.php';
if(!isset($_SESSION))  {
    session_start();
}
$cart = getCartById('travel-insurance');
$cart_tmp = getCartDataById('travel-insurance');

if ($cart) {
    if (isset($cart['value']) && isset($cart['value']['travel-health-insurance'])) {
        $travelHealthInsurance = isset($cart['value']) ? explode(' - ', $cart['value']['travel-health-insurance']) : array();
    } else {
        $travelHealthInsurance = array();
    }
    $yourAge = isset($cart['value']) ? $cart['value']['your-age'] : '';
    $tripFor = isset($cart['value']) ? $cart['value']['trip-for'] : '';
    $valueOfTheTrip = isset($cart['value']) ? $cart['value']['value-of-the-trip'] : '';
    $travelPeriodFrom = isset($cart['value']) ? strtotime($cart['value']['travel-period-from']) : '';
    $travelPeriodTo = isset($cart['value']) ? strtotime($cart['value']['travel-period-to']) : '';
    $bookingDate = isset($cart['value']) ? strtotime($cart['value']['booking-date']) : '';
    $directionTravel = isset($cart['value']) ? $cart['value']['direction-travel'] : '';
    $travelDestination = isset($cart['value']) ? $cart['value']['travel-destination'] : '';
    $reasionOfTravel = isset($cart['value']) ? $cart['value']['reasion-of-travel'] : '';
} else if ($cart_tmp) {
    if (isset($cart_tmp['value']) && isset($cart_tmp['value']['travel-health-insurance'])) {
        $travelHealthInsurance = isset($cart_tmp['value']) ? explode(' - ', $cart_tmp['value']['travel-health-insurance']) : array();
    } else {
        $travelHealthInsurance = array();
    }
    $yourAge = isset($cart_tmp['value']) ? $cart_tmp['value']['your-age'] : '';
    $tripFor = isset($cart_tmp['value']) ? $cart_tmp['value']['trip-for'] : '';
    $valueOfTheTrip = isset($cart_tmp['value']) ? $cart_tmp['value']['value-of-the-trip'] : '';
    $travelPeriodFrom = isset($cart_tmp['value']) ? strtotime($cart_tmp['value']['travel-period-from']) : '';
    $travelPeriodTo = isset($cart_tmp['value']) ? strtotime($cart_tmp['value']['travel-period-to']) : '';
    $bookingDate = isset($cart_tmp['value']) ? strtotime($cart_tmp['value']['booking-date']) : '';
    $directionTravel = isset($cart_tmp['value']) ? $cart_tmp['value']['direction-travel'] : '';
    $travelDestination = isset($cart_tmp['value']) ? $cart_tmp['value']['travel-destination'] : '';
    $reasionOfTravel = isset($cart_tmp['value']) ? $cart_tmp['value']['reasion-of-travel'] : '';
} else {
    $travelHealthInsurance = array();
    $yourAge = '';
    $tripFor = '';
    $valueOfTheTrip = '';
    $travelPeriodFrom = '';
    $travelPeriodTo = '';
    $bookingDate = '';
    $directionTravel = '';
    $travelDestination = '';
    $reasionOfTravel = '';
}
$reasionOfTravels = array(
    'private' => 'Private',
    'business' => 'Business',
    'private-business' => 'Private & Business',
    'study' => 'Study',
    'au-pair' => 'Au-pair',
    'work-travel' => 'Work & travel',
);
$travelDestinations = array(
    'germany' => 'Germany',
    'europe' => 'Europe',
    'including-usa-canada' => 'Worldwide including USA & Canada',
    'without-usa-canada' => 'Worldwide without USA & Canada'
);
$directionTravels = array(
    'from-germany-abroad' => 'From Germany abroad',
    'inside-germany' => 'Inside Germany',
    'from-abroad-to-germany' => 'From abroad to Germany'
);
$tripFors = array(
    'single-person' => 'Single person',
    'couple' => 'Couple',
    'family' => 'Family'
);
?>
<form method="post" id="frm" novalidate>
    <div class="modal-header">
        <button type="button" class="close btn-close-without-save-data" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="mb-40 text-center">
            <h2>Travel Insurances</h2>
        </div>
        <div class="form-group">
            <div class="error"><span></span></div>
        </div>
        <div class="form-group">
            <label for="cover" class="label-control">I want to insure</label>
            <div class="form-check frm-multiple-checkbox">
                <input class="form-check-input" name="travel-health-insurance[]" type="checkbox" value="travel health insurance" id="travel-health-insurance" <?php echo ($travelHealthInsurance && in_array('travel health insurance', $travelHealthInsurance) ? 'checked' : '')?>/>
                <label class="form-check-label" for="travel-health-insurance">Travel health insurance</label>
            </div>
            <div class="form-check frm-multiple-checkbox">
                <input class="form-check-input" name="travel-health-insurance[]" type="checkbox" value="travel cancellation insurance" id="travel-cancellation-insurance" <?php echo ($travelHealthInsurance && in_array('travel cancellation insurance', $travelHealthInsurance) ? 'checked' : '')?>/>
                <label class="form-check-label" for="travel-cancellation-insurance">Travel cancellation insurance</label>
            </div>
            <div class="form-check frm-multiple-checkbox">
                <input class="form-check-input" name="travel-health-insurance[]" type="checkbox" value="travel interruption insurance" id="travel-interruption-insurance" <?php echo ($travelHealthInsurance && in_array('travel interruption insurance', $travelHealthInsurance) ? 'checked' : '')?>/>
                <label class="form-check-label" for="travel-interruption-insurance">Travel interruption insurance</label>
            </div>
            <div class="form-check frm-multiple-checkbox">
                <input class="form-check-input" name="travel-health-insurance[]" type="checkbox" value="luggage insurance" id="luggage-insurance" <?php echo ($travelHealthInsurance && in_array('luggage insurance', $travelHealthInsurance) ? 'checked' : '')?>/>
                <label class="form-check-label" for="luggage-insurance">Luggage insurance</label>
            </div>
            <p class="text-danger d-none"><small>Please select at least 1 case</small></p>
        </div>
        <div class="form-group form-inline">
            <label for="your-age" class="label-control">Your age (eldest person of travelling group):</label>&nbsp;&nbsp;
            <input type="text" class="form-control your-age" name="your-age" id="your-age" value="<?php echo $yourAge?>" required/>
        </div>
        <div class="form-group form-inline">
            <label for="trip-for" class="label-control">Trip is for</label>&nbsp;&nbsp;
            <select name="trip-for" id="trip-for" class="form-control" required>
                <option value="">Please select</option>
                <?php foreach($tripFors as $key => $trip) {?>
                    <option value="<?php echo $key?>" <?php echo ($tripFor == $key) ? 'selected' : ''?>><?php echo $trip?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group form-inline">
            <label for="value-of-the-trip" class="label-control">Value of the trip in €:</label>&nbsp;&nbsp;
            <input type="text" class="form-control value-of-the-trip" name="value-of-the-trip" id="value-of-the-trip" value="<?php echo $valueOfTheTrip?>" required/>
        </div>
        <div class="form-group form-inline">
            <label for="travel-period-from" class="label-control">Travel period from:</label>&nbsp;&nbsp;
            <input type="text" autocomplete="false" class="form-control" name="travel-period-from" id="travel-period-from" required value="<?php echo (isset($travelPeriodFrom) && $travelPeriodFrom != '') ? date('m/d/Y', $travelPeriodFrom) : ''?>"/>
        </div>
        <div class="form-group form-inline">
            <label for="travel-period-to" class="label-control">To:</label>&nbsp;&nbsp;
            <input type="text" autocomplete="false" class="form-control" name="travel-period-to" id="travel-period-to" required value="<?php echo (isset($travelPeriodTo) && $travelPeriodTo != '') ? date('m/d/Y', $travelPeriodTo) : ''?>"/>
        </div>
        <div class="form-group form-inline">
            <label for="booking-date" class="label-control">Booking date of the travel:</label>&nbsp;&nbsp;
            <input type="text" autocomplete="false" class="form-control datepicker" name="booking-date" id="booking-date" required value="<?php echo (isset($bookingDate) && $bookingDate != '') ? date('m/d/Y', $bookingDate) : ''?>"/>
        </div>
        <div class="form-group">
            <label for="direction-travel" class="label-control">Direction of the travel</label>
            <select name="direction-travel" id="direction-travel" class="form-control" required>
                <option value="">Please select</option>
                <?php foreach($directionTravels as $key => $direction) {?>
                    <option value="<?php echo $key?>" <?php echo ($directionTravel == $key) ? 'selected' : ''?>><?php echo $direction?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label for="travel-destination" class="label-control">Travel destination:</label>
            <select name="travel-destination" id="travel-destination" class="form-control" required>
                <option value="">Please select</option>
                <?php foreach($travelDestinations as $key => $destination) {?>
                    <option value="<?php echo $key?>" <?php echo ($travelDestination == $key) ? 'selected' : ''?>><?php echo $destination?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label for="reasion-of-travel" class="label-control">Reason of travel:</label>
            <select name="reasion-of-travel" id="reasion-of-travel" class="form-control" required>
                <option value="">Please select</option>
                <?php foreach($reasionOfTravels as $key => $reasion) {?>
                    <option value="<?php echo $key?>" <?php echo ($reasionOfTravel == $key) ? 'selected' : ''?>><?php echo $reasion?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="modal-footer clearfix">
        <input type="hidden" name="id_box">
        <button type="submit" class="btn btn-secondary pull-left step1 btn-close">Additional Offers</button>
        <button type="submit" class="btn btn-primary pull-right btn-next step1">Next</button>
    </div>
</form>

<script>
  $( function() {
    $( function() {
      var dateFormat = "mm/dd/yy",
        from = $( "#travel-period-from" )
          .datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 1
          })
          .on( "change", function() {
            to.datepicker( "option", "minDate", getDate( this ) );
          }),
        to = $( "#travel-period-to" ).datepicker({
          defaultDate: "+1w",
          changeMonth: true,
          numberOfMonths: 1
        })
          .on( "change", function() {
            from.datepicker( "option", "maxDate", getDate( this ) );
          });

      function getDate( element ) {
        var date;
        try {
          date = $.datepicker.parseDate( dateFormat, element.value );
        } catch( error ) {
          date = null;
        }

        return date;
      }
    } );
    $(".datepicker").datepicker();
  } );
</script>