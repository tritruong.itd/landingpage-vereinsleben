<?php
include_once '../../libraries/functions.php';
if(!isset($_SESSION))  {
    session_start();
}
$cart = getCartById('legal-protection-insurance');
$cart_tmp = getCartDataById('legal-protection-insurance');

if ($cart) {
    if (isset($cart['value']) && isset($cart['value']['cover-law'])) {
        $coverLaw = isset($cart['value']) ? explode(' - ', $cart['value']['cover-law']) : array();
    } else {
        $coverLaw = array();
    }
    $payment = isset($cart['value']) ? $cart['value']['payment'] : '';
    $legal_expenses_insurance = isset($cart['value']) ? $cart['value']['legal-expenses-insurance'] : '';
    $claim = isset($cart['value']) ? $cart['value']['claim'] : '';
} else if ($cart_tmp) {
    if (isset($cart_tmp['value']) && isset($cart_tmp['value']['cover-law'])) {
        $coverLaw = isset($cart_tmp['value']) ? explode(' - ', $cart_tmp['value']['cover-law']) : array();
    } else {
        $coverLaw = array();
    }
    $payment = isset($cart_tmp['value']) ? $cart_tmp['value']['payment'] : '';
    $legal_expenses_insurance = isset($cart_tmp['value']) ? $cart_tmp['value']['legal-expenses-insurance'] : '';
    $claim = isset($cart_tmp['value']) ? $cart_tmp['value']['claim'] : '';
} else {
    $coverLaw = array();
    $payment = '';
    $legal_expenses_insurance = '';
    $claim = '';
}
$payments = array(
    'yearly' => 'Yearly',
    'half-yearly' => 'Half-yearly',
    'quaterly' => 'Quarterly',
    'monthly' => 'Monthly',
);
$yesno = array(
    "yes" => "Yes",
    "no" => "No"
);
?>
<form method="post" id="frm" novalidate>
    <div class="modal-header">
        <button type="button" class="close btn-close-without-save-data" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="mb-40 text-center">
            <h2>Legal Expenses Insurance</h2>
        </div>
        <div class="form-group">
            <div class="error"><span></span></div>
        </div>
        <div class="form-group check-checkbox">
            <label for="cover" class="label-control">I want to cover</label>
            <div class="form-check frm-multiple-checkbox">
                <input class="form-check-input" name="cover-law[]" type="checkbox" value="Civil law" id="civil-law" <?php echo ($coverLaw && in_array('Civil law', $coverLaw) ? 'checked' : '')?>/>
                <label class="form-check-label" for="civil-law">Civil law</label>
            </div>
            <div class="form-check frm-multiple-checkbox">
                <input class="form-check-input" name="cover-law[]" type="checkbox" value="Labour law (only available together with civil law)" id="labour-law" <?php echo ($coverLaw && in_array('Labour law (only available together with civil law)', $coverLaw) ? 'checked' : '')?>/>
                <label class="form-check-label" for="labour-law">Labour law (only available together with civil law)</label>
            </div>
            <div class="form-check frm-multiple-checkbox">
                <input class="form-check-input" name="cover-law[]" type="checkbox" value="Traffic law" id="traffic-law" <?php echo ($coverLaw && in_array('Traffic law', $coverLaw) ? 'checked' : '')?>/>
                <label class="form-check-label" for="traffic-law">Traffic law</label>
            </div>
            <div class="form-check frm-multiple-checkbox">
                <input class="form-check-input" name="cover-law[]" type="checkbox" value="Tenancy law" id="tenancy-law" <?php echo ($coverLaw && in_array('Tenancy law', $coverLaw) ? 'checked' : '')?>/>
                <label class="form-check-label" for="tenancy-law">Tenancy law</label>
            </div>
            <div class="form-check frm-multiple-checkbox">
                <input class="form-check-input" name="cover-law[]" type="checkbox"
                       value="Special criminal law (to defend yourself against the charge of a criminal offense)" id="special-criminal-law"
                    <?php echo ($coverLaw && in_array('Special criminal law (to defend yourself against the charge of a criminal offense)', $coverLaw) ? 'checked' : '')?>/>
                <label class="form-check-label" for="special-criminal-law">Special criminal law (to defend yourself against the charge of a criminal offense)</label>
            </div>
            <p class="text-danger d-none"><small>Please select at least 1 case</small></p>
        </div>
        <div class="form-group">
            <label for="payment" class="label-control">Payment</label>
            <select name="payment" id="payment" class="form-control" required>
                <option value="">Please select</option>
                <?php foreach($payments as $key => $pay) { ?>
                    <option value="<?php echo $key?>" <?php echo ($payment == $key) ? 'selected' : ''?>><?php echo $pay?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label for="legal-expenses-insurance" class="label-control">Prior legal expenses insurance?</label>
            <select name="legal-expenses-insurance" id="legal-expenses-insurance" class="form-control" required>
                <option value="">Please select</option>
                <?php foreach($yesno as $key => $value) { ?>
                    <option value="<?php echo $key?>" <?php echo ($legal_expenses_insurance == $key ? 'selected' : '')?>><?php echo $value?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group form-inline">
            <label for="claim" class="label-control">Number of claims in the past 5 years:</label>&nbsp;&nbsp;
            <input class="form-control form-claim" type="text" name="claim" id="claim" value="<?php echo $claim?>" required/>&nbsp;&nbsp;
            <label for="payment">claims</label>
        </div>
    </div>
    <div class="modal-footer clearfix">
        <input type="hidden" name="id_box">
        <button type="submit" class="btn btn-secondary pull-left step1 btn-close">Additional Offers</button>
        <button type="submit" class="btn btn-primary pull-right btn-next step1">Next</button>
    </div>
</form>