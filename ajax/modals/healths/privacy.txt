PRIVACY POLICY
I hereby agree that my personal data (in particular that relating to my financial circumstances and my state of health) 
which I disclose to the distributor / broker in the context of data collection, consultancy documentation, conclusion of 
the contract or the management of my contracts, are included in this data collection, and especially electronically stored 
and processed. I further agree that the companies with which I establish contractual relationships through the brokerage / 
brokerage agency, provide all data relating to the progress and development of these broker pool agreements and associated 
companies to the distributor / broker and any legal successors and that this is stored and processed by the respective 
data-receiving agency. The storage and processing should serve the preparation of offers for the conclusion of insurance, 
capital investment and loan agreements and their subsequent support. In this context, I further agree that my data may be 
transmitted to the following third parties electronically, by fax, email and / or by post and may be stored or further 
processed by the latter:                

- Commercial agents associated with the distributor / broker       
- Specialist brokers, brokerage pools, related companies and settlement platforms       
- Insurance companies and their authorized representatives      
- Social insurance institutions       
- Lawyers, accountants and auditors.     

The prerequisite for the transmission of my data to a recipient of the above category and its storage or further processing 
is that this serves either the conclusion of the contract, the achievement of improved conditions, quality control / improvement, 
the assessment of the application, the request for necessary additional advice or the receipt of required information. My consent 
is voluntary and revocable. However, I have been advised that a refusal of consent or revocation means that the distributor / 
broker cannot provide the brokerage and advisory services it offers and that follow-up of existing contracts is no longer guaranteed. 
In the event of a cancellation, the distributor / broker will limit the data storage and / or data transmission to the extent 
necessary to fulfill its legal obligations. Consent is under the express condition that the distributor / broker meets its obligation 
to protect my data against unauthorized access by third parties and not to disclose it to third parties that are not related to the 
above-mentioned purposes of data processing.