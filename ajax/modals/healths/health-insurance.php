<?php
$array_questions = array(
    '1)... of the heart, circulatory system, vessels (e.g., pain in the cardiac region, heart attack, heart failure, dysrhythmia, physician-determined high blood pressure, stroke, vascular changes, circulatory disorders, thrombosis)?',
    '2)... the lungs, bronchi, trachea, nose (e.g., inflammation, asthma, chronic bronchitis, sleep apnea)',
    '3)... the esophagus, stomach, intestine, liver, pancreas, bile, spleen (e.g., inflammation, ulcer, bleeding, high liver  values)?',
    '4)... the kidneys, urinary tract, reproductive organs, breast (e.g., inflammation, stone formation, cystic kidney, blood or protein   secretion)?',
    '5)... the metabolism, blood, the thyroid (e.g., diabetes, increased lipid levels, gout, anemia, leukemia, lymph nodes)?',
    '6) ... of the brain, nervous system, psyche (e.g., multiple sclerosis, epilepsy, migraine, paralysis, fatigue syndrome, chronic pain, dizziness, anxiety disorder, depression, attention deficit syndrome, eating disorder, psychosomatic disorder, addiction, suicide   attempt)?',
    '7) ... the bones, joints, muscles, tendons or ligaments, as well as the spine, intervertebral discs (e.g., movement restrictions, misalignments, wear, inflammation, rheumatism,   fibromyalgia)?',
    '8) ... of the eyes (e.g., visual disturbances, retinal diseases, increased eye pressure, lack of vision of more than 6 diopters)?',
    '9) ... of the ears (e.g., hearing loss, sudden deafness, ear noises, balance disorders)?',
    '10) ... the skin or allergies (for example, inflammation, eczema, atopic dermatitis, psoriasis, house dust, pollen, pet hair, food or occupational allergy)?',
    '11) ... such as infectious diseases, sexually transmitted diseases, tropical diseases (e.g., malaria, hepatitis, tuberculosis, borreliosis)?',
    '12) ... the psyche (eg, depression, anxiety disorders, attention deficit syndrome, chronic fatigue syndrome, psychosomatic disorder), addiction (e.g., taking drugs, consequences of drinking alcohol)?',
    '13) ... like benign or malignant tumors (e.g., cancer, cyst, adenoma), was there a suicide attempt or was an HIV infection detected (positive AIDS test)?',
    '14) Is or has there been an incapacity to work or severe disability in the last 5 years?',
    '15) Have you been examined, advised or treated in the past 5 years on an outpatient or inpatient basis by doctors or other practitioners (e.g.,  Alternative Practitioner, Psychotherapist)?',
    '16) Have any medicines been prescribed or taken in the last 12 months?',
    '17) Have you undergone surgery, hospitalization or stay at a health resort, or have you had an accident, injury or poisoning over the past 10 years?',
    '18) Is there currently a dental treatment, the preparation or renewal of dentures, a periodontal treatment or a jaw (tooth) regulation, or have such measures been inteded or advised?',
    '19) How many teeth are there missing that have not yet been replaced (except for milk and wisdom teeth, as well as teeth where the gaps have been closed by adjacent teeth)?'
);
?>
<form method="post" id="frm" novalidate>
    <div class="modal-header">
        <button type="button" class="close btn-close-without-save-data" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="mb-40 text-center">
            <h2>Health Insurance</h2>
        </div>
        <div class="form-group">
            <div class="row clearfix">
                <label for="height" class="col-sm-4">Height:</label>
                <div class="col-sm-8">
                    <div class="input-group">
                        <input id="height" type="number" name="height" class="form-control" aria-describedby="height-addon" required/>
                        <div class="input-group-append">
                            <span class="input-group-text" id="height-addon">cm</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row clearfix">
                <label for="weight" class="col-sm-4">Weight:</label>
                <div class="col-sm-8">
                    <div class="input-group">
                        <input id="weight" type="number" name="weight" class="form-control" aria-describedby="weight-addon" required/>
                        <div class="input-group-append">
                            <span class="input-group-text" id="weight-addon">kg</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <p>Are there or have there been diseases, complaints or dysfunctions in the last 10 years?</p>
        <?php foreach ($array_questions as $key => $question) {?>
            <div class="form-group">
                <div class="row clearfix">
                    <label class="col-sm-7"><?php echo $question?><span class="text-danger">*</span></label>
                    <div class="col-sm-5">
                        <?php if ($key != 18) { ?>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input question" type="radio" data-id="<?php echo $key?>" name="question_<?php echo $key?>" id="question_<?php echo $key?>_1" value="no" required>
                                <label class="form-check-label" for="question_<?php echo $key?>_1">no</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input question" type="radio" data-id="<?php echo $key?>" name="question_<?php echo $key?>" id="question_<?php echo $key?>_2" value="yes" required>
                                <label class="form-check-label" for="question_<?php echo $key?>_2">yes</label>
                            </div>
                        <?php } else { ?>
                            <input class="form-control" type="number" name="question_<?php echo $key?>" id="number_teeth_<?php echo $key?>"/>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php if ($key == 13) { ?>
                <div class="form-group d-none addition_<?php echo $key?>">
                    <div class="row clearfix">
                        <label for="additional_<?php echo $key?>" class="col-sm-4">Additional information</label>
                        <div class="col-sm-8">
                            <textarea name="additional_<?php echo $key?>" id="additional_<?php echo $key?>" cols="30" rows="5" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php if ($key == 15) { ?>
                <div class="d-none addition_<?php echo $key?>">
                    <div class="box-card">
                        <div class="card border-primary mb-3 card_<?php echo $key?>">
                            <div class="card-body text-primary">
                                <div class="form-group">
                                    <div class="row clearfix">
                                        <label for="additional_<?php echo $key?>" class="col-sm-4">Diagnosis</label>
                                        <div class="col-sm-8">
                                            <textarea name="additional_<?php echo $key?>[]" id="additional_<?php echo $key?>" cols="30" rows="5" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row clearfix">
                                        <label for="medication_from_<?php echo $key?>" class="col-sm-4">Medication intake from:</label>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" name="medication_from_<?php echo $key?>[]" id="medication_from_<?php echo $key?>"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row clearfix">
                                        <label for="medication_to_<?php echo $key?>" class="col-sm-4">Medication intake to:</label>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" name="medication_to_<?php echo $key?>[]" id="medication_to_<?php echo $key?>"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row clearfix">
                                        <label for="treatment_<?php echo $key?>" class="col-sm-4">Which treatment?</label>
                                        <div class="col-sm-8">
                                            <textarea name="treatment_<?php echo $key?>[]" id="treatment_<?php echo $key?>" cols="30" rows="5" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row clearfix">
                                        <label for="medication_<?php echo $key?>" class="col-sm-4">Medication, dose:</label>
                                        <div class="col-sm-8">
                                            <textarea name="medication_<?php echo $key?>[]" id="medication_<?php echo $key?>" cols="30" rows="5" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row clearfix">
                                        <label for="medication_<?php echo $key?>" class="col-sm-4">Free of complaints?</label>
                                        <div class="col-sm-8">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input free_complaint" type="radio" data-id="<?php echo $key?>" data-index="0" name="free_of_complaint_<?php echo $key?>_0[]" id="free_of_complaint_<?php echo $key?>_1_0" value="no">
                                                <label class="form-check-label" for="free_of_complaint_<?php echo $key?>_1_0">no</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input free_complaint" type="radio" data-id="<?php echo $key?>" data-index="0" name="free_of_complaint_<?php echo $key?>_0[]" id="free_of_complaint_<?php echo $key?>_2_0" value="yes">
                                                <label class="form-check-label" for="free_of_complaint_<?php echo $key?>_2_0">yes</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group free_complaint_<?php echo $key?>_0 d-none">
                                    <div class="row clearfix">
                                        <label for="free_of_complaint_addition_info_<?php echo $key?>" class="col-sm-4">Since when:</label>
                                        <div class="col-sm-8">
                                            <textarea name="free_of_complaint_addition_info_<?php echo $key?>[]" id="free_of_complaint_addition_info_<?php echo $key?>" cols="30" rows="5" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php if ($key != 15 && $key != 13 && $key != 18) { ?>
                <div class="d-none addition_<?php echo $key?>">
                    <div class="box-card">
                        <div class="card border-primary mb-3 card_<?php echo $key?>">
                            <div class="card-body text-primary">
                                <div class="form-group">
                                    <div class="row clearfix">
                                        <label for="additional_<?php echo $key?>" class="col-sm-4">Diagnosis</label>
                                        <div class="col-sm-8">
                                            <textarea name="additional_<?php echo $key?>[]" id="additional_<?php echo $key?>" cols="30" rows="5" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row clearfix">
                                        <label for="treat_from_<?php echo $key?>" class="col-sm-4">Treatment from:</label>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" name="treat_from_<?php echo $key?>[]" id="treat_from_<?php echo $key?>"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row clearfix">
                                        <label for="treat_to_<?php echo $key?>" class="col-sm-4">Treatment to:</label>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" name="treat_to_<?php echo $key?>[]" id="treat_to_<?php echo $key?>"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row clearfix">
                                        <label for="treatment_<?php echo $key?>" class="col-sm-4">Which treatment?</label>
                                        <div class="col-sm-8">
                                            <textarea name="treatment_<?php echo $key?>[]" id="treatment_<?php echo $key?>" cols="30" rows="5" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row clearfix">
                                        <label for="medication_<?php echo $key?>" class="col-sm-4">Medication, dose:</label>
                                        <div class="col-sm-8">
                                            <textarea name="medication_<?php echo $key?>[]" id="medication_<?php echo $key?>" cols="30" rows="5" class="form-control not-required"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row clearfix">
                                        <label for="treatment_finished_<?php echo $key?>" class="col-sm-4">Treatment finished?</label>
                                        <div class="col-sm-8">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input treatment_finished" type="radio" data-id="<?php echo $key?>" data-index="0" name="treatment_finished_<?php echo $key?>_0[]" id="treatment_finished_<?php echo $key?>_1_0" value="no">
                                                <label class="form-check-label" for="treatment_finished_<?php echo $key?>_1_0">no</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input treatment_finished" type="radio" data-id="<?php echo $key?>" data-index="0" name="treatment_finished_<?php echo $key?>_0[]" id="treatment_finished_<?php echo $key?>_2_0" value="yes">
                                                <label class="form-check-label" for="treatment_finished_<?php echo $key?>_2_0">yes</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group treatment_finished_<?php echo $key?>_0 d-none">
                                    <div class="row clearfix">
                                        <label for="treatment_finished_addition_info_<?php echo $key?>" class="col-sm-4">Since when:</label>
                                        <div class="col-sm-8">
                                            <textarea name="treatment_finished_addition_info_<?php echo $key?>[]" id="treatment_finished_addition_info_<?php echo $key?>" cols="30" rows="5" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row clearfix">
                                        <label for="free_of_complaint_<?php echo $key?>" class="col-sm-4">Free of complaints?</label>
                                        <div class="col-sm-8">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input free_complaint" type="radio" data-id="<?php echo $key?>" data-index="0" name="free_of_complaint_<?php echo $key?>_0[]" id="free_of_complaint_<?php echo $key?>_1_0" value="no">
                                                <label class="form-check-label" for="free_of_complaint_<?php echo $key?>_1_0">no</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input free_complaint" type="radio" data-id="<?php echo $key?>" data-index="0" name="free_of_complaint_<?php echo $key?>_0[]" id="free_of_complaint_<?php echo $key?>_2_0" value="yes">
                                                <label class="form-check-label" for="free_of_complaint_<?php echo $key?>_2_0">yes</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group free_complaint_<?php echo $key?>_0 d-none">
                                    <div class="row clearfix">
                                        <label for="free_of_complaint_addition_info_<?php echo $key?>" class="col-sm-4">Since when:</label>
                                        <div class="col-sm-8">
                                            <textarea name="free_of_complaint_addition_info_<?php echo $key?>[]" id="free_of_complaint_addition_info_<?php echo $key?>" cols="30" rows="5" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-right"><button type="button" class="btn btn-add btn-success btn-sm" data-key="<?php echo $key?>"><i class="fa fa-plus-square" aria-hidden="true"></i> Add</button></div>
                </div>
            <?php } ?>
        <?php } ?>
    </div>
    <div class="modal-footer clearfix">
        <input type="hidden" name="id_box" value=""/>
        <button type="submit" class="btn btn-primary">Next</button>
        <button type="button" class="btn btn-default btn-close-without-save-data">Close</button>
    </div>
</form>

<script>
  $(document).ready(function () {
    $('input[type="radio"].question').change(function() {
      var idx = $(this).attr('data-id');
      var $object = $('.addition_' + idx);
      $object.find('textarea').removeAttr('required');
      $object.find('input').removeAttr('required');
      $object.addClass('d-none');
      if ($(this).val() === 'yes') {
        $object.removeClass('d-none');
        $object.find('textarea').not('.not-required').attr('required', 'required');
        $object.find('input').attr('required', 'required');
      }
    });
    $('.modal-body').on('click', '.btn-add', function() {
      var key = $(this).attr('data-key');
      var idx = $('.card_' + key).length;
      var object = '<div class="card border-primary mb-3 card_'+ key +'">\n' +
        '                        <i class="fa fa-window-close" aria-hidden="true"></i>\n\n' +
        '                        <div class="card-body text-primary">\n' +
        '                            <div class="form-group">\n' +
        '                                <div class="row clearfix">\n' +
        '                                    <label for="additional_'+ key +'" class="col-sm-4">Diagnosis</label>\n' +
        '                                    <div class="col-sm-8">\n' +
        '                                        <textarea name="additional_'+ key +'[]" id="additional_'+ key +'" cols="30" rows="5" class="form-control"></textarea>\n' +
        '                                    </div>\n' +
        '                                </div>\n' +
        '                            </div>\n' +
        '                            <div class="form-group">\n' +
        '                                <div class="row clearfix">\n' +
        '                                    <label for="treat_from_'+ key +'" class="col-sm-4">Treatment from:</label>\n' +
        '                                    <div class="col-sm-8">\n' +
        '                                        <input class="form-control" type="text" name="treat_from_'+ key +'[]" id="treat_from_'+ key +'"/>\n' +
        '                                    </div>\n' +
        '                                </div>\n' +
        '                            </div>\n' +
        '                            <div class="form-group">\n' +
        '                                <div class="row clearfix">\n' +
        '                                    <label for="treat_from_'+ key +'" class="col-sm-4">Treatment to:</label>\n' +
        '                                    <div class="col-sm-8">\n' +
        '                                        <input class="form-control" type="text" name="treat_to_'+ key +'[]" id="treat_to_'+ key +'"/>\n' +
        '                                    </div>\n' +
        '                                </div>\n' +
        '                            </div>\n' +
        '                            <div class="form-group">\n' +
        '                                <div class="row clearfix">\n' +
        '                                    <label for="treatment_'+ key +'" class="col-sm-4">Which treatment?</label>\n' +
        '                                    <div class="col-sm-8">\n' +
        '                                        <textarea name="treatment_'+ key +'[]" id="treatment_'+ key +'" cols="30" rows="5" class="form-control"></textarea>\n' +
        '                                    </div>\n' +
        '                                </div>\n' +
        '                            </div>\n' +
        '                            <div class="form-group">\n' +
        '                                <div class="row clearfix">\n' +
        '                                    <label for="medication_'+ key +'" class="col-sm-4">Medication, dosage:</label>\n' +
        '                                    <div class="col-sm-8">\n' +
        '                                        <textarea name="medication_'+ key +'[]" id="medication_'+ key +'" cols="30" rows="5" class="form-control not-required"></textarea>\n' +
        '                                    </div>\n' +
        '                                </div>\n' +
        '                            </div>\n' +
        '                            <div class="form-group">\n' +
        '                                <div class="row clearfix">\n' +
        '                                    <label for="treatment_finished_'+ key +'" class="col-sm-4">Treatment finished?</label>\n' +
        '                                    <div class="col-sm-8">\n' +
        '                                        <div class="form-check form-check-inline">\n' +
        '                                            <input class="form-check-input treatment_finished" type="radio" data-id="'+ key +'" data-index="'+ idx +'" name="treatment_finished_'+ key +'_'+ idx +'[]" id="treatment_finished_'+ key +'_1' + '_' + idx + '" value="no">\n' +
        '                                            <label class="form-check-label" for="treatment_finished_'+ key +'_1' + '_' + idx + '">no</label>\n' +
        '                                        </div>\n' +
        '                                        <div class="form-check form-check-inline">\n' +
        '                                            <input class="form-check-input treatment_finished" type="radio" data-id="'+ key +'" data-index="'+ idx +'" name="treatment_finished_'+ key +'_'+ idx +'[]" id="treatment_finished_'+ key +'_2' + '_' + idx + '" value="yes">\n' +
        '                                            <label class="form-check-label" for="treatment_finished_'+ key +'_2' + '_' + idx + '">yes</label>\n' +
        '                                        </div>\n' +
        '                                    </div>\n' +
        '                                </div>\n' +
        '                            </div>\n' +
        '                            <div class="form-group treatment_finished_'+ key +'_'+ idx +' d-none">\n' +
        '                                <div class="row clearfix">\n' +
        '                                    <label for="treatment_finished_addition_info_'+ key +'" class="col-sm-4">Since when:</label>\n' +
        '                                    <div class="col-sm-8">\n' +
        '                                        <textarea name="treatment_finished_addition_info_'+ key +'[]" id="treatment_finished_addition_info_'+ key +'" cols="30" rows="5" class="form-control"></textarea>\n' +
        '                                    </div>\n' +
        '                                </div>\n' +
        '                            </div>\n' +
        '                            <div class="form-group">\n' +
        '                                <div class="row clearfix">\n' +
        '                                    <label for="free_of_complaint_'+ key +'" class="col-sm-4">Free of complaints?</label>\n' +
        '                                    <div class="col-sm-8">\n' +
        '                                        <div class="form-check form-check-inline">\n' +
        '                                            <input class="form-check-input free_complaint" type="radio" data-index="' + idx + '" data-id="'+ key +'" name="free_of_complaint_'+ key +'_'+ idx +'[]" id="free_of_complaint_'+ key +'_1' + '_' + idx + '" value="no">\n' +
        '                                            <label class="form-check-label" for="free_of_complaint_'+ key +'_1' + '_' + idx + '">no</label>\n' +
        '                                        </div>\n' +
        '                                        <div class="form-check form-check-inline">\n' +
        '                                            <input class="form-check-input free_complaint" type="radio" data-index="' + idx + '" data-id="'+ key +'" name="free_of_complaint_'+ key +'_'+ idx +'[]" id="free_of_complaint_'+ key +'_2' + '_' + idx + '" value="yes">\n' +
        '                                            <label class="form-check-label" for="free_of_complaint_'+ key +'_2' + '_' + idx + '">yes</label>\n' +
        '                                        </div>\n' +
        '                                    </div>\n' +
        '                                </div>\n' +
        '                            </div>\n' +
        '                            <div class="form-group free_complaint_'+ key +'_'+ idx +' d-none">\n' +
        '                                <div class="row clearfix">\n' +
        '                                    <label for="free_of_complaint_addition_info_'+ key +'" class="col-sm-4">Since when:</label>\n' +
        '                                    <div class="col-sm-8">\n' +
        '                                        <textarea name="free_of_complaint_addition_info_'+ key +'[]" id="free_of_complaint_addition_info_'+ key +'" cols="30" rows="5" class="form-control"></textarea>\n' +
        '                                    </div>\n' +
        '                                </div>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                    </div>';
      if (key === '15') {
        object = '<div class="card border-primary mb-3 card_'+ key +'">\n' +
          '                        <i class="fa fa-window-close" aria-hidden="true"></i>\n\n' +
          '                        <div class="card-body text-primary">\n' +
          '                                <div class="form-group">\n' +
          '                                    <div class="row clearfix">\n' +
          '                                        <label for="additional_' + key + '" class="col-sm-4">Diagnosis</label>\n' +
          '                                        <div class="col-sm-8">\n' +
          '                                            <textarea name="additional_' + key + '" id="additional_' + key + '[]" cols="30" rows="5" class="form-control"></textarea>\n' +
          '                                        </div>\n' +
          '                                    </div>\n' +
          '                                </div>\n' +
          '                                <div class="form-group">\n' +
          '                                    <div class="row clearfix">\n' +
          '                                        <label for="medication_from_' + key + '" class="col-sm-4">Medication intake from:</label>\n' +
          '                                        <div class="col-sm-8">\n' +
          '                                            <input class="form-control" type="text" name="medication_from_' + key + '[]" id="medication_from_' + key + '"/>\n' +
          '                                        </div>\n' +
          '                                    </div>\n' +
          '                                </div>\n' +
          '                                <div class="form-group">\n' +
          '                                    <div class="row clearfix">\n' +
          '                                        <label for="medication_to_' + key + '" class="col-sm-4">Medication intake to:</label>\n' +
          '                                        <div class="col-sm-8">\n' +
          '                                            <input class="form-control" type="text" name="medication_to_' + key + '[]" id="medication_to_' + key + '"/>\n' +
          '                                        </div>\n' +
          '                                    </div>\n' +
          '                                </div>\n' +
          '                                <div class="form-group">\n' +
          '                                    <div class="row clearfix">\n' +
          '                                        <label for="medication_' + key + '" class="col-sm-4">Medication, dose:</label>\n' +
          '                                        <div class="col-sm-8">\n' +
          '                                            <textarea name="medication_' + key + '" id="medication_' + key + '[]" cols="30" rows="5" class="form-control"></textarea>\n' +
          '                                        </div>\n' +
          '                                    </div>\n' +
          '                                </div>\n' +
          '                                <div class="form-group">\n' +
          '                                    <div class="row clearfix">\n' +
          '                                        <label for="medication_' + key + '" class="col-sm-4">Free of complaints?</label>\n' +
          '                                        <div class="col-sm-8">\n' +
          '                                            <div class="form-check form-check-inline">\n' +
          '                                                <input class="form-check-input free_complaint" type="radio" data-id="' + key + '" data-index="' + idx + '" name="free_of_complaint_' + key + '_' + idx + '[]" id="free_of_complaint_' + key + '_1' + '_' + idx + '" value="no">\n' +
          '                                                <label class="form-check-label" for="free_of_complaint_' + key + '_1' + '_' + idx + '">no</label>\n' +
          '                                            </div>\n' +
          '                                            <div class="form-check form-check-inline">\n' +
          '                                                <input class="form-check-input free_complaint" type="radio" data-id="' + key + '" data-index="' + idx + '" name="free_of_complaint_' + key + '_' + idx + '[]" id="free_of_complaint_' + key + '_2' + '_' + idx + '" value="yes">\n' +
          '                                                <label class="form-check-label" for="free_of_complaint_' + key + '_2' + '_' + idx + '">yes</label>\n' +
          '                                            </div>\n' +
          '                                        </div>\n' +
          '                                    </div>\n' +
          '                                </div>\n' +
          '                                <div class="form-group free_complaint_'+ key +'_'+ idx +' d-none">\n' +
          '                                    <div class="row clearfix">\n' +
          '                                        <label for="free_of_complaint_addition_info_'+ key +'" class="col-sm-4">Since when:</label>\n' +
          '                                        <div class="col-sm-8">\n' +
          '                                            <textarea name="free_of_complaint_addition_info_'+ key +'[]" id="free_of_complaint_addition_info_'+ key +'" cols="30" rows="5" class="form-control"></textarea>\n' +
          '                                        </div>\n' +
          '                                    </div>\n' +
          '                                </div>\n' +
          '                        </div>\n' +
          '                    </div>';
      }
      $(object).insertAfter('.card_' + key + ':last-child');
      $(object).find('textarea').not('.not-required').attr('required', 'required');
      $(object).find('input').attr('required', 'required');
    });
    $('.modal-body').on('click', '.fa.fa-window-close', function() {
      $(this).parent('.card.border-primary').remove();
    });
    $('.modal-body').on('change', 'input.treatment_finished', function() {
      var key = $(this).attr('data-id');
      var index = $(this).attr('data-index');
      if ($(this).val() === 'yes') {
        $('.treatment_finished_' + key + '_' + index).removeClass('d-none');
      } else {
        $('.treatment_finished_' + key + '_' + index).addClass('d-none');
      }
    });
    $('.modal-body').on('change', 'input.free_complaint', function() {
      var key = $(this).attr('data-id');
      var index = $(this).attr('data-index');
      if ($(this).val() === 'yes') {
        $('.free_complaint_' + key + '_' + index).removeClass('d-none');
      } else {
        $('.free_complaint_' + key + '_' + index).addClass('d-none');
      }
    });
  });
</script>