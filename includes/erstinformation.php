<div class="container page" id="erstinformation">
    <div class="row">
        <div class="col-12 text-right">
            <a href="assets/files/Erstinformation.pdf" download class="btn btn-warning">Download</a>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <p>Translation aid of</p>

            <p>Initial information</p>

            <p><span style="color:#e74c3c">(Legally binding is only the below following German version)</span></p>

            <p><strong>1. Your broker</strong></p>
            <p>Your broker has a business license according to § 34d para.1 of the GewO as an insurance broker and is registered under the following
                number in the register of brokers according to § 11a of the GewO. As a broker, she is your contact person for the agreed insurance matters
                and personally responsible for her advice according to §§ 60,61 und 63 of the VVG.</p>
            <p>(1) Broker<br/>
            Martina Martínez<br/>
            Im Mühlengrund 11<br/>
            56566 Neuwied<br/>
            D-3KF9-A1I4B-40</p>
            <p>(2) Contact person<br/>
            Martina Martínez<br/>
            Im Mühlengrund 11<br/>
            56566 Neuwied<br/>
            Fax.02631-9522749<br/>
            mm@martina-martinez.de<br/>
            D-3KF9-A1I4B-40</p>
            <p>(3) There is no participation in or from insurers or other parent companies.</p><br/>
            <p><strong>2. Your contractual partner</strong></p>
            <p>Your contractual partner is at all times the following company and has a business license according to § 34d para.1 of the GewO as an
            insurance broker. Should you be unsatisfied in a particular case with the advice given by the aforementioned broker, you can at all times
                contact your contractual partner.</p>
            <p>(1) Contractual partner<br/>
            Martina Martínez<br/>
            Im Mühlengrund 11<br/>
            56566 Neuwied<br/>
                D-3KF9-A1I4B-40</p>
            <p>(2) There is no participation in or from insurers or other parent companies.</p>
            <p>(3) There is a professional liability insurance in compliance with the law. This has been verified by the IHK. The registration took place
                through the IHK Koblenz under Registration No. D-3KF9-A1I4B-40.</p><br/>
            <p><strong>3. Offer of consulting services</strong></p>
            <p>The client will be offered consulting services regarding the desired insurance coverage before the procurement or signing of an insurance
            policy. Whether the client desired and obtained consulting services results from the consultation documentation or a declaration to abstain
                from the consulting documentation by the client.</p><br/>
            <p><strong>4. Common information</strong></p>
            <p>Should you wish to check the registration in the brokerage register, you may do so in this website:</p>
            <p><a href="http://www.vermittlerregister.info" title="veremitllerregister.info">www.vermittlerregister.info</a></p>
            <p>or on</p>
            <p>Telephone no.: <a href="tel:+01806005850">(0 180) 60 05 85 0</a> (Landline price €0.20 per call; Mobile phone price maximum €0.60 per call)</p>
            <p>or at</p>
            <p>DIHK e.V.<br/>
            Breite Straße 29<br/>
            10178 Berlin<br/>
            Tel.: (030) 20308-0<br/>
            Website: <a href="http://www.dihk.de">www.dihk.de</a></p>
            <p>as common competent bodies according to § 11a of the GewO.</p>
            <p>Should you at any time not be satisfied with our services, you can call the following out-of-court complaint and redress bodies:</p>
            <p>Versicherungsombudsmann e.V., Postfach 080 632, 10006 Berlin<br>
            Ombudsmann Private Kranken- und Pflegeversicherung, Postfach 06 02 22, 10052 Berlin</p>
            <p>Online dispute resolution via the EU<br>
                <a href="https://webgate.ec.europa.eu/odr">https://webgate.ec.europa.eu/odr</a></p>
            <p><strong>5. Basis of the consulting and brokerage services</strong></p>
            <p>The client is hereby informed on which basis the consulting and brokerage services of the contract are provided:<br/>
            (1) free consulting services for clients</p>
            <p><strong>6. Client´s signature</strong></p>
            <p>With the following signature you confirm the receipt and understanding of the previously mentioned information.</p>
            <br/>
            <br/>
            <br/>
                <strong>1. Ihr Vermittler</strong><br />
                Ihr Vermittler verf&uuml;gt &uuml;ber eine Gewerbeerlaubnis nach &sect; 34d Abs.1 GewO als Versicherungsmakler und ist unter der nachstehend genannten<br />
                Registernummer in das Vermittlerregister nach &sect; 11a GewO eingetragen. Er ist als Vermittler Ihr Ansprechpartner in den vereinbarten<br />
                Versicherungsangelegenheiten und pers&ouml;nlich verantwortlich f&uuml;r seine Beratung nach &sect;&sect; 60,61 und 63 VVG.</p>

            <p>(1) Vermittler<br />
                Martina Mart&iacute;nez<br />
                Im M&uuml;hlengrund 11<br />
                56566 Neuwied<br />
                D-3KF9-A1I4B-40</p>

            <p>(2) Ansprechpartner<br />
                Martina Mart&iacute;nez<br />
                Im M&uuml;hlengrund 11<br />
                56566 Neuwied<br />
                Fax.02631-9522749<br />
                mm@martina-Mart&iacute;nez.de<br />
                D-3KF9-A1I4B-40</p>

            <p>(3) Es bestehen keine Beteiligungen an oder von Versicherern oder deren Muttergesellschaften.</p>

            <p><strong>2. Ihr Vertragspartner</strong><br />
                Ihr Vertragspartner ist stets die nachgenannte Gesellschaft und verf&uuml;gt &uuml;ber eine Gewerbeerlaubnis nach &sect; 34d Abs.1 GewO als<br />
                Versicherungsmakler. Sollten Sie mit der Beratung durch Ihren o.g. Vermittler im Einzelfall nicht zufrieden sein, so k&ouml;nnen Sie sich jederzeit an<br />
                Ihren Vertragspartner wenden.</p>

            <p>(1) Vertragspartner<br />
                Martina Mart&iacute;nez<br />
                Im M&uuml;hlengrund 11<br />
                56566 Neuwied<br />
                D-3KF9-A1I4B-40</p>

            <p>(2) Es bestehen keine Beteiligungen an oder von Versicherern oder deren Muttergesellschaften.</p>

            <p>(3) Es besteht eine gesetzeskonforme Verm&ouml;gensschadenhaftpflicht, diese wurde der IHK nachgewiesen. Die Registrierung erfolgte &uuml;ber die<br />
                IHK Koblenz unter Registriernummer: D-3KF9-A1I4B-40</p>

            <p><strong>3. Beratungsangebot</strong><br />
                Dem Kunden wird eine Beratung &uuml;ber den gew&uuml;nschten Versicherungsschutz vor einer Vertragsvermittlung oder dem Abschluss eines<br />
                Versicherungsvertrages angeboten. Ob der Kunde eine Beratung gew&uuml;nscht und erhalten hatte, ergibt sich aus der Beratungsdokumentation<br />
                oder einer Beratungsverzichtserkl&auml;rung des Kunden.</p>

            <p><strong>4. Gemeinsame Angaben</strong></p>

            <p>Sofern Sie die Eintragungen im Vermittlerregister &uuml;berpr&uuml;fen m&ouml;chten, so k&ouml;nnen Sie dies &uuml;ber die Internetseite</p>

            <p>www.vermittlerregister.info</p>

            <p>oder unter<br />
                Telefon: (0 180) 60 05 85 0 (Festpreis 0,20 &euro;/Anruf; Mobilfunkpreise maximal 0,60 &euro;/Anruf)</p>

            <p>oder bei der</p>

            <p>DIHK e.V.<br />
                Breite Stra&szlig;e 29<br />
                10178 Berlin<br />
                Telefon: (030) 20308-0<br />
                Internet: www.dihk.de</p>

            <p>als registerf&uuml;hrende gemeinsame Stelle nach &sect; 11a GewO jederzeit veranlassen.</p>

            <p>Sofern Sie mit unseren Dienstleistungen einmal nicht zufrieden sein sollten, k&ouml;nnen Sie folgende Stellen als au&szlig;ergerichtliche<br />
                Schlichtungsstellen anrufen:</p>

            <p>Versicherungsombudsmann e.V., Postfach 080 632, 10006 Berlin<br />
                Ombudsmann Private Kranken- und Pflegeversicherung, Postfach 06 02 22, 10052 Berlin</p>

            <p>Online-Streitbeilegung via EU<br />
                https://webgate.ec.europa.eu/odr</p>

            <p><strong>5. Grundlage der Beratung und Vermittlung</strong><br />
                Dem Mandanten/Kunden wird vom Vermittler hiermit mitgeteilt, auf welcher Grundlage die Beratung und Vermittlung des Vertrages erfolgt:<br />
                (1) Kostenfreie Beratung f&uuml;r den Kunden</p>

            <p><strong>6. Unterschrift Mandant</strong><br />
                <br />
                Mit der nachfolgenden Unterschrift best&auml;tigen Sie die vorgenannten Informationen erhalten und verstanden zu haben.</p>
        </div>
    </div>
</div>