<div class="container tabContent activeTab" id="homepage">
    <div class="row description">
        <div class="basket">
            My Enquiries
            <span class="badge badge-pill badge-danger"><?php echo getTotalCart(); ?></span>
            <ul class="listItem" id="list_item">
                <?php
                $itemCarts = getItemCart();
                if ($itemCarts) {
                    foreach ($itemCarts as $item) {
                        ?>
                        <li class="item btn-edit clearfix"><span
                            data-id="<?php echo $item['id'] ?>"><?php echo $item['name'] ?></span></li><?php
                    }
                    echo '<li class="item btn-edit"><button data-id=""
                                                          class="btn btn-sm btn-submit btn-success btn-block">Submit Request
                            </button></li>';
                }
                ?>

            </ul>
        </div>
        <div class="avatar">
            <img src="assets/images/avatar.png" alt="">
        </div>
        <div class="col-md-2">

        </div>
        <div class="content col-md-11">
            <p class="col-7">
                Are you an English-speaking person in Germany looking for personal insurance?<br><br>
                Please choose the types of insurance which interest you.
            </p>
        </div>
    </div>
    <div class="section row">
        <div class="item item-homepage col-md-4 text-center">
            <div class="title">
                <h2>Liability<br/>
                    insurance</h2>
                <div class="tool-tip">
                    <div class="icon-tooltip" data-toggle="tooltip" data-placement="bottom"
                         title="The personal liability insurance protects you against the financial consequences of damaging something belonging to a third party or injuring a person unintentionally.">
                        <p><i class="fa fa-question-circle"></i></p>
                    </div>
                </div>
            </div>
            <div class="icon" data-id="liability-insurance">
                <img src="assets/images/liability.png" alt="">
            </div>
            <div class="check-box">
                <input id="liability-insurance" type="checkbox" name="liability-insurance"
                       class="checkbox-label checkbox-item" value="liability-insurance"
                    <?php echo checkExistCart('liability-insurance') ? 'checked' : '' ?>/>
                <label for="liability-insurance">
                    I'm Interested
                </label>
            </div>
        </div>
        <div class="item item-homepage col-md-4 text-center">
            <div class="title">
                <h2>Household<br/>
                    insurance</h2>
                <div class="tool-tip">
                    <div class="icon-tooltip" data-html="true" data-toggle="tooltip" data-placement="bottom"
                         title="Protects your belongings against damage caused by fire, water leaks, storm/hail, burglary and other risks.">
                        <p><i class="fa fa-question-circle"></i></p>
                    </div>
                </div>
            </div>
            <div class="icon" data-id="household-insurance">
                <img src="assets/images/household.png" alt="">
            </div>
            <div class="check-box">
                <input id="household-insurance" type="checkbox" name="household-insurance"
                       class="checkbox-label checkbox-item" value="household-insurance"
                    <?php echo checkExistCart('household-insurance') ? 'checked' : '' ?>/>
                <label for="household-insurance">
                    I'm Interested
                </label>
            </div>
        </div>
        <div class="item item-homepage col-md-4 text-center">
            <div class="title">
                <h2>Legal expenses<br/>
                    insurance</h2>
                <div class="tool-tip">
                    <div class="icon-tooltip" data-toggle="tooltip" data-placement="bottom" title="Sometimes legal support is required to defend yourself against accusations or to enforce your own
                                        rights and interests. The legal protection insurance covers the costs for many of the possible legal
                                        disputes.">
                        <p><i class="fa fa-question-circle"></i></p>
                    </div>
                </div>
            </div>
            <div class="icon" data-id="legal-protection-insurance">
                <img src="assets/images/legal-protection.png" alt="">
            </div>
            <div class="check-box">
                <input id="legal-protection-insurance" type="checkbox" name="legal-protection-insurance"
                       class="checkbox-label checkbox-item" value="legal-protection-insurance"
                    <?php echo checkExistCart('legal-protection-insurance') ? 'checked' : '' ?>/>
                <label for="legal-protection-insurance">
                    I'm Interested
                </label>
            </div>
        </div>
    </div>
    <div class="section row">
        <div class="item item-homepage col-md-4 text-center">
            <div class="title">
                <h2>Travel<br/>
                    insurances</h2>
                <div class="tool-tip">
                    <div class="icon-tooltip" data-html="true" data-toggle="tooltip" data-placement="bottom"
                         title="Travel insurances are helpful for private and business travel and for stays abroad. Even if you plan
                                        everything well, unexpected things can happen. <br><br>
                                        Travel <b>health insurance</b> is needed if you have a German public health insurance (which is mostly not
                                        valid outside of Germany) and want to travel abroad. It is also necessary for visitors from abroad who
                                        want to visit Germany. <br><br>
                                        The <b>travel cancellation or interruption insurances</b> take over the cancellation fee of your travel costs in case of sudden illness, accident, unemployment or death of the travelling person. <br><br>
                                        The <b>luggage insurance</b> protects in case of loss of baggage.">
                        <p><i class="fa fa-question-circle"></i></p>
                    </div>
                </div>
            </div>
            <div class="icon" data-id="travel-insurance">
                <img src="assets/images/travel-insurances.png" alt="">
            </div>
            <div class="check-box">
                <input id="travel-insurance" type="checkbox" name="travel-insurances"
                       class="checkbox-label checkbox-item" value="travel-insurance"
                    <?php echo checkExistCart('travel-insurance') ? 'checked' : '' ?>/>
                <label for="travel-insurance">
                    I'm Interested
                </label>
            </div>
        </div>
        <div class="item item-homepage col-md-4 text-center">
            <div class="title">
                <h2>Car<br/>
                    insurance</h2>
                <div class="tool-tip">
                    <div class="icon-tooltip" data-html="true" data-toggle="tooltip" data-placement="bottom"
                         title="<b>Motor vehicle liability</b> is compulsory for every car owner who wants to drive their vehicle in public traffic. It pays if the driver damages a third vehicle. In addition, you can insure damages to your own car through the <b>partial</b> or <b>comprehensive covers</b>.">
                        <p><i class="fa fa-question-circle"></i></p>
                    </div>
                </div>
            </div>
            <div class="icon" data-id="car-insurance">
                <img src="assets/images/car-insurance.png" alt="">
            </div>
            <div class="check-box">
                <input id="car-insurance" type="checkbox" name="car-insurance"
                       class="checkbox-label checkbox-item" value="car-insurance"
                    <?php echo checkExistCart('car-insurance') ? 'checked' : '' ?>/>
                <label for="car-insurance">
                    I'm Interested
                </label>
            </div>
        </div>
        <div class="item item-homepage col-md-4 text-center">
            <div class="title">
                <h2>Term life<br/>
                    insurance</h2>
                <div class="tool-tip">
                    <div class="icon-tooltip" data-html="true" data-toggle="tooltip" data-placement="bottom"
                         title="The term life insurance secures your family. In the most difficult situation in life, your family gets
                                        financial support, which can be used for example for paying open loans or the education of your
                                        children.">
                        <p><i class="fa fa-question-circle"></i></p>
                    </div>
                </div>
            </div>
            <div class="icon" data-id="term-life-insurance">
                <img src="assets/images/term-life-insurance.png" alt="">
            </div>
            <div class="check-box">
                <input id="term-life-insurance" type="checkbox" name="term-life-insurance"
                       class="checkbox-label checkbox-item"
                       value="term-life-insurance"
                    <?php echo checkExistCart('term-life-insurance') ? 'checked' : '' ?>/>
                <label for="term-life-insurance">
                    I'm Interested
                </label>
            </div>
        </div>
    </div>
    <div class="section row">
        <div class="item item-homepage col-md-4 text-center">
            <div class="title">
                <h2>Accident/disability<br/>
                    insurance</h2>
                <div class="tool-tip">
                    <div class="icon-tooltip" data-html="true" data-toggle="tooltip" data-placement="bottom"
                         title="An <b>accident insurance</b> helps you with the financial consequences after an accident with a disability
                                        benefit, accident pension, death benefit, daily hospital allowance and some more extras. It is valid
                                        worldwide and covers you 24 h a day. <br>
                                        An <b>occupational disability insurance</b> pays if you can&#39;t continue working in your job because of illness
                                        or accident. This insurance secures the monthly subsistence for you and your family.">
                        <p><i class="fa fa-question-circle"></i></p>
                    </div>
                </div>
            </div>
            <div class="icon" data-id="accident-disability-insurance">
                <img src="assets/images/accident-disability.png" alt="">
            </div>
            <div class="check-box">
                <input id="accident-disability-insurance" type="checkbox" name="accident-disability-insurances"
                       class="checkbox-label checkbox-item"
                       value="accident-disability-insurance"
                    <?php echo checkExistCart('accident-disability-insurance') ? 'checked' : '' ?>/>
                <label for="accident-disability-insurance">
                    I'm Interested
                </label>
            </div>
        </div>
        <div class="item item-homepage col-md-4 text-center">
            <div class="title">
                <h2>Health<br/>
                    insurance</h2>
                <div class="tool-tip">
                    <div class="icon-tooltip" data-html="true" data-toggle="tooltip" data-placement="bottom"
                         title="Health insurance in Germany is mandatory. The general coverage of <b>the public health insurance</b>
                                        companies is prescribed by law, but they differ in contribution and some extras.<br><br>
                                        The <b>supplementary health insurance</b> is for people who have public health insurance and want to add
                                        the benefits and advantages a private patient has.<br><br>
                                        <b>Private health insurance</b> allows you to choose your own medical coverage, which usually has a
                                        higher quality than public health insurance. Mostly you have better services, shorter waiting times
                                        for medical appointments, better treatment and medicines. The private health insurance is available
                                        both for self-employed people and employees with a salary above the fixed ceiling for public health
                                        insurance (in 2019: €5,062.50 per month / €60,750 per year)">
                        <p><i class="fa fa-question-circle"></i></p>
                    </div>
                </div>
            </div>
            <div class="icon" data-id="health-insurance">
                <img src="assets/images/health-insurance.png" alt="">
            </div>
            <div class="check-box">
                <input id="health-insurance" type="checkbox" name="health-insurance"
                       class="checkbox-label checkbox-item" value="health-insurance"
                    <?php echo checkExistCart('health-insurance') ? 'checked' : '' ?>/>
                <label for="health-insurance">
                    I'm Interested
                </label>
            </div>
        </div>
        <div class="item item-homepage col-md-4 text-center">
            <div class="title">
                <h2>Pension<br/>
                    insurance</h2>
                <div class="tool-tip">
                    <div class="icon-tooltip" data-html="true" data-toggle="tooltip" data-placement="bottom"
                         title="Wherever you plan to spend your retirement, whether in Germany or abroad, one day the payment from
                                        your employer will end and you will live from the savings you made during your working life. In
                                        Germany there are several pension models, some are subsidized by the German government with
                                        tax-savings and allowances. Which one fits best for you depends on your personal situation and
                                        preferences. <br><br>
                                        You can get offers for <b>private pension insurance, basic pension</b> (tax-saving), <b>Riester pension</b> (tax-saving, allowances) and <b>company pension</b> (tax-saving)">
                        <p><i class="fa fa-question-circle"></i></p>
                    </div>
                </div>
            </div>
            <div class="icon" data-id="pension-insurance">
                <img src="assets/images/pension-insurance.png" alt="">
            </div>
            <div class="check-box">
                <input id="pension-insurance" type="checkbox" name="pension-insurance"
                       class="checkbox-label checkbox-item" value="pension-insurance"
                    <?php echo checkExistCart('pension-insurance') ? 'checked' : '' ?>/>
                <label for="pension-insurance">
                    I'm Interested
                </label>
            </div>
        </div>
    </div>
    <div class="row description-footer">
        <p>
            Are you looking for insurance offers in Germany and need advice in English? With our enquiry tool
            you can easily and quickly choose the insurances you are interested in. No matter whether you are
            looking for a personal liability insurance, a legal expenses insurance or a household insurance, you
            only need to enter a few key figures and we will calculate the individual offers for you. In
            addition,
            we can clarify in detail any questions you may have in a telephone consultation. Finally, you should
            understand what each insurance means.
        </p><br>
        <p>
            Protection against third parties is provided by <strong>private liability insurance</strong> (=
            Privathaftpflichtversicherung) and <strong>legal expenses insurance</strong> (=
            Rechtsschutzversicherung).<br><br>
            Property can be protected with <strong>household insurance</strong> (= Hausratversicherung),
            <strong>car insurance</strong> (=Kfz-Versicherung or Autoversicherung), and <strong>building
                insurance</strong> (= Gebäudeversicherung).<br><br>
            <strong>Travel insurances</strong> are helpful for private and business travel and for stays abroad.
            There is travel
            health insurance (= Auslandsreise-Krankenversicherung), travel cancellation insurance (=
            Reiserücktritt-Versicherung), travel interruption insurance (= Reiseabbruch-Versicherung) and
            luggage insurance (= Reisegepäck-Versicherung)<br><br>
            Personal protection for you and your family you will find with <strong>accident insurance</strong>
            (=
            Unfallversicherung), <strong>occupational disability insurance</strong> (=
            Berufsunfähigkeitsversicherung) and <strong>term life insurance</strong> (=
            Risikolebensversicherung)<br><br>
            <strong>Pension insurances</strong> for your retirement are available as private pension insurance
            (= Private
            Rentenversicherung), Riester pension (tax saving/with subsidy of the government), basic pension (tax
            saving, also called Rürup-Rente), company pension (tax saving, also called Betriebliche
            Altersvorsorge)
        </p>
    </div>
</div>