<div class="container tabContent activeTab" id="homepage">
    <div class="row description">
        <div class="avatar">
            <img src="assets/images/avatar.png" alt="">
        </div>
        <div class="col-md-2">

        </div>
        <div class="content col-md-11">
            <div class="col-7">
                <div class="title">
                    <strong>Health questionnaire<div class="tool-tip">
                            <div class="icon-tooltip" data-html="true" data-toggle="tooltip" data-placement="bottom"
                                 title="When applying for a life insurance or disability or health insurance, the insurance companies want to know about your medical history. It is the basis for the decision whether they will provide coverage or not. <br/>It is important to answer these questions truly, because by giving false data you risk losing your coverage in case of a claim.">
                                <i class="fa fa-question-circle"></i>
                            </div>
                        </div>
                    </strong>
                </div>
                Please choose the type of insurance and answer the health questions.
            </div>
        </div>
    </div>
    <div class="section row">
        <div class="item item-health col-sm-4 text-center">
            <div class="title">
                <h2 class="small-text">Life<br/>
                    insurance</h2>
            </div>
            <div class="icon" data-id="life-insurance">
                <img src="assets/images/term-life-insurance.png" alt="Life insurance"/>
            </div>
            <div class="check-box">
                <input id="life-insurance" type="checkbox" name="life-insurance"
                       class="checkbox-label checkbox-item-health" value="life-insurance"/>
                <label for="life-insurance" class="label-small">
                    Choose Type
                </label>
            </div>
        </div>
        <div class="item item-health col-sm-4 text-center">
            <div class="title">
                <h2 class="small-text">Disability<br/>
                    insurance</h2>
            </div>
            <div class="icon" data-id="disability-insurance">
                <img src="assets/images/accident-disability.png" alt="Disability insurance"/>
            </div>
            <div class="check-box">
                <input id="disability-insurance" type="checkbox" name="disability-insurance"
                       class="checkbox-label checkbox-item-health" value="disability-insurance"/>
                <label for="disability-insurance" class="label-small">
                    Choose Type
                </label>
            </div>
        </div>
        <div class="item item-health col-sm-4 text-center">
            <div class="title">
                <h2 class="small-text">Health<br/>
                    insurance</h2>
            </div>
            <div class="icon" data-id="health-insurance">
                <img src="assets/images/health-insurance.png" alt="Health insurance"/>
            </div>
            <div class="check-box">
                <input id="health-insurance" type="checkbox" name="health-insurance"
                       class="checkbox-label checkbox-item-health" value="health-insurance"/>
                <label for="health-insurance" class="label-small">
                    Choose Type
                </label>
            </div>
        </div>
    </div>
</div>