<div class="container page" id="impressum">
    <div class="row">
        <div class="col-12">
            <h3>Impressum</h3><br>
            <div class="fusion-text"><p>Martina Martínez<br> Im Mühlengrund 11<br> 56566 Neuwied</p>
                <p>Telefon: +49.2631.9569718<br> Telefax: +49.2631.9522749<br> Mobil: +49.170.9604841<br> E-Mail:
                    mm@martina-martinez.de</p>
                <p><strong>Branche / Tätigkeit:</strong> Vermittlung von Versicherungen<br> <strong>Staat, der die
                        Berufsbezeichnung verliehen hat:</strong> Deutschland<br> <strong>Inhaltlich Verantwortlicher
                        gemäß § 55 Abs. 2 RStV:</strong> Martina Martínez</p>
                <p><strong>Zuständiges Finanzamt:</strong><br> Neuwied<br> Augustastr. 70<br> 56564 Neuwied<br> Telefon
                    (Zentrale): 02631/9100<br> Fax (Zentrale): 02631/91029906</p>
                <p><strong>Behörde für die Erlaubnis nach § 34d Abs. 1 GewO:</strong><br> Industrie- und Handelskammer
                    zu Koblenz<br> Schlossstraße 2<br> 56068 Koblenz</p>
                <p>Die Erlaubnis gem. § 34d Abs. 1 GewO wurde erteilt.<br> <strong>Registrierungsnummer (gem. § 34d
                        GewO) lautet: D-3KF9-A1I4B-40</strong></p>
                <p><strong>Die Eintragung im Vermittlerregister kann wie folgt überprüft werden:</strong><br> Deutscher
                    Industrie- und Handelskammertag (DIHK) e.V.<br> Breite Straße 29<br> 10178 Berlin<br> Telefon: 0180
                    600 58 50<br> (Festnetzpreis 0,20 €/Anruf; Mobilfunkpreise maximal 0,60 €/Anruf)<br>
                    www.vermittlerregister.info<br> <strong>Berufsbezeichnung:</strong> Versicherungsmaklerin mit
                    Erlaubnis nach § 34d Abs. 1 GewO</p>
                <p><strong>Berufsrechtliche Regelungen:</strong><br> • § 34d Gewerbeordnung<br> • §§ 59-68 VVG<br> •
                    VersVermV<br> Die berufsrechtlichen Regelungen können über die vom Bundesministerium der Justiz und
                    von der juris GmbH betriebenen Homepage www.gesetze-im-internet.de eingesehen und abgerufen werden.
                </p>
                <p><strong>Folgende Schlichtungsstellen können zur außergerichtlichen Streitbeilegung angerufen
                        werden:</strong></p>
                <p>Versicherungsombudsmann e.V.,<br> Postfach 08 06 32, 10006 Berlin<br> Weitere Informationen:
                    www.versicherungsombudsmann.de</p>
                <p>Ombudsmann Private Kranken- und Pflegeversicherung,<br> Postfach 06 02 22, 10052 Berlin<br> Weitere
                    Informationen: www.pkv-ombudsmann.de</p>
                <p><img class="alignnone size-full wp-image-355 jetpack-lazy-image jetpack-lazy-image--handled"
                        src="assets/images/makler-protekt-siegel.png"
                        alt="" width="240" height="240" data-recalc-dims="1"
                        data-pagespeed-url-hash="2784691299"
                        sizes="(max-width: 240px) 100vw, 240px">
                </p><p><strong>Martina Martínez ist als unabhängige Versicherungsmaklerin tätig.</strong><br> Sie hält
                    keine direkten oder indirekten Beteiligungen von über 10% der Stimmrechte oder des Kapitals an einem
                    Versicherungsunternehmen. Ein Versicherungsunternehmen hält keine direkten oder indirekten
                    Beteiligungen von über 10% der Stimmrechte oder des Kapitals an Martina Martínez.</p>
                <p><strong>Hinweis nach Art. 14 Abs. 1 der EU-Verordnung 524/2013</strong><br> Online-Streitbeilegung
                    (Art. 14 Abs. 1 ODR-Verordnung): Die Europäische Kommission stellt unter ec.europa.eu/consumers/odr/
                    eine Plattform zur Online-Streitbeilegung bereit.<br> <strong>Haftungshinweis:</strong></p>
                <p><strong>1. Haftungsbeschränkung</strong><br> Die Inhalte dieser Website werden mit größtmöglicher
                    Sorgfalt erstellt. Der Anbieter übernimmt jedoch keine Gewähr für die Richtigkeit, Vollständigkeit
                    und Aktualität der bereitgestellten Inhalte. Die Nutzung der Inhalte der Website erfolgt auf eigene
                    Gefahr des Nutzers. Namentlich gekennzeichnete Beiträge geben die Meinung des jeweiligen Autors und
                    nicht immer die Meinung des Anbieters wieder. Mit der reinen Nutzung der Website kommt keinerlei
                    Vertragsverhältnis zwischen dem Nutzer und dem Anbieter zustande.</p>
                <p><strong>2. Externe Links</strong><br> Diese Website enthält Verknüpfungen zu Websites Dritter
                    („externe Links“). Diese Websites unterliegen der Haftung der jeweiligen Betreiber. Der Anbieter hat
                    bei der erstmaligen Verknüpfung der externen Links die fremden Inhalte daraufhin überprüft, ob
                    etwaige Rechtsverstöße bestehen. Zu dem Zeitpunkt waren keine Rechtsverstöße ersichtlich. Er hat
                    keinerlei Einfluss auf die aktuelle und zukünftige Gestaltung und auf die Inhalte der verknüpften
                    Seiten. Das Setzen von externen Links bedeutet nicht, dass er sich die hinter dem Verweis oder Link
                    liegenden Inhalte zu eigen machen. Eine ständige Kontrolle der externen Links ist für uns ohne
                    konkrete Hinweise auf Rechtsverstöße nicht zumutbar. Bei Kenntnis von Rechtsverstößen werden jedoch
                    derartige externe Links unverzüglich gelöscht.</p>
                <p><strong>3. Urheber- und Leistungsschutzrechte</strong><br> Die auf dieser Website veröffentlichen
                    Inhalte unterliegen dem deutschen Urheber- und Leistungsschutzrecht. Jede vom deutschen Urheber- und
                    Leistungsschutzrecht nicht zugelassene Verwertung bedarf der vorherigen schriftlichen Zustimmung des
                    Anbieters oder jeweiligen Rechtsinhabers. Dies gilt insbesondere für Vervielfältigung, Bearbeitung,
                    Übersetzung, Einspeicherung, Verarbeitung bzw. Wiedergabe von Inhalten in Datenbanken oder anderen
                    elektronischen Medien und Systeme. Inhalte und Rechts Dritter sind dabei als solche gekennzeichnet.
                    Die unerlaubte Vervielfältigung oder Weitergabe einzelner Inhalte oder kompletter Seiten ist nicht
                    gestattet und strafbar. Lediglich die Herstellung von Kopien und Downloads für den persönlichen,
                    privaten und nicht kommerziellen Gebrauch ist erlaubt.<br> Die Darstellung dieser Website in fremden
                    Frames ist nur mit schriftlicher Erlaubnis zulässig.</p>
                <p><strong>4. Rechtswirksamkeit dieses Haftungsausschlusses</strong><br> Dieser Haftungsausschluss ist
                    als Teil des Internetangebotes zu betrachten, von dem aus auf diese Seite verwiesen wurde. Sofern
                    Teile oder einzelne Formulierungen dieses Textes der geltenden Rechtslage nicht, nicht mehr oder
                    nicht vollständig entsprechen sollten, bleiben die übrigen Teile des Dokumentes in ihrem Inhalt und
                    ihrer Gültigkeit davon unberührt.</p>
                <p><strong>5. Rechtlicher Hinweis</strong><br> Die zur Verfügung gestellten Daten erheben keinen
                    Anspruch auf Richtigkeit oder Vollständigkeit. Für die Richtigkeit und Vollständigkeit der von
                    Dritten zur Verfügung gestellten Daten und Dokumente wird keinerlei Gewähr übernommen. Jegliche
                    Haftung für Schäden aus der hier zur Verfügung gestellten Information ist ausgeschlossen.</p>
        </div>
    </div>
</div>