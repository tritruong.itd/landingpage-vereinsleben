<div class="container tabContent activeTab" id="homepage">
    <div class="row description">
        <div class="avatar">
            <img src="assets/images/avatar.png" alt="">
        </div>
        <div class="col-md-2">

        </div>
        <div class="content col-md-11">
            <p class="col-7">
                <strong>Notification of claim</strong><br/>
                We need some information from you to report the damage to the insurance company.<br>
                Please choose the type of insurance for your claim.
            </p>
        </div>
        <!--<div class="content content-claim col-md-11">
            <p class="col-7 claim">
                <span><strong>Notification of claim</strong><br/> <br/>
                <u>Required data:</u></span><span> We need some information from you to report the damage to the insurance company.</>
                 <br/> <br/>
                <span>Please choose the type of insurance for your claim</span>
            </p>
        </div>-->
    </div>
    <div class="section row">
        <div class="item item-claim col-sm-6 text-center">
            <div class="title">
                <h2 class="small-text">Liability<br/>
                    insurance</h2>
                <!--<div class="tool-tip">
                    <div class="icon-tooltip" data-toggle="tooltip" data-placement="bottom"
                         title="The personal liability insurance protects you against the financial consequences of damaging something belonging to a third party or injuring a person unintentionally.">
                        <p><i class="fa fa-question-circle"></i></p>
                    </div>
                </div>-->
            </div>
            <div class="icon" data-id="liability-insurance">
                <img src="assets/images/liability.png" alt="">
            </div>
            <div class="check-box">
                <input id="liability-insurance" type="checkbox" name="liability-insurance"
                       class="checkbox-label checkbox-item-claim" value="liability-insurance"/>
                <label for="liability-insurance" class="label-small">
                    Choose Type
                </label>
            </div>
        </div>
        <div class="item item-claim col-sm-6 text-center">
            <div class="title">
                <h2 class="small-text">Household<br/>
                    insurance</h2>
                <!--<div class="tool-tip">
                    <div class="icon-tooltip" data-html="true" data-toggle="tooltip" data-placement="bottom"
                         title="Protects your belongings against damage caused by fire, water leaks, storm/hail, burglary and other risks.">
                        <p><i class="fa fa-question-circle"></i></p>
                    </div>
                </div>-->
            </div>
            <div class="icon" data-id="household-insurance">
                <img src="assets/images/household.png" alt="">
            </div>
            <div class="check-box">
                <input id="household-insurance" type="checkbox" name="household-insurance"
                       class="checkbox-label checkbox-item-claim" value="household-insurance"/>
                <label for="household-insurance" class="label-small">
                    Choose Type
                </label>
            </div>
        </div>
        <div class="item item-claim col-sm-6 text-center">
            <div class="title">
                <h2 class="small-text">Legal expenses<br/>
                    insurance</h2>
                <!--<div class="tool-tip">
                    <div class="icon-tooltip" data-toggle="tooltip" data-placement="bottom" title="Sometimes legal support is required to defend yourself against accusations or to enforce your own
                                        rights and interests. The legal protection insurance covers the costs for many of the possible legal
                                        disputes.">
                        <p><i class="fa fa-question-circle"></i></p>
                    </div>
                </div>-->
            </div>
            <div class="icon" data-id="legal-protection-insurance">
                <img src="assets/images/legal-protection.png" alt="">
            </div>
            <div class="check-box">
                <input id="legal-protection-insurance" type="checkbox" name="legal-protection-insurance"
                       class="checkbox-label checkbox-item-claim" value="legal-protection-insurance"/>
                <label for="legal-protection-insurance" class="label-small">
                    Choose Type
                </label>
            </div>
        </div>
        <div class="item item-claim col-sm-6 text-center">
            <div class="title">
                <h2 class="small-text">Car<br/>
                    insurance</h2>
                <!--<div class="tool-tip">
                    <div class="icon-tooltip" data-html="true" data-toggle="tooltip" data-placement="bottom"
                         title="<b>Motor vehicle liability</b> is compulsory for every car owner who wants to drive their vehicle in public traffic. It pays if the driver damages a third vehicle. In addition, you can insure damages to your own car through the <b>partial</b> or <b>comprehensive covers</b>.">
                        <p><i class="fa fa-question-circle"></i></p>
                    </div>
                </div>-->
            </div>
            <div class="icon" data-id="car-insurance">
                <img src="assets/images/car-insurance.png" alt="">
            </div>
            <div class="check-box">
                <input id="car-insurance" type="checkbox" name="car-insurance"
                       class="checkbox-label checkbox-item-claim" value="car-insurance"/>
                <label for="car-insurance" class="label-small">
                    Choose Type
                </label>
            </div>
        </div>
    </div>
</div>