<div class="container page" id="datenschutz">
    <div class="row">
        <div class="col-12 text-right">
            <a href="assets/files/Datenschutz.pdf" download class="btn btn-warning">Download</a>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <p>Translation aid of <br>Data Privacy Statement</p>
            <p><span style="color:#e74c3c">(Legally binding is only the below following German version)</span></p>
            <br/>
            <h3>Data Privacy Statement</h3>

            <p>Data protection</p>

            <p>The following provisions inform you (the "User") of the nature, scope and purpose of the collection,
                use and processing of personal data by</p>

            <p>Martina Martínez<br>
                In Mühlengrund 11<br>
                56566 Neuwied</p>

            <p>Telephone: <a href="tel:+4926319569718">+49.2631.9569718</a><br>
                Fax: <a href="fax:+4926319569718">+49.2631.9522749</a><br>
                Mobile: <a href="tel:+491709604841">+49.170.9604841</a><br>
                E-Mail: <a href="mailto:mm@martina-martinez.de">mm@martina-martinez.de</a></p>

            <p>(hereinafter referred to as "provider") on this website.</p>

            <p>Data is protected in accordance with legal regulations. Below you will find information about what
                data is collected during your visit and how it is used.</p>

            <p>Herein, the provider complies with the relevant data protection provisions, in particular the
                provisions of the Telemedia Act (TMG) and the Federal Data Protection Act (BDSG).</p>

            <p><strong>1. Data collection and processing</strong></p>

            <p><strong>2. Anonymous data processing</strong></p>

            <p>Users can visit the website of the provider without giving information about their person. Only access
                data without personal reference is stored.</p>

            <p>For technical reasons, the following data, which its Internet browser transmits to the provider or to
                its web space provider, are recorded (so-called server log files):</p>

            <p>- Browser type and version<br>
                - Used operating system<br>
                - Website from which you visit us (Referrer URL)<br>
                - Website you visit<br>
                - Date and time of your access<br>
                - Your Internet Protocol (IP) address<br>
                - "Internet Service Provider"</p>

            <p>A prior consent of the user is not required for this purpose.</p>

            <p>This anonymous data is stored separately from any personal information that may be provided and
                thus does not allow any conclusions to be drawn about a particular person. They are evaluated for
                statistical purposes in order to optimize the Internet presence and the offer of the provider.
                However, the latter reserves the right to subsequently review the access data if, on the basis of
                concrete evidence, a legitimate suspicion of unlawful use exists.</p>

            <p>This data will not be merged with other data sources. After statistical analysis, all data will be
                deleted.</p>

            <p>This data is evaluated solely to improve the offer of the provider and do not allow any personal
                conclusions about the user. Personal data is only collected if the user voluntarily agrees with this
                during his visit to the website.</p>

            <p><strong>1. Personal data</strong></p><br>

            <p>Personal data is information with the help of which a person can be determined. This includes
                personal information such as name, address, date of birth, e-mail address or telephone number. The
                provider only collects, uses or transfers personal data in compliance with the data protection
                regulations or if the user agrees to the data collection, for example, in personal contact or when
                sending emails to the provider. Personal data is only collected if the user voluntarily provides this
                information during their visit to the website. This data is collected by the provider, processed and
                stored and used for purposes of quotation and further processing and support. A transfer of this data
                to uninvolved third parties without consent does not take place, unless this is provided for by law. In
                addition, personal data is collected, processed, stored and used if the user explicitly specifies this.
                The provider collects, uses or transfers personal data only in compliance with the data protection
                regulations. The personal data of the user stored at the provider are processed by the provider or

                used to safeguard their own legitimate business interests and, if necessary, transmitted to the
                appropriate service providers in order to deliver the respectively desired service and to communicate
                with the user. In the context of contract brokerage, the provider also collects, processes and uses the
                data required by the user for tax and billing purposes.</p><br>

            <p><strong>2. Contact form</strong></p><br>

            <p>The provider offers the user the opportunity to contact them by e-mail and / or via a contact form on
                their site. In this case, the information provided by the user for the purpose of processing their
                contact as well as in the event that follow-up questions arise is stored. A passing on to third parties
                does not take place. A comparison of the data collected with data which may have been collected in
                other parts of the site, also does not occur.</p><br>

            <p><strong>3. Use of cookies</strong></p><br>

            <p>The provider uses cookies on its website. Cookies are small text files that are stored on the user's
                computer and allow an analysis of the use of the website by the user. The information generated by
                the cookies, such as time, location and frequency of the website visit including the IP address of the
                user is stored by the provider on the user's PC. The IP address is immediately anonymized during this
                process so that you remain anonymous to the provider as a user. The information generated by the
                cookie about the use of this website will not be disclosed to third parties. The user can prevent the
                installation of the cookies by a corresponding setting of their browser software. However, the user
                should be aware that in this case they may not be able to use all features on the website.</p><br>

            <p><strong>4. Security of the data</strong></p><br>

            <p>For technical reasons, the obtainment of data by unauthorized persons over the Internet can never
                be completely ruled out. The provider has taken the necessary technical and organizational measures
                to adequately protect the user´s stored data. The provider expressly states that despite all the
                technical precautions, the Internet does not permit absolute data security. The provider is not liable
                for the actions of third parties.</p><br>

            <p><strong>5. Revocation, changes, corrections and up-dates of data</strong></p><br>

            <p>The user has the right, upon request, to receive free information about the personal data that has
                been stored about them. In addition, they have the right to correct inaccurate data, block and delete
                their personal data, insofar as this does not conflict with a statutory retention requirement.</p>
            <br>

            <p>The revocation must be sent to:</p><br>

            <p>Martina Martínez<br>
                In Mühlengrund 11<br>
                56566 Neuwied
            </p><br>


            <p>Telephone: <a href="tel:+4926319569718">+49.2631.9569718</a><br>
                Fax: <a href="fax:+4926319569718">+49.2631.9522749</a><br>
                Mobile: <a href="tel:+491709604841">+49.170.9604841</a><br>
                E-Mail: <a href="mailto:mm@martina-martinez.de">mm@martina-martinez.de</a></p><br>

            <p><strong>6. Contact person for the use of your data</strong></p><br>

            <p>If you have any questions about the collection, processing or use of your personal data as well as
                information, corrections, blocking or deletion of data, please contact:</p><br>

            <p>Martina Martínez<br>
                Im Mühlengrund 11<br>
                56566 Neuwied
            </p><br>

            <p>Telephone: <a href="tel:+4926319569718">+49.2631.9569718</a><br>
                Fax: <a href="fax:+4926319569718">+49.2631.9522749</a><br>
                Mobile: <a href="tel:+491709604841">+49.170.9604841</a><br>
                E-Mail: <a href="mailto:mm@martina-martinez.de">mm@martina-martinez.de</a></p><br>

            <p><strong>7. Deletion of your data</strong></p><br>

            <p>You have the right to request deletion of the personal data stored by us. The possibility of an actual
                deletion depends on whether the fulfilment of a legal obligation by us, such as compliance with
                statutory retention requirements and the assertion, exercise and defence of legal claims makes this
                possible.</p><br>

            <p>Basically, we can delete your data if it is no longer required for the particular purpose for which it
                was collected.</p><br>

            <br/>
            <br/>
            <br/>
            <p style="text-align:left"><strong><h3>Datenschutz</h3></strong></p>

            <p style="text-align:left">Nachstehende Regelungen informieren Sie (den &bdquo;Nutzer&rdquo;) &uuml;ber die Art, den Umfang und Zweck der Erhebung, die Nutzung und die Verarbeitung von personenbezogenen Daten durch die</p>

            <p style="text-align:left">Martina Mart&iacute;nez<br />
                Im M&uuml;hlengrund 11<br />
                56566 Neuwied</p>

            <p style="text-align:left">Telefon: <a href="tel:+4926319569718">+49.2631.9569718</a><br />
                Telefax: <a href="fax:+4926319569718">+49.2631.9522749</a><br>
                Mobil: <a href="tel:+491709604841">+49.170.9604841</a><br>
                E-Mail: <a href="mailto:mm@martina-martinez.de">mm@martina-martinez.de</a></p>

            <p style="text-align:left">(im folgenden &bdquo;Anbieter&ldquo; genannt) auf dieser Webseite.</p>

            <p style="text-align:left">Daten werden im Rahmen der gesetzlichen Vorschriften gesch&uuml;tzt. Nachfolgend finden Sie Informationen, welche Daten w&auml;hrend Ihres Besuchs erfasst und wie diese genutzt werden.</p>

            <p style="text-align:left">Hierbei beachtet der Anbieter die einschl&auml;gigen datenschutzrechtlichen Bestimmungen, insbesondere die Regelungen des Telemediengesetzes (TMG) und des Bundesdatenschutzgesetzes (BDSG).</p>

            <ol>
                <li>
                    <p style="text-align:left"><strong>Datenerhebung und -verarbeitung</strong></p>
                </li>
                <li>
                    <p style="text-align:left"><strong>Anonyme Datenverarbeitung</strong></p>
                </li>
            </ol>

            <p style="text-align:left">Der Nutzer kann den Internetauftritt des Anbieters besuchen, ohne Angaben zu seiner Person zu machen. Gespeichert werden lediglich Zugriffsdaten ohne Personenbezug.</p>

            <p style="text-align:left">Aus technischen Gr&uuml;nden werden folgende Daten, die sein Internet-Browser an den Anbieter bzw. an seinen Webspace-Provider &uuml;bermittelt, erfasst (sogenannte Serverlogfiles):</p>

            <ul>
                <li>
                    <p style="text-align:left">Browsertyp und -version</p>
                </li>
                <li>
                    <p style="text-align:left">verwendetes Betriebssystem</p>
                </li>
                <li>
                    <p style="text-align:left">Webseite, von der aus Sie uns besuchen (Referrer URL)</p>
                </li>
                <li>
                    <p style="text-align:left">Webseite, die Sie besuchen</p>
                </li>
                <li>
                    <p style="text-align:left">Datum und Uhrzeit Ihres Zugriffs</p>
                </li>
                <li>
                    <p style="text-align:left">Ihre Internet Protokoll (IP)-Adresse</p>
                </li>
                <li>
                    <p style="text-align:left">&bdquo;Internet Service Provider&ldquo;</p>
                </li>
            </ul>

            <p style="text-align:left">Einer vorherigen Zustimmung des Nutzers bedarf es hierf&uuml;r nicht.</p>

            <p style="text-align:left">Diese anonymen Daten werden getrennt von eventuell angegebenen personenbezogenen Daten gespeichert und lassen so keine R&uuml;ckschl&uuml;sse auf eine bestimmte Person zu. Sie werden zu statistischen Zwecken ausgewertet, um den Internetauftritt und das Angebot des Anbieters optimieren zu k&ouml;nnen. Dieser beh&auml;lt sich jedoch vor, die Zugriffsdaten nachtr&auml;glich zu &uuml;berpr&uuml;fen, wenn aufgrund konkreter Anhaltspunkte der berechtigte Verdacht einer rechtswidrigen Nutzung steht.</p>

            <p style="text-align:left">Eine Zusammenf&uuml;hrung dieser Daten mit anderen Datenquellen wird nicht vorgenommen. Nach erfolgter statistischer Auswertung werden s&auml;mtliche Daten gel&ouml;scht.</p>

            <p style="text-align:left"><a name="_Toc460328546"></a> Diese Daten werden ausschlie&szlig;lich zur Verbesserung des Angebots des Anbieters ausgewertet und erlauben keinerlei R&uuml;ckschl&uuml;sse auf die Person des Nutzers. Personenbezogene Daten werden nur erhoben, wenn der Nutzer uns diese im Rahmen seines Besuchs des Internetauftritts freiwillig mitteilt.</p>

            <ol>
                <li>
                    <p style="text-align:left"><strong>Personenbezogene Daten</strong></p>
                </li>
            </ol>

            <p style="text-align:left"><a name="_Toc460328547"></a> Personenbezogene Daten sind solche Informationen, mit deren Hilfe eine Person bestimmbar ist. Dazu geh&ouml;ren pers&ouml;nliche Daten wie der Name, die Adresse, das Geburtsdatum, die E-Mail- Adresse oder die Telefonnummer. Der Anbieter erhebt, nutzt oder gibt personenbezogene Daten nur unter Einhaltung der datenschutzrechtlichen Vorschriften weiter oder wenn der Nutzer in die Datenerhebung einwilligt, beispielsweise bei pers&ouml;nlicher Kontaktaufnahme oder bei der Versendung von E- Mails an den Anbieter. Personenbezogene Daten werden nur erhoben, wenn der Nutzer dieses im Rahmen seines Besuchs des Internetauftritts freiwillig mitteilt. Diese Daten werden von dem Anbieter erhoben, verarbeitet und f&uuml;r Zwecke der Angebotserstellung sowie der weiteren Bearbeitung und Betreuung gespeichert und genutzt. Eine Weitergabe dieser Daten an unbeteiligte Dritte ohne Einwilligung erfolgt nicht, sofern dies nicht gesetzlich vorgesehen ist. Dar&uuml;ber hinaus werden pers&ouml;nliche Daten erhoben, verarbeitet, gespeichert und genutzt, wenn der Nutzer diese von sich aus angibt. Der Anbieter erhebt, nutzt oder gibt personenbezogene Daten nur unter Einhaltung der datenschutzrechtlichen Vorschriften weiter. Die bei dem Anbieter gespeicherten pers&ouml;nlichen Daten des Nutzers werden von dem Anbieter verarbeitet bzw. zur Wahrung berechtigter eigener Gesch&auml;ftsinteressen genutzt und gegebenenfalls an die entsprechenden Serviceanbieter &uuml;bermittelt, um den jeweils gew&uuml;nschten Service zu erbringen und um mit dem Nutzer kommunizieren zu k&ouml;nnen. Im Rahmen der Vertragsvermittlung erhebt, verarbeitet und nutzt der Anbieter die daf&uuml;r erforderlichen Daten des Nutzers auch f&uuml;r steuerliche und abrechnungsrelevante Zwecke.</p>

            <ol start="2">
                <li>
                    <p style="text-align:left"><strong>Kontaktm&ouml;glichkeit/Kontaktformular</strong></p>
                </li>
            </ol>

            <p style="text-align:left"><a name="_Toc460328549"></a> Der Anbieter bietet dem Nutzer auf seiner Seite die M&ouml;glichkeit, mit ihm per E-Mail und/oder &uuml;ber ein Kontaktformular in Verbindung zu treten. In diesem Fall werden die vom Nutzer gemachten Angaben zum Zwecke der Bearbeitung seiner Kontaktaufnahme sowie f&uuml;r den Fall, dass Anschlussfragen entstehen, gespeichert. Eine Weitergabe an Dritte erfolgt nicht. Ein Abgleich der so erhobenen Daten mit Daten, die m&ouml;glicherweise durch andere Komponenten der Seite erhoben werden, erfolgt ebenfalls nicht.</p>

            <ol start="3">
                <li>
                    <p style="text-align:left"><strong>Einsatz von Cookies</strong></p>
                </li>
            </ol>

            <p style="text-align:left"><a name="_Toc460328558"></a> Der Anbieter setzt auf seiner Webseite Cookies ein. Cookies sind kleine Textdateien, die auf dem Computer des Nutzers gespeichert werden und die eine Analyse der Benutzung der Webseite durch den Nutzer erm&ouml;glichen. Die durch die Cookies erzeugten Informationen, beispielsweise Zeit, Ort und H&auml;ufigkeit des Webseiten-Besuchs einschlie&szlig;lich der IP-Adresse des Nutzers speichert der Anbieter auf dem PC des Nutzers. Die IP-Adresse wird bei diesem Vorgang umgehend anonymisiert, so dass Sie als Nutzer f&uuml;r den Anbieter anonym bleiben. Die durch den Cookie erzeugten Informationen &uuml;ber die Benutzung dieser Webseite werden nicht an Dritte weitergegeben. Der Nutzer kann die Installation der Cookies durch eine entsprechende Einstellung seiner Browser-Software verhindern; der Anbieter weist ihn jedoch darauf hin, dass er in diesem Fall gegebenenfalls nicht s&auml;mtliche Funktionen der Webseite vollumf&auml;nglich nutzen kann.</p>

            <ol start="4">
                <li>
                    <p style="text-align:left"><strong>Sicherheit der Daten</strong></p>
                </li>
            </ol>

            <p style="text-align:left"><a name="_Toc460328570"></a> Bei der Daten&uuml;bertragung im Internet kann aus technischen Gr&uuml;nden nie vollst&auml;ndig ausgeschlossen werden, dass Unberechtigte Zugriff auf die &uuml;bertragenen Daten erhalten. Der Anbieter hat die erforderlichen technischen und organisatorischen Ma&szlig;nahmen ergriffen, um die durch ihn gespeicherten Daten der Nutzer angemessen zu sch&uuml;tzen. Er weist ausdr&uuml;cklich darauf hin, dass das Internet trotz aller technischen Vorkehrungen eine absolute Datensicherheit nicht zul&auml;sst. Der Anbieter haftet nicht f&uuml;r Handlungen Dritter.</p>

            <ol start="5">
                <li>
                    <p style="text-align:left"><strong>Widerruf, &Auml;nderungen, Berichtigungen und Aktualisierungen von Daten</strong></p>
                </li>
            </ol>

            <p style="text-align:left"><a name="_Toc460328571"></a> Der Nutzer hat das Recht, auf Antrag unentgeltlich Auskunft zu erhalten &uuml;ber die personenbezogenen Daten, die &uuml;ber ihn gespeichert wurden. Zus&auml;tzlich hat er das Recht auf Berichtigung unrichtiger Daten, Sperrung und L&ouml;schung seiner personenbezogenen Daten, soweit dem keine gesetzliche Aufbewahrungspflicht entgegensteht</p>

            <p style="text-align:left"><strong>Der Widerruf ist zu richten an:</strong></p>

            <p style="text-align:left">Martina Mart&iacute;nez<br />
                Im M&uuml;hlengrund 11<br />
                56566 Neuwied</p>

            <p style="text-align:left">Telefon: <a href="tel:+4926319569718">+49.2631.9569718</a><br />
                Telefax: <a href="fax:+4926319569718">+49.2631.9522749</a><br />
                Mobil: <a href="tel:+491709604841">+49.170.9604841</a><br />
                E-Mail: <span style="color:#0563c1"><u><a href="mailto:mm@martina-martinez.de" style="color:#0563c1">mm@martina-martinez.de</a></u></span></p>

            <ol start="6">
                <li>
                    <p style="text-align:left"><strong>Ansprechpartner zur Nutzung Ihrer Daten</strong></p>
                </li>
            </ol>

            <p style="text-align:left">Bei Fragen zur Erhebung, Verarbeitung oder Nutzung Ihrer personenbezogenen Daten sowie bei Ausk&uuml;nften, Berichtigungen, Sperrung oder L&ouml;schung von Daten wenden Sie sich bitte an:</p>

            <p style="text-align:left">Martina Mart&iacute;nez<br />
                Im M&uuml;hlengrund 11<br />
                56566 Neuwied</p>

            <p style="text-align:left">Telefon: <a href="tel:+4926319569718">+49.2631.9569718</a><br />
                Telefax: <a href="fax:+4926319569718">+49.2631.9522749</a><br />
                Mobil: <a href="tel:+491709604841">+49.170.9604841</a><br />
                E-Mail: mm@martina-martinez.de</p>

            <ol start="7">
                <li>
                    <p style="text-align:left"><strong>L&ouml;schung Ihrer Daten</strong></p>
                </li>
            </ol>

            <p style="text-align:left">Sie haben das Recht, von uns L&ouml;schung der bei uns gespeicherten personenbezogenen Daten zu verlangen. Die M&ouml;glichkeit einer tats&auml;chlichen L&ouml;schung richtet sich dabei danach, ob die Erf&uuml;llung einer rechtlichen Verpflichtung durch uns, wie etwa die Einhaltung gesetzlicher Aufbewahrungspflichten sowie die Geltendmachung, Aus&uuml;bung und Verteidigung von Rechtsanspr&uuml;chen dies m&ouml;glich macht.</p>

            <p style="text-align:left">Grunds&auml;tzlich gilt f&uuml;r die L&ouml;schung Ihrer Daten, dass dies durch uns dann erfolgt, wenn die Daten f&uuml;r den jeweiligen Zweck nicht mehr erforderlich sind.</p>
        </div>
    </div>
</div>